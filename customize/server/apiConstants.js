const HOST_NAME = "https://xpph7.happydoit.com:8090/front/wxapp"; // 线上地址 new
// const HOST_NAME = "https://meco.chinaxpp.com:8090/front/wxapp"; // 线上地址 废弃
// const HOST_NAME = "http://101.34.186.44:8080/front/wxapp"; // 线上地址
// const HOST_NAME = "http://10.3.48.188:8090/front/wxapp"; //本地服务
let HOST_NAME_PROMOTION = "https://playbill.chinaxpp.com/media-api"; //测试环境
export default {
  HOST_NAME_UPLOAD_FILE: `${HOST_NAME}/christmasRedBag/uploadRecording`,
  HOST_NAME_GET_TOKEN: `${HOST_NAME}/getToken`, // XPP-token
  HOST_NAME_SUBMIT_CHRISTMAS_CALL: `${HOST_NAME}/christmasRedBag/submitActivity`, // 圣诞祝福制作提交
  HOST_NAME_GET_CHRISTMAS_CALL_DETAIL: `${HOST_NAME}/christmasRedBag/inActivity`, // 获取圣诞祝福详情
  HOST_NAME_ADD_POWER: `${HOST_NAME}/christmasRedBag/toHelp`, // 圣诞助力
  HOST_NAME_OPEN_RED_BAG: `${HOST_NAME}/christmasRedBag/openRedBag`, // 领取红包
  HOST_NAME_GET_ACTIVITY_LIST: `${HOST_NAME}/christmasRedBag/myActivityList`, // 获取活动列表
  //快闪
  HOST_NAME_SET_PROMOTION_PAY_STATUS: `${HOST_NAME_PROMOTION}/wechat/statusNotify`, // 支付状态回调
};
