// import { HTTP_STATUS } from "./statusCode";

// // 封装 request
// const baseOptions = (params, method = "GET") => {
//   let { url, data } = params;
//   let contentType = "application/json";
//   contentType = params.contentType || contentType;

//   const option = {
//     url: url,
//     data: data,
//     method: method,
//     header: {
//       "content-type": contentType,
//     },
//   };
//   return wx.request(option);
// };

// // 封装GET请求
// export const getApi = (url, data) => {
//   let option = { url, data };
//   return baseOptions(option).then((res) => {
//     //请求成功200
//     if (res.statusCode === HTTP_STATUS.SUCCESS) {
//       if (res.data.code === HTTP_STATUS.HTTP_ERROR) {
//         wx.showToast({
//           title: res.data.msg,
//           icon: "none",
//           duration: 2000,
//         });
//       } else if (res.data.code === HTTP_STATUS.SUCCESS) {
//         return res.data;
//       } else {
//         console.log(res.data.code);
//         wx.showToast({
//           title: res.data.msg,
//           icon: "none",
//           duration: 2000,
//         });
//         return res.data;
//       }
//     } else {
//       console.error(res);
//     }
//   });
// };

// // 封装POST请求
// export const postApi = (url, data, contentType) => {
//   let option = { url, data, contentType };
//   return baseOptions(option, "POST").then((res) => {
//     if (res.statusCode === HTTP_STATUS.SUCCESS) {
//       if (res.data.code === HTTP_STATUS.HTTP_ERROR) {
//         wx.showToast({
//           title: res.data.msg,
//           icon: "none",
//           duration: 2000,
//         });
//       } else if (res.data.code === HTTP_STATUS.SUCCESS_CODE) {
//         return res.data;
//       } else {
//         wx.showToast({
//           title: res.data.msg,
//           icon: "none",
//           duration: 2000,
//         });
//       }
//     } else {
//       console.error(res);
//     }
//   });
// };

const request = (options) => {
  const { url, data, method } = options;
  return new Promise((resolve, reject) => {
    wx.request({
      url: url,
      method: method || "get",
      data: data || {},
      header: options.header || {
        "content-type": "application/json",
      },
      success: resolve,
      fail: reject,
    });
  });
};
export default request;
