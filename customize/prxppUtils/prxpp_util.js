
var http = require("./prxpp_httputil")

const UnionId_Storage_Key = 'xpp_pengren_unionid'
const UserInfo_Storage_Key = 'xpp_pengren_userInfo'
const PhoneNumber_Storage_Key = 'xpp_pengren_phoneNumber'

const opnelog = true
function log() {
  if(opnelog){
    console.log(...arguments)
  }
}

function showModal(){
  wx.showModal({
    title:"提示",
    confirmText:"确定",
    content:'为保障奖品的顺利发放需要您授权相应的用户信息!',
    showCancel: false
  })
}

function showToast(errMsg) {
  wx.showToast({
    title: errMsg,
    icon: 'none'
  })
}

/**
 * 向应用服务器发送code拉取openId，unionId，session_key
 * 私有方法
 * @param {String}} code  微信wx.login返回的code
 */
function getUserId(code){
  return new Promise(( resolve, reject ) => {
    http.prxpp_postRequest({
      url: 'applet/login',
      params: {
        code: code
      },
      success: data => {
        resolve(data.data)
      },
      failed: errMsg => {
        reject(errMsg)
      }
    })
  })
}

/**
 * 上传用户信息
 * @param {Object} userinfo 
 */
function uploadUserInfo(userinfo) {
  return new Promise(( resolve, reject ) => {
    http.prxpp_postRequest({
      url: 'applet/user/edit',
      params: {
        userInfo: userinfo,
      },
      success: data => {
        resolve(data.data)
      },
      failed: errMsg => {
        reject(errMsg)
      }
    })
  })
}

class prxpp_auth {
  constructor(){
  }

  get unionId() {
    let unionId = wx.getStorageSync(UnionId_Storage_Key)
    http.prxpp_updateHeader(unionId)
    return unionId
  }

  set unionId(val) {
    http.prxpp_updateHeader(val)
    wx.setStorageSync(UnionId_Storage_Key, val)
  }

  get phoneNumber() {
    let phonenumber = wx.getStorageSync(PhoneNumber_Storage_Key)
    return phonenumber
  }

  set phoneNumber(val) {
    wx.setStorageSync(PhoneNumber_Storage_Key, val)
  }

  /**
   * 注意：首次启动直接调用login，因为是嵌入到别人的小程序里的，不能保证是否登录过
   * 使用接口 wx.checkSession可以校验 session_key 是否有效，从而避免小程序反复执行登录流程。
   * session_key 用以解密加密数据，比如用户的手机号
   * @param {Function} result 过期返回result(false)，没有过期返回result(true)
   */
  // checkSession(result){
  //   wx.checkSession({
  //     success: () => {
  //       //登录未过期
  //       result(true)
  //     },fail: () => {
  //       //登录过期 需要调用login重新获取code传给服务器，服务器拉取session_key
  //       result(false)
  //     }
  //   })
  // }

  /**
   * 登录
   * @param {string} uiLoadingTitle loading title
   */
  login(){
    var self = this
    return new Promise(( resolve, reject ) => {
      wx.login({
        success: res => {
          // 发送 res.code 到后台换取 openId, sessionKey, unionId
          if (res.code) {
            log("登录成功：",res)
            this._code = res.code
            getUserId(res.code).then( result => {
              self.unionId = result.unionid
              resolve(result)
            }).catch( errMsg => {
              reject(errMsg)
            })
          }else{
            showToast(res.errMsg)
            reject(res.errMsg)
          }
        },
        fail: res => {
          showToast(res.errMsg)
          reject(res.errMsg)
        }
      })
    })
  }

  /**
   * 判断拉取用户已授权信息
   */
  checkUserInfo(){
    return new Promise(( resolve, reject ) => {
      var value = wx.getStorageSync(UserInfo_Storage_Key)
      if (value) {
        resolve(value)
      }else{
        wx.getUserProfile({
          desc: '用于发放相应的奖品',
          success: (res) => {
            let userInfo = res.userInfo
            uploadUserInfo(userInfo).then( _ => {
              wx.setStorageSync(UserInfo_Storage_Key, userInfo)
              resolve(userInfo)
            }).catch( errMsg => {
              reject(errMsg)
            })
          },
          fail: res => {
            showModal()
            reject(res.errMsg)
          }
        })
      }
    })
  }

  /**
   * 解密手机
   * @param {string} encryptedData 包括敏感数据在内的完整用户信息的加密数据
   * @param {string} iv 加密算法的初始向量
   */
  dncryptPhoneNumber(encryptedData, iv){
    let self = this
    return new Promise(( resolve, reject ) => {
      if(!!encryptedData && !!iv){
        http.prxpp_postRequest({
          url: 'applet/mobile/edit',
          params: {
            encryptedData: encryptedData,
            iv: iv
          },
          success: data => {
            self.phoneNumber = data.data.mobile
            resolve(data.data)
          },
          failed: errMsg => {
            reject(errMsg)
          }
        })
      }else{
        showModal()
        reject("fail to get user phonenumber")
      }
    })
  }
}

var util = new prxpp_auth

module.exports = util