
/**
 * 请求头
 */
var commonHeader = {
  'Authorization': "",
  'version': 'prxpp-mp-V1.0.0',
}

function updateUnionId(uid) {
  commonHeader.Authorization = uid
}

//const API_BASE_URL = 'https://app-api.acekid.com/api/v1/'
// const API_BASE_URL = 'https://test-xpp-bargain-api.acekid.com/app/'
const API_BASE_URL = 'https://tuiguang2.zjxpp.com/app/'

function showToast(errMsg, time) {
  wx.showToast({
    title: errMsg,
    icon: 'none',
    duration: time
  })
}

 /**
  * 供外部post请求调用
  * @param {Object} obj 
  * {
  *    String   url
  *    Object   params  
  *    String   header
  *    Function success
  *    Function failed
  * }
  */
function post(obj) {
  console.log("请求方式：", "POST")
  obj.method = "POST"
  request(obj);
}

/**
 * 供外部get请求调用
 * @param {Object} obj 
 * {
 *    String   url
 *    Object   params  
 *    String   header
 *    Function success
 *    Function failed
 * }
*/
function get(obj) {
  console.log("请求方式：", "GET")
  obj.method = "GET"
  request(obj);
}

/**
 * function: 封装网络请求
 * @url URL地址
 * @params 请求参数
 * @method 请求方式：GET/POST
 * @onSuccess 成功回调
 * @onFailed  失败回调
 */

function request(obj) {
  console.log('请求url：' + obj.url);
  wx.showLoading({
    title: "正在加载中...",
  })
  let header = obj.header || commonHeader
  let url = obj.url.indexOf("http") == 0 ? obj.url : API_BASE_URL + obj.url
  console.log("请求头：", header)
  wx.request({
    url: url,
    data: dealParams(obj.params),
    method: obj.method,
    header: header,
    success: function (res) {
      wx.hideLoading();
      console.log('响应：', res.data);

      if (res.data) {
        /** start 根据需求 接口的返回状态码进行处理 */
        if (res.statusCode == 200) {
          if(res.data.code == 0){
            obj.success(res.data); //request success
          }else{
            if(res.data.code == 104) {
              wx.removeStorageSync('xpp_pengren_unionid')
              wx.removeStorageSync('xpp_pengren_userInfo')
              wx.removeStorageSync('xpp_pengren_phoneNumber')
              showToast('登录已过期，请退出重进', 3000)
            }else{
              showToast(res.data.message, 2000)
            }
          }
        } else {
          showToast(res.data.message, 2000)
          obj.failed(res.data.message); //request failed
        }
        /** end 处理结束*/
      }
    },
    fail: function (error) {
      wx.hideLoading();
      showToast(error.errMsg, 2000)
      obj.failed(error.errMsg); //failure for other reasons
    }
  })
}

/**
 * function: 根据需求处理请求参数：添加固定参数配置等
 * @params 请求参数
 */
function dealParams(params) {
  console.log("请求参数:", params)
  return params;
}


// 1.通过module.exports方式提供给外部调用
module.exports = {
  prxpp_postRequest: post,
  prxpp_getRequest: get,
  prxpp_updateHeader: updateUnionId
}