// 圣诞背景
export const christmasBgList = {
  // 默认壁纸
  default: {
    bgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/defaultBg.gif", // 祝福制作完成/已接听页面静态壁纸
    callBgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/defaultBg.gif", // 待接听页面动态壁纸
    btnBg: "background: rgba(13, 27, 43, 0.9)", // 底部按钮透明背景
  },
  // 壁纸1
  1: {
    bgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/receive_bg_yellow.jpg",
    callBgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/bg1.jpg",
    btnBg: "background: rgba(152, 101, 37, 0.9)",
  },
  2: {
    bgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/receive_bg_green.jpg",
    callBgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/bg2.jpg",
    btnBg: "background: rgba(26, 90, 52, 0.9)",
  },
  3: {
    bgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/receive_bg_red.jpg",
    callBgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/bg3.png",
    btnBg: "background: rgba(110, 8, 13, 0.9)",
  },
  4: {
    bgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/receive_bg_blue.jpg",
    callBgUrl:
      "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/bg4.jpg",
    btnBg: "background: rgba(27, 55, 108, 0.9)",
  },
};

// 圣诞电话铃声
export const callMusic =
  "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/callMusic.mp3";

// 圣诞默认祝福语音
export const defaultMusic =
  "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/defaultMusic.mp3";
// 圣诞默认祝福语音-时长
export const defaultMusicTime = 15; // 单位s

// 圣诞默认祝福头像
export const defaultSenderAvatar =
  "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/defaultAvatar.png";
