import CryptoJS from "./crypto-js.min";
const heloo = {
  decryptDES: function (ciphertext) {
    const keyHex = CryptoJS.enc.Utf8.parse('meco_seession_key')
    if (ciphertext) {
      const decrypted = CryptoJS.DES.decrypt(ciphertext, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7,
      })
      return decrypted.toString(CryptoJS.enc.Utf8)
    }
  },
  wxLogin: function (loadType) {
  
    let that = this;
    return new Promise((resolve, reject) => {
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      wx.login({
        success(res) {
          console.log("code=" + res.code);
          wx.request({
            url: that.globalData.url + "/api/userIsNull",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              code: res.code,
            },
            success(data) {
              if (!loadType) {
                wx.hideLoading();
              }
              if (data.data.status == 0) {
                // 未注册
                let userInfo = {};
                userInfo.openid = data.data.openid;
                userInfo.unionid = data.data.unionid;
                userInfo.key =  that.decryptDES(data.data.session_key_secret);
                that.globalData.register = userInfo;
                that.globalData.userKey =  that.decryptDES(data.data.session_key_secret);
                wx.clearStorageSync();
                reject(data);
              } else {
                // 已注册
                that.globalData.userInfo = data.data.wxuser;
                that.globalData.userKey = that.decryptDES(data.data.session_key_secret);
                console.log('session_key_secret='+that.globalData.userKey)
                resolve(data);
              }
            },
          });
        },
      });
    });
  },
  writeTime: function (type, path, url) {
    let that = this;
    let options = wx.getLaunchOptionsSync();
    let openid = that.globalData.openid;
    if (path) {
      var page = path;
    } else {
      var page = options.path;
    }
    if (that.globalData.userInfo) {
      openid = that.globalData.userInfo.openid;
    }
    let data = {
      openid: openid,
      type: type,
      page: page,
      memo: options.scene,
    };
    if (url) {
      data.CouponURL = url;
    }
    let location = wx.getStorageSync("longitude");
    if (type == "2" && location && path) {
      data.location = location;
      data.location2 = wx.getStorageSync("latitude");
    }
    wx.request({
      url: that.globalData.url + "/api/meco/pvuv",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: data,
      success(data) {
        console.log(data);
      },
    });
  },
  writeCoupon: function (type, path, url) {
    let that = this;
    let options = wx.getLaunchOptionsSync();
    let openid = that.globalData.openid;
    if (path) {
      var page = path;
    } else {
      var page = options.path;
    }
    if (that.globalData.userInfo) {
      openid = that.globalData.userInfo.openid;
    }
    wx.request({
      url: that.globalData.url + "/api/meco/pvuv",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        openid: openid,
        type: type,
        page: page,
        CouponURL: url,
        location: wx.getStorageSync("longitude"),
        location2: wx.getStorageSync("latitude"),
        memo: options.scene,
      },
      success(data) {
        console.log(data);
      },
    });
  },
  signChannel: function (channel) {
    let that = this;
    let openid = that.globalData.openid;
    if (that.globalData.userInfo) {
      openid = that.globalData.userInfo.openid;
    }
    wx.request({
      url: that.globalData.url + "/api/skimLog",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        openid: openid,
        channel: channel,
      },
      success(data) {
        console.log(data);
      },
    });
  },
  globalData: {
    CryptoJS,
    userInfo: {},
    xpp_birthday: "", // 由群脉接口获取
    xpp_phone: "", // 由群脉接口获取
    xpp_isActivated: false, // 是否注册会员
    register: null,
    userKey: "",
    url: "https://ssl.meco.chinaxpp.com",
    christmasVoice: "", // 圣诞语音来电
    christmasBg: "", // 来电背景
    _fmOpt: {
      partnerCode: "chinaxpp", // 请填入您的partner code
      appName: "meco_xcx", // 请填入您的app name
      // 谨慎配置，确认是生产环境的情况下才需要设置下面的值，测试沙盒环境下面配置需要删掉
      env: "PRODUCTION"
    },
    // url:"https://yezi.happydoit.com"
    urlOrchard: "https://xpph7.happydoit.com/farm/index.html", //果园跳转
    // urlOrchard:'http://localhost:8080',//果园跳转测试环境
    baseUrlOrchard: "https://xpph7.happydoit.com:8090",
    wm_shop_app_id:"wx1a8460be609a207c",
  },
};
// Page({
//   onLoad: function (options) {

//   }
// })
export default heloo;