// import { Setting } from '../engine/index';

Component({
  data: {
    selected: null,
    isEnabled: true,
    nav_bar__center: "Meco真茶局",
    configStyle: {
      navigationbarinnerStyle: "",
      navBarLeft: 0,
      navBarHeight: 0,
      capsulePosition: {},
      navBarExtendHeight: 0,
      ios: false,
      rightDistance: 0,
    }, // 导航栏样式信息
  },
  properties: {
    back: Boolean,
    noBack: Boolean,
    home: Boolean,
    theme: String,
    title: String,
    color: String,
    extClass: String, // 自定义导航栏样式
    background: String,
    backgroundColorTop: String, //导航颜色
    delta: Number,
  },
  async attached() {
    const info = this.getSystemInfo();
    const configStyle = this.setStyle(info);
    const { navBarHeight, navBarExtendHeight, ios } = configStyle;
    const {
      backgroundColorTop,
      background,
      extClass,
      title,
      theme,
    } = this.data;
    this.setData({
      themeColor: theme === "dark" ? "" : "white",
      configStyle,
      nav_bar__center: title,
      heightStyle: navBarHeight + navBarExtendHeight,
      backgroundStyle: backgroundColorTop ? backgroundColorTop : background,
      navWrapStyle: `nav-bar-wrap ${ios ? "ios" : "android"} ${extClass}`,
    });
  },
  observers: {
    title: function (title) {
      this.setData({
        nav_bar__center: title,
      });
    },
  },
  computed: {},
  methods: {
    onBack() {
      // if (_isFunction(this.props.onBack)) {
      //   this.props.onBack();
      // } else{
      const pages = getCurrentPages();
      if (pages.length >= 2) {
        wx.navigateBack({
          delta: 1,
        });
      } else {
        wx.reLaunch({
          url: "/customize/pages/index/index",
        });
      }
      // }
    },
    onGoHome() {
      wx.redirectTo({
        url: "/customize/pages/index/index",
      });
    },
    getSystemInfo() {
      if (wx.globalSystemInfo && !wx.globalSystemInfo.ios) {
        return wx.globalSystemInfo;
      } else {
        let systemInfo = wx.getSystemInfoSync() || {
          model: "",
          system: "",
        };
        let ios = !!(systemInfo.system.toLowerCase().search("ios") + 1);
        let rect;
        try {
          rect = wx.getMenuButtonBoundingClientRect
            ? wx.getMenuButtonBoundingClientRect()
            : null;
          if (rect === null) {
            throw "getMenuButtonBoundingClientRect error";
          }
          //取值为0的情况  有可能width不为0 top为0的情况
          if (!rect.width || !rect.top || !rect.left || !rect.height) {
            throw "getMenuButtonBoundingClientRect error";
          }
        } catch (error) {
          let gap = ""; //胶囊按钮上下间距 使导航内容居中
          let width = 96; //胶囊的宽度
          if (systemInfo.platform === "android") {
            gap = 8;
            width = 96;
          } else if (systemInfo.platform === "devtools") {
            if (ios) {
              gap = 5.5; //开发工具中ios手机
            } else {
              gap = 7.5; //开发工具中android和其他手机
            }
          } else {
            gap = 4;
            width = 88;
          }
          if (!systemInfo.statusBarHeight) {
            //开启wifi的情况下修复statusBarHeight值获取不到
            systemInfo.statusBarHeight =
              systemInfo.screenHeight - systemInfo.windowHeight - 20;
          }
          rect = {
            //获取不到胶囊信息就自定义重置一个
            bottom: systemInfo.statusBarHeight + gap + 32,
            height: 32,
            left: systemInfo.windowWidth - width - 10,
            right: systemInfo.windowWidth - 10,
            top: systemInfo.statusBarHeight + gap,
            width: width,
          };
          console.log("error", error);
          console.log("rect", rect);
        }

        let navBarHeight = "";
        if (!systemInfo.statusBarHeight) {
          //开启wifi和打电话下
          systemInfo.statusBarHeight =
            systemInfo.screenHeight - systemInfo.windowHeight - 20;
          navBarHeight = (function () {
            let gap = rect.top - systemInfo.statusBarHeight;
            return 2 * gap + rect.height;
          })();

          systemInfo.statusBarHeight = 0;
          systemInfo.navBarExtendHeight = 0; //下方扩展4像素高度 防止下方边距太小
        } else {
          navBarHeight = (function () {
            let gap = rect.top - systemInfo.statusBarHeight;
            return systemInfo.statusBarHeight + 2 * gap + rect.height;
          })();
          if (ios) {
            systemInfo.navBarExtendHeight = 4; //下方扩展4像素高度 防止下方边距太小
          } else {
            systemInfo.navBarExtendHeight = 0;
          }
        }
        systemInfo.navBarHeight = navBarHeight; //导航栏高度不包括statusBarHeight
        systemInfo.capsulePosition = rect; //右上角胶囊按钮信息bottom: 58 height: 32 left: 317 right: 404 top: 26 width: 87 目前发现在大多机型都是固定值 为防止不一样所以会使用动态值来计算nav元素大小
        systemInfo.ios = ios; //是否ios
        wx.globalSystemInfo = systemInfo; //将信息保存到全局变量中,后边再用就不用重新异步获取了
        console.log("systemInfo", systemInfo);
        return systemInfo;
      }
    },
    setStyle(systemInfo) {
      const {
        statusBarHeight,
        navBarHeight,
        capsulePosition,
        navBarExtendHeight,
        ios,
        windowWidth,
      } = systemInfo;
      const { back, home, title, color } = this.data;

      let rightDistance = windowWidth - capsulePosition.right; //胶囊按钮右侧到屏幕右侧的边距
      // let leftWidth = windowWidth; //胶囊按钮左侧到屏幕右侧的边距
      let leftWidth = windowWidth - capsulePosition.left - 70; //胶囊按钮左侧到屏幕右侧的边距 - 返回按钮宽度

      let navigationbarinnerStyle = [
        `font-weight:${ios ? "bold" : "normal"}`,
        "width:100%",
        `color:${color}`,
        //`background:${background}`,
        `height:${navBarHeight + navBarExtendHeight}px`,
        "display:flex",
        "align-items:center",
        `padding-top:${statusBarHeight}px`,
        `padding-right:${leftWidth}px`,
        `padding-bottom:${navBarExtendHeight}px`,
      ].join(";");
      let navBarLeft = [];
      if ((back && !home) || (!back && home)) {
        navBarLeft = [
          `width:${capsulePosition.width}PX`,
          `height:${capsulePosition.height}PX`,
          `margin-left:0px`,
          `margin-right:${rightDistance}PX`,
        ].join(";");
      } else if ((back && home) || title) {
        navBarLeft = [
          `width:${capsulePosition.width}PX`,
          `height:${capsulePosition.height}PX`,
          `margin-left:0`,
          // `margin-left:${rightDistance}PX`,
        ].join(";");
      } else {
        navBarLeft = [`width:auto`, `margin-left:0px`].join(";");
      }
      this.triggerEvent("myevent", {
        barHeight:
          navBarHeight +
          navBarExtendHeight +
          statusBarHeight +
          navBarExtendHeight,
      });
      return {
        navigationbarinnerStyle,
        navBarLeft,
        navBarHeight,
        capsulePosition,
        navBarExtendHeight,
        ios,
        rightDistance,
      };
    },
  },
});
