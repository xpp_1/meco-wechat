import heloo from "../../utils/heloo";

// customize/components/prize-mask/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    prizeid: {
      type: Object,
    },
    wxid: {
      type: Number,
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    // 一元换购券
    isShowOne: true, // 中奖弹出层
    latitude: "", // 维度
    longitude: "", // 经度
    buildingList: [], // 门店列表
    isWrite: false, // 核销
    qrCodeImg: "", // 二维码
    testUrl: "https://ssl.meco.chinaxpp.com", //https://ssl.meco.chinaxpp.com
  },
  ready() {
    this.getAddressList();
    this.getQrCode(this.data.prizeid.id);
  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 获取门店列表
    getAddressList() {
      wx.getLocation({
        success(res) {
          console.log(res.longitude);
          wx.setStorageSync("longitude", res.longitude);
          wx.setStorageSync("latitude", res.latitude);
        },
      });
      let that = this;
      wx.showLoading({
        title: "加载中",
        mask: true,
      });
      wx.request({
        url: this.data.testUrl + "/api/xpppresent/range",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.wxid,
          longitude: wx.getStorageSync("longitude"),
          latitude: wx.getStorageSync("latitude"), //30.85752
        },
        success(res) {
          if (res.data.status == 1) {
            // 处理距离信息
            res.data.data.forEach((el) => {
              let num = parseInt(Number(el.distance));
              el.distance =
                num > 1000 ? parseInt(num / 1000) + "km" : num + "m";
            });

            that.setData({
              buildingList: res.data.data,
            });
            wx.hideLoading();
          }
        },
        complete() {
          wx.hideLoading();
        },
      });
    },
    // 获取二维码
    getQrCode(prizeId) {
      let that = this;
      wx.request({
        url: this.data.testUrl + "/api/xpppresent/verificationList",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.wxid,
          longitude: wx.getStorageSync("longitude"),
          latitude: wx.getStorageSync("latitude"),
          presentAddrId: prizeId,
        },
        success(res) {
          console.log("qrcode", res.data.data);
          that.setData({
            qrCodeImg: res.data.data,
          });
        },
      });
    },
    // 去兑换
    goExchange() {
      this.setData({
        isShowOne: false,
        isWrite: true,
      });
      wx.showLoading({
        title: "加载中",
        mask: true,
      });
    },
    // 一元换购
    closeOne() {
      // 发送参数 关闭遮罩层
      this.triggerEvent("closeOnePrize", {
        isShowOne: true,
      });
      //   初始化
      this.setData({
        isShowOne: true,
        isWrite: false,
      });
    },
    // 打电话
    callTel(e) {
      wx.makePhoneCall({
        phoneNumber: e.currentTarget.dataset.tel + "",
        success: function () {
          console.log("拨打电话成功！");
        },
      });
    },
    qrcodeSuccess(ecode) {
      console.log(ecode);
      wx.hideLoading();
    },
    qrcodeError() {
      wx.hideLoading();
    },
  },
});
