/**
 * @description 圣诞壁纸头像
 */
Component({
  data: {
  },
  properties: {
    size: String,
    bgType: String, // 壁纸类型 3、4展示不同圣诞帽
    avatarUrl:String,// 头像（当前用户头像/分享者头像）
    avatarName:String,// 昵称
  },
  methods: {
    // 打开红包
    openRedEnvelope() {
      console.log("open");
    },
  },
});
