/**
 * @description 圣诞壁纸头像
 */

Component({
  data: {
    info: {},
    proList: {
      default:
        "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/christmasBgDefault.png",
      1: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/christmasBg1.jpg",
      2: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/christmasBg2.png",
      3: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/christmasBg3.png",
      4: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/christmasBg4.png",
    },
  },
  properties: {
    size: String,
    bgType: String,
    hasSelected: Boolean, // 是否为已选择背景（高度展示1624rpx）
    userInfo: Object,
  },
  observers: {
    userInfo: function (value) {
      this.setData({
        info: value,
      });
    },
  },
  methods: {
    // 打开红包
    openRedEnvelope() {
      console.log("open");
    },
  },
});
