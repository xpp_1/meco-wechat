// pages/web/web.js
// 获取应用实例
import heloo from "../../utils/heloo";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: "",
    userInfo: null,
    otherunionid: "",
    h5Url: "",
    shareUrl:"",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("options",options)
    let that = this;
    if(options.appid){
      // console.log("appid",options);
      that.goMeco(options)
      // wx.navigateToMiniProgram({
      //   appId: options.appid,
      //   path:"pages/home/index?introduceID=990138357523&st=101&si=xinnianhuodong_Meco20211227",
      //   success(res){
      //     console.log("打开成功")
      //   },
      //   fail(res) {
      //     console.log("打开失败",res.errMsg);
      //     wx.navigateBack({
      //       delta: 0,
      //     })
      //   },
      // });
      return;
    }
    let h5Url = decodeURIComponent(options.url);
    this.setData({
      h5Url: h5Url
    })
    if (heloo.globalData.userInfo.unionid) {
      // 用户已登录并且是注册过的
      console.log("用户已登录并且是注册过的");
      that.setData({
        userInfo: heloo.globalData.userInfo
      })
      that.userLogin();
    } else {
      if (heloo.globalData.register&&heloo.globalData.register.unionid) {
        // 用户已登录未注册 弹出弹框提醒用户授权注册
        console.log("用户已登录未注册 弹出弹框提醒用户授权注册");
        wx.showModal({
          content: "您还未授权注册，是否进行注册？",
          confirmText: "是",
          cancelText: "否",
          success(res) {
            if (res.confirm) {
              that.getInfo();
            } else {
              wx.reLaunch({
                url: '../index/index',
              })
            }
          }
        })
      } else {
        heloo.wxLogin().then(() => {
          // 用户未登录，已注册
          console.log("用户未登录，已注册");
          that.setData({
            userInfo: heloo.globalData.userInfo
          })
          that.userLogin();
        }, () => {
          // 用户未登录，未注册  弹出弹框提醒用户授权注册
          console.log("用户未登录，未注册  弹出弹框提醒用户授权注册");
          wx.showModal({
            content: "您还未授权注册，是否进行注册？",
            confirmText: "是",
            cancelText: "否",
            success(res) {
              if (res.confirm) {
                that.getInfo();
              } else {
                wx.reLaunch({
                  url: '../index/index',
                })
              }
            }
          })
        })
      }
    }
  },

  // 拷贝用户信息接口
  userLogin() {
    let that = this;
    console.log(that.data.userInfo);
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: "https://xpph7.happydoit.com:8090/front/card/getunionid",
      method: "POST",
      header: {
        "Content-Type": "application/json;charset=UTF-8",
      },
      data: {
        userInfo: JSON.stringify(that.data.userInfo),
        unionid: that.data.userInfo.unionid,
        openid: that.data.userInfo.openid
      },
      success(data) {
        console.log("userLogin数据", data);
        wx.hideLoading();
        if (data.data.status == 1) {
          let url = that.data.h5Url;
          url = url + "&otherunionid=" + that.data.userInfo.unionid;
          console.log("h5-url", url);
          that.setData({
            url: url,
            otherunionid:that.data.userInfo.unionid
          });
        } else {

        }
      },
    });
  },

  // 授权
  getInfo() {
    let that = this;
    if (wx.canIUse("getUserProfile")) {
      wx.getUserProfile({
        lang: "zh_CN",
        desc: "本次授权用于个人信息显示",
        success(res) {
          console.log(res);
          wx.showLoading({
            title: "加载中...",
            mask: true,
          });
          let unionid = "";
          let openid = "";
          if (that.data.userInfo) {
            unionid = that.data.userInfo.unionid;
            openid = that.data.userInfo.openid;
          } else {
            unionid = heloo.globalData.register.unionid;
            openid = heloo.globalData.register.openid;
          }
          wx.request({
            url: heloo.globalData.url + "/api/getunionid",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              userInfo: JSON.stringify(res.userInfo),
              unionid: unionid,
              openid: openid,
              source: "集卡h5",
            },
            success(data) {
              console.log(data);
              wx.hideLoading();
              that.setData({
                userInfo: data.data.wxuser,
              });
              heloo.globalData.userInfo = data.data.wxuser;
              that.userLogin();
            },
          });
        },
      });
    } else {
      wx.showModal({
        content: "微信客户端版本过低，请更新后重试！",
        showCancel: false,
        confirmText: "我知道了",
      });
    }
  },

  // 跳转到meco官方小程序
  goMeco(options){
    let that = this;
    wx.showModal({
        content: "点击“确定”跳转至“Meco官方旗舰店”小程序",
        confirmText: "确定",
        cancelText: "取消",
        success(res) {
          if (res.confirm) {
            wx.navigateToMiniProgram({
              appId: options.appid,
              path:"pages/home/index?introduceID=990138357523&st=101&si=xinnianhuodong_Meco20211227",
              success(res){
                console.log("打开成功",res)
              },
              fail(res) {
                console.log("打开失败",res.errMsg);
                let currentTime = new Date().getTime();
                let url = "https://xpph7.happydoit.com/card/index.html?unionid=" + heloo.globalData.userInfo.unionid + "&currentTime=" + currentTime;
                that.setData({
                  url:url
                })
              },
            });
          } else {
            let currentTime = new Date().getTime();
            let url = "https://xpph7.happydoit.com/card/index.html?unionid=" + heloo.globalData.userInfo.unionid + "&currentTime=" + currentTime;
            that.setData({
              url:url
            })
          }
        }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  // 获取h5传输的数据
  getMessage(e){
    console.log(e);
    if(e.detail.data[0].sign == "index"){
      this.setData({
        shareUrl:e.detail.data[0].url + "?unionid=" + ""
      })
    }else if(e.detail.data[0].sign == "help"){
      this.setData({
        shareUrl:e.detail.data[0].url + "?unionid=" + this.data.userInfo.unionid
      })
    }else{
      this.setData({
        shareUrl:"https://xpph7.happydoit.com/card/index.html" + "?unionid=" + ""
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    // let url = this.data.shareUrl + "?unionid=" + this.data.userInfo.unionid;
    let url = this.data.shareUrl;
    if(!url){
      url = "https://xpph7.happydoit.com/card/index.html" + "?unionid=" + ""
    }
    console.log(encodeURIComponent(url))
    return {
      title: '集卡抽大奖',
      imageUrl:"https://meco-1259435766.cos.ap-shanghai.myqcloud.com/cardCollect/share.jpg",
      path: '/customize/pages/webView/webView?url=' + encodeURIComponent(url),
      success: (res) => {
        console.log("转发成功", e);
      },
      fail: (res) => {
        console.log("转发失败", e);
      }
    }
  }

})