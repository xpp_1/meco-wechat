/**
 * @description 来电接听
 */
import heloo from "../../utils/heloo";
import apiConstants from "../../server/apiConstants";
import {
  christmasBgList,
  defaultMusic,
  defaultMusicTime,
  defaultSenderAvatar,
} from "../../utils/constants";
Page({
  data: {
    senderAvatar: "", // 发送用户头像
    senderName: "", // 发送用户昵称
    userInfo: {}, // 用户信息
    navTitle: "Meco真茶局",
    innerAudioContext: null,
    audioUrl: "",
    audioTime: null, // 语音总时长 单位:ms
    audioStatus: "start", // 播放状态 播放中:playing 开始:start 结束:end  暂停:pause 重新播放:rePlay
    curAudioTime: 0, // 当前播放时长
    timer: null,
    secondes: 0,
    memberNum: 3,
    memberList: [], // 开现金成员,
    envelopStatus: "progress", // progress 待集齐 success 待领取 received 已领取 receivedAll 已领完 disabled: 无法领取 disabledReceived:失效后打开红包
    redEndTip: "", // 无法领取红包文案
    bgType: "",
    bgList: christmasBgList,
    sponsorSeq: "", // 活动key
    sponsor: {}, // 发起人
    powerFlag: true, // 当前用户能否进行助力
    hasRedBag: false, // 是否有分享红包
    options: {},
    redBagInfo: {}, // 红包信息
    curUserMoney: 0, // 当前用户红包金额
    receivedNum: 0, // 已领取人数
    hasPowerFull: false,
    hasOpen: false, // 是否点过开
    isPowerMember: false, // 当前用户是否助力成员
    verifyShow: false, // 滑动验证
  },
  /**
   *监听页面滚动
   */
  onPageScroll(e) {
    if (e.scrollTop > 30) {
      this.setData({
        navTitle: "",
      });
    } else {
      this.setData({
        navTitle: "Meco真茶局",
      });
    }
  },
  onLoad(options) {
    console.log(heloo.globalData.userInfo, "userinfo____", heloo.globalData);
    this.setData({
      options,
    });
    const { bgType, sponsorSeq, shareType } = options;
    if (sponsorSeq) {
      // 非默认meco祝福
      this.init(sponsorSeq);
    } else {
      // 默认
      this.setData({
        audioUrl: defaultMusic,
        senderAvatar: defaultSenderAvatar,
        senderName: "Meco",
        audioTime: defaultMusicTime * 1000,
        bgType: "default",
      });
    }

    this.setData({
      shareType,
      sponsorSeq,
      bgType,
      userInfo: heloo.globalData.userInfo,
    });
  },
  onShow() {
    const innerAudioContext = wx.createInnerAudioContext({
      // autoPlay: true,
      useWebAudioImplement: true,
    });
    if (!this.data.options.sponsorSeq) {
      innerAudioContext.autoplay = true;
      innerAudioContext.src = defaultMusic;
    }
    innerAudioContext.onPlay(() => {
      console.log("开始播放");
      //开始播放计时
      this.countDown();
      this.setData({
        audioStatus: "playing",
      });
    });
    innerAudioContext.onStop(() => {
      console.log("已播放结束");
      this.setData({
        audioStatus: "stop",
      });
    });
    innerAudioContext.onError((res) => {
      console.log(res.errMsg);
      console.log(res.errCode);
    });
    this.setData({
      innerAudioContext,
    });
  },
  onHide() {
    this.cancelMsg();
  },
  onUnload() {
    this.cancelMsg();
  },
  cancelMsg() {
    clearInterval(this.data.timer);
    this.data.innerAudioContext.destroy();
    this.setData({
      innerAudioContext: null,
      curAudioTime: 0,
      progressWidth: 0,
      secondes: 0,
      audioStatus: "start",
      timer: null,
    });
  },
  async init(id) {
    await this.getActivityDetail(id, "init");
    if (this.data.hasRedBag && this.data.powerFlag) {
      // 带有分享红包&& 助力人数未到 && 未助力
      this.addPower();
    }
  },
  getActivityDetail(id, type) {
    let _this = this;
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_GET_CHRISTMAS_CALL_DETAIL,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          sponsorSeq: id,
          openid: heloo.globalData.userInfo.openid,
        },
        success(data) {
          if (data.data.status == 1) {
            const {
              countCanOpenRedBag,
              myCanOpenRedBag,
              resource,
              users,
              redBagInfo,
            } = data.data.data;
            console.log(myCanOpenRedBag, "myCanOpenRedBag");

            _this.setData({
              bgType: resource.backgroundPictureId,
            });
            // 助力人员
            const usersArr = users;
            // 红包领取状态
            const redInfo = redBagInfo;

            if (type && type === "init") {
              _this.data.innerAudioContext.autoplay = true;
              _this.data.innerAudioContext.src = resource?.soundResourcesId;
            }
            _this.setData({
              audioTime: resource?.soundLen * 1000,
              audioUrl: resource?.soundResourcesId,
              redBagInfo: redInfo,
            });
            // 是否助力列表成员
            let isPowerMember = false;
            if (users && users.length > 0) {
              users.forEach((user) => {
                if (user.openid === _this.data.userInfo.openid) {
                  isPowerMember = true;
                }
              });
              if (users.length === 4) {
                // 助力已集齐
                if (countCanOpenRedBag === "false") {
                  // 4W个红包已领完
                  _this.setData({
                    redEndTip: "活动红包4W个已领完，慢了一步哦",
                    envelopStatus: "disabled",
                  });
                } else if (myCanOpenRedBag === "false") {
                  //用户是否已领取两次红包限制
                  _this.setData({
                    redEndTip:
                      "您已领取两次红包，按照活动规则约定，您不能再领取红包",
                    envelopStatus: "disabled",
                  });
                }
              }
            }
            _this.setData({
              isPowerMember,
            });

            let receivedNum = 0; // 已领取人数
            if (redInfo?.data && redInfo?.data.length > 0) {
              const newUsersArr = redInfo.data.reduce((acc, cur) => {
                const target = acc.find((e) => e.openid === cur.openid);
                if (cur?.sendTime) {
                  cur.sendTime = cur.sendTime.substring(5, 19);
                }
                if (target) {
                  Object.assign(target, cur);
                } else {
                  acc.push(cur);
                }
                if (cur.sendStatus === 1) {
                  receivedNum++;
                }
                return acc;
              }, usersArr);
              _this.setData({
                memberList: newUsersArr,
                receivedNum,
              });
              // 当前用户红包状态
              const redStatus = redInfo.data.filter(
                (item) => item.openid === _this.data.userInfo.openid
              );
              if (
                redStatus &&
                redStatus.length > 0 &&
                countCanOpenRedBag === "true" &&
                myCanOpenRedBag === "true"
              ) {
                let tip;
                switch (redStatus[0].sendStatus) {
                  case -6:
                    tip = "活动红包4W个已领完，慢了一步哦";
                    break;
                  case -7:
                    tip = "红包超过领取时间，已失效";
                    break;
                  case -8:
                    tip =
                      "您已领取两次红包，按照活动规则约定，您不能再领取红包";
                    break;
                }
                _this.setData({
                  redEndTip: tip,
                  envelopStatus: "disabled",
                });
              }
              _this.setData({
                curUserMoney: redStatus[0]?.money || 0,
              });
              if (
                redStatus &&
                redStatus.length > 0 &&
                redStatus[0].sendStatus === 1
              ) {
                _this.setData({
                  envelopStatus: "received",
                  curUserMoney: redStatus[0].money,
                });
              } else if (
                redStatus &&
                redStatus.length > 0 &&
                redStatus[0].sendStatus === 0 &&
                redInfo.status === 1
              ) {
                _this.setData({ envelopStatus: "success" });
              } else if (
                redStatus &&
                redStatus.length > 0 &&
                redStatus[0].sendStatus === "-3" &&
                countCanOpenRedBag === "true" &&
                myCanOpenRedBag === "true"
              ) {
                // 红包已过期
                _this.setData({
                  redEndTip: "红包超过领取时间，已失效",
                  envelopStatus: "disabled",
                });
              }
            }
            // 助力信息
            _this.refreshPower(usersArr, receivedNum);
            resolve(data.data.data.redBagInfo);
          } else {
            console.log("error___");
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
          }
        },
        fail(res) {
          console.log(res, "error");
          _this.setData({
            loading: false,
          });
        },
      });
    });
  },
  // 更新助力情况
  refreshPower(usersArr) {
    let sponsor = {};
    let primaryUser = [];
    let restUser = [];
    let hasPowerFull = false; // 助力已满但当前用户未助力
    if (usersArr && usersArr.length > 0) {
      usersArr.forEach((user) => {
        if (user.sponsorType === "1") {
          sponsor = user;
        } else {
          primaryUser.push(user);
        }
        if (
          user.openid === this.data.userInfo.openid ||
          usersArr.length >= this.data.memberNum + 1
        ) {
          // 判断当前是否已助力用户 || 助力人数已满
          this.setData({
            powerFlag: false,
          });
        }
      });

      if (usersArr.length >= this.data.memberNum + 1) {
        hasPowerFull = usersArr.some(
          (item) => item.openid === this.data.userInfo.openid
        );
        if (!hasPowerFull) {
          // 助力人数集齐后 其他用户进入展示助力已满
          wx.showToast({
            title: "助力已满",
            icon: "none",
          });
          this.setData({
            envelopStatus: "progress",
            hasPowerFull: !hasPowerFull,
          });
        }
      }

      if (primaryUser.length < this.data.memberNum) {
        restUser = new Array(this.data.memberNum - primaryUser.length); // 仍需助力人数
      }

      this.setData({
        sponsor,
        primaryUser,
        restUser,
        hasRedBag: true, // 发起人红包不限次数
        // hasRedBag: sponsor.sponsorBagType === "1", // 1:带有分享红包 2:超出分享次数限制 废弃
        senderAvatar: sponsor.header,
        senderName: sponsor.nickname,
      });
    }
  },
  // 助力
  addPower() {
    let _this = this;
    const {
      openid,
      country,
      province,
      city,
      nickname,
      sex,
      unionid,
      header,
      qunMemberId,
    } = heloo.globalData.userInfo;
    const { xpp_phone, xpp_birthday } = heloo.globalData;
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_ADD_POWER,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          openid,
          country,
          province,
          city,
          nickname,
          sex,
          phone: xpp_phone,
          birthday: xpp_birthday,
          unionid,
          header,
          phoneNumber: xpp_phone,
          backgroundPicture: this.data.bgType,
          qunMemberId,
          activityId: 9, // 写死
          resourcesId: this.data.audioUrl,
          sponsorSeq: this.data.sponsorSeq,
        },
        success(data) {
          if (data.data.status == 1) {
            // const usersArr = data.data.data.users;
            // _this.refreshPower(usersArr);
            _this.getActivityDetail(_this.data.sponsorSeq);
            if (data.data.data?.helpCode === -1) {
              wx.showToast({
                title: "您的10次助力已用完，看看其他活动吧",
                icon: "none",
                duration: 2000,
              });
            } else {
              wx.showToast({
                title: "助力成功",
                icon: "none",
              });
            }
            resolve();
          } else {
            console.log("error___");
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
          }
        },
        fail(res) {
          console.log(res, "error");
          _this.setData({
            loading: false,
          });
        },
      });
    });
  },
  goHome() {
    wx.redirectTo({
      url: "/customize/pages/index/index",
    });
  },
  // 播放
  onPlayAudio() {
    const { audioStatus } = this.data;
    switch (audioStatus) {
      case "start":
        this.data.innerAudioContext.autoplay = true;
        this.data.innerAudioContext.src = this.data.audioUrl;
        break;
      case "playing":
        this.data.innerAudioContext.pause();
        this.setData({
          audioStatus: "pause",
        });
        clearInterval(this.data.timer);
        break;
      case "pause":
        this.data.innerAudioContext.play();
        break;
      case "stop":
        this.setData({
          curAudioTime: 0,
          progressWidth: 0,
          secondes: 0,
        });
        this.data.innerAudioContext.play();
        break;
    }
  },
  countDown() {
    this.data.timer = setInterval(() => {
      this.data.secondes += 100;
      if (this.data.secondes > this.data.audioTime) {
        clearInterval(this.data.timer);
        this.data.innerAudioContext.stop();
        this.setData({
          audioStatus: "stop",
        });
        return;
      }
      const progressWidth = (this.data.secondes / this.data.audioTime) * 100;
      this.setData({
        curAudioTime: Math.round(this.data.secondes / 1000),
        progressWidth: progressWidth >= 100 ? 100 : progressWidth,
      });
    }, 100);
  },
  linkToMake() {
    heloo.globalData.christmasVoice = "";
    heloo.globalData.christmasBg = "";
    wx.navigateTo({
      url: "/customize/pages/christmasCallMake/index",
    });
  },
  // 开红包
  openRedEnvelope() {
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    let _this = this;
    const { sponsorSeq } = this.data.options;
    if (_this.data.hasOpen) {
      _this.setData({
        envelopStatus: "received",
      });
      return;
    }
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_OPEN_RED_BAG,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          sponsorSeq,
          openid: heloo.globalData.userInfo.openid,
        },
        success(data) {
          wx.hideLoading();
          if (data.data.status > 0) {
            // 开完一次后不再开红包
            _this.setData({
              envelopStatus: "received",
              hasOpen: true,
            });
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            resolve();
          } else {
            console.log("error___");
            let redEndTip = "";
            switch (data.data.status) {
              case -6:
                redEndTip = "活动红包4W个已领完，慢了一步哦";
                break;
              case -7:
                redEndTip = "红包超过领取时间，已失效";
                break;
              case -8:
                redEndTip =
                  "您已领取两次红包，按照活动规则约定，您不能再领取红包";
                break;
            }
            _this.setData({
              redEndTip,
              envelopStatus: "disabled",
            });
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
          }
        },
        fail(res) {
          console.log(res, "error");
          wx.hideLoading();
        },
      });
    });
  },
  openVerify() {
    this.setData({
      verifyShow: true,
    });
  },
  async open() {
    await this.openRedEnvelope();
    await this.getActivityDetail(this.data.options.sponsorSeq);
  },

  getNavBarHeight(e) {
    this.setData({ barHeight: e.detail.barHeight });
  },

  openDisabledBag() {
    this.setData({
      envelopStatus: "disabledReceived",
    });
  },
  linkToChristmasRule() {
    wx.navigateTo({
      url: "/customize/pages/christmasRule/index",
    });
  },
  linkToActivity() {
    wx.navigateTo({
      url: "/customize/pages/christmasActivity/index",
    });
  },
  onVerifySuccess(e) {
    console.log(e, "e");
    if (e.detail) {
      // 验证成功
      this.setData({
        verifyShow: false,
      });
      this.open();
    }
  },
  onCloseVerify() {
    this.setData({
      verifyShow: false,
    });
  }
});
