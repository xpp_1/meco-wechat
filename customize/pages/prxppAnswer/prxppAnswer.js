const http = require("../../prxppUtils/prxpp_httputil")
const util = require("../../prxppUtils/prxpp_util")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ques_type: "",
    ques_str: "",
    ans_list: [],
    ansID: -1,
    currentChooseIndex: -1,
    isShow: false,
    isCorrect: -1,
    chanceStr: '',
    isShowButton: true,
    prizeID: -1,
    prizeStr: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('ansid = ' + options.ansid)

    var that = this;

    http.prxpp_postRequest({
      url: 'answer/topic/get',
      params: {
        id: options.ansid
      },
      success: data => {

        var ansList = data.data.answer.option;
        var myList = new Array();
        var questionList = new Array();
        questionList = data.data.answer.topic.split('#');
        var quesStr = '';

        for (var i = 0; i < ansList.length; i++) {
          myList.push({
            id: i,
            index: ansList[i].index,
            name: ansList[i].value,
            isChecked: false
          })
        }

        for (var j = 0; j < questionList.length; j++) {
          quesStr = quesStr + questionList[j] + '\n\n';
        }

        that.setData({
          ansID: data.data.answer.id,
          ques_type: data.data.answer.subject,
          ques_str: quesStr,
          ans_list: myList
        })

      },
      failed: errMsg => {
        console.log(errMsg);
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  ans_click: function (e) {

    var data = this.data.ans_list

    for (let i = 0; i < data.length; i++) {
      data[i].isChecked = false
    }

    data[e.currentTarget.dataset.index].isChecked = true
    this.setData({
      ans_list: data
    })

    this.data.currentChooseIndex = this.data.ans_list[e.currentTarget.dataset.index].index
    console.log("currentChooseIndex:" + this.data.currentChooseIndex)
  },

  ans_submit: function () {
    console.log("ans_submit")
    var that = this;
    http.prxpp_postRequest({
      url: 'answer/topic/submit',
      params: {
        answer_id: this.data.ansID,
        option: this.data.currentChooseIndex
      },
      success: data => {
        console.log(data);

        var prizeTxt = '';
        if (data.data.prize == 1) {
          // 得到积分
          prizeTxt = data.data.prize_string + "积分"
        } else if (data.data.prize == 2)(
          // 得到红包
          prizeTxt = data.data.prize_string + "元"
        )

        if (util.phoneNumber) {
          // 本地存在手机号
          console.log('------本地存在手机号-----');
          that.setData({
            isShowButton: false,
            isShow: true,
            isCorrect: data.data.is_correct,
            chanceStr: data.data.power_string,
            prizeID: data.data.prize_id,
            prizeStr: prizeTxt
          })
        } else {
          // 本地不存在手机号，刷新key，点击唤起 获取手机号界面
          console.log('------本地不存在手机号-----');
          util.login().then(result => {
            that.setData({
              isShow: true,
              isCorrect: data.data.is_correct,
              chanceStr: data.data.power_string,
              prizeID: data.data.prize_id,
              prizeStr: prizeTxt
            })
          }).catch(error => {
            console.error(error)
          });
        }

      },
      failed: errMsg => {
        console.log(errMsg);
      }
    })
  },

  getPhoneNumber(e) {
    console.log('------getPhoneNumber-----');
    var that = this;
    util.dncryptPhoneNumber(e.detail.encryptedData, e.detail.iv).then(_ => {
      wx.reLaunch({
        url: '../prxppGetPrize/prxppGetPrize?id=' + that.data.prizeID
      })
    }).catch(err => {
      console.error(err)
    })
  },
  goGetPrize(){
    console.log('------goGetPrize-----');
    wx.reLaunch({
      url: '../prxppGetPrize/prxppGetPrize?id=' + this.data.prizeID
    })
  },
  clickToShare() {
    console.log('------clickToShare-----');
    http.prxpp_postRequest({
      url: 'answer/help/join',
      success: data => {
        console.log(data);
        wx.navigateTo({
          url: '../prxppShare/prxppShare?id=' + data.data.id,
        })
      },
      failed: errMsg => {
        console.log(errMsg);
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})