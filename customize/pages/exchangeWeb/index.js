import heloo from "../../utils/heloo";
Page({
  data: {
    url: "",
  },
  onReady: function () {
    if (!heloo.globalData?.userInfo?.unionid) {
      wx.showToast({
        title: "请先授权登录～",
        icon: "none",
      });
      setTimeout(() => {
        wx.redirectTo({
          url: "/customize/pages/index/index",
        });
      }, 1500);
    } else {
      const url =
        "https://xppduiba.happydoit.com/back/persona/getUrl?uid=" +
        heloo.globalData.userInfo.unionid;
      this.setData({
        url,
      });
    }
  },
});
