import {
  baseConfig
} from "../../utils/mecoBaseConfig.js"

import {
  scrmLoinSaveData
} from "../../utils/mecoUserInfo"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    defaultSelect: 0,
    loadMore: true,
    page: 0,
    size: 10,
    shopPage: 1,
    shopSize: 10,
    cardTotalPages: null,
    shopTotalPages: null,
    list: [],
    city: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if (options.selectId) {
      this.setData({
        defaultSelect: options.selectId
      })
    }
    if (this.data.defaultSelect == 0) {
      this.getList()
    } else {
      // if (!wx.getStorageSync('mecoLatitude')) {
      //   this.getUserLocation()
      // }
      this.getNearShop()
    }
    if (!wx.getStorageSync('SCRM_VO')) {
      this.getScrmUserInfo()

    }
  },
  //scrm授权
  getScrmUserInfo() {
    scrmLoinSaveData().then(res => {
      if (!wx.getStorageSync('mecoLatitude')) {
        return
      }
      this.getNearShop()
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },
  toggle() {
    this.setData({
      list: [],
      page: 0,
      shopPage: 1,
      size: 10
    })
    if (this.data.defaultSelect == 0) {
      this.setData({
        defaultSelect: 1
      })
      if (!wx.getStorageSync('mecoLatitude')) {
        this.getUserLocation()
        return
      }
      this.getNearShop()
    } else if (this.data.defaultSelect == 1) {
      this.setData({
        defaultSelect: 0
      })
      this.getList()
    }
  },
  //滑动加载
  loadMore() {
    this.setData({
      loadMore: false
    })
    if (this.data.defaultSelect == 0 && (this.data.page + 1) == this.data.cardTotalPages) {
      return
    } else {
      this.setData({
        page: this.data.page + 1,
      })
      this.getList()
    }
  },
  loadMoreShop() {
    if (this.data.defaultSelect == '1' && (this.data.shopPage * this.data.shopSize) >= this.data.shopTotalPages) {
      return false
    } else {
      this.setData({
        page: this.data.shopPage + 1,
      })
      this.getNearShop()
    }
  },
  //券码list
  getList() {
    let that = this
    this.setData({
      loadMore: true
    })
    wx.showLoading()
    wx.request({
      url: baseConfig.UCODE_URL + `/user/v2/lotteryRecords?page=${this.data.page}&size=10&longitude=${wx.getStorageSync('mecoLongitude')}&latitude=${wx.getStorageSync('mecoLatitude')}`,
      method: "POST",
      header: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + wx.getStorageSync('UMTOKEN'),
      },
      data: {
        matchParams: [{
          recordType: 'HIT_LOTTERY_RECORD'
        }]
      },
      success(res) {
        wx.hideLoading()
        if (res.statusCode == 200) {
          that.setData({
            list: that.data.list.concat(res.data.content),
            cardTotalPages: res.data.totalPages
          })
          console.log(res.data.content, that.data.list, '券列表');
        }
      },
    });
  },
  //附近店铺
  getNearShop() {
    //
    let that = this
    this.setData({
      loadMore: true
    })
    wx.showLoading({
      mask: true
    })
    //经纬度逆解析
    wx.request({
      url: baseConfig.BASE_URL + '/meco/reverse',
      method: 'POST',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: wx.getStorageSync('SCRM_TOKEN'),
      },
      data: {
        'reverseGeoParam.longitude': wx.getStorageSync('mecoLongitude'),
        'reverseGeoParam.latitude': wx.getStorageSync('mecoLatitude')
      },
      success(res) {
        wx.hideLoading()
        that.setData({
          city: res.data.data.data.city
        })
        //查询附近位置
        that.nearShop()
      },
      fail() {
        wx.hideLoading()
      }
    })

  },
  //腾讯地图转百度
  qqMapToBMap(gg_lng, gg_lat) {
    //调用百度地图地址解析
    return new Promise((resolve, reject) => {
      wx.request({
        url: `https://api.map.baidu.com/geoconv/v1/?coords=${gg_lng},${gg_lat}&from=3&to=5&ak=1lpeNEGZoAXNkKlAiQoGiMdkjjgHNslX`,
        method: 'GET',
        success: (result) => {
          if (result.statusCode == 200) {
            const resResult = result?.data?.result[0]
            resolve([resResult.x, resResult.y])
          } else {
            wx.showModal({
              title: '提示',
              content: '位置解析失败，请稍后再试',
            })
            reject()
          }
        },
        fail: (err) => {
          console.log(err, 'err')
          wx.showModal({
            title: '提示',
            content: JSON.stringify(err),
          })
          reject()
        },
      })
    })
  },
  //附近店铺
  nearShop() {
    let that = this
    wx.showLoading({
      mask: true
    })
    this.qqMapToBMap(wx.getStorageSync('mecoLongitude'), wx.getStorageSync('mecoLatitude')).then(resp => {
      wx.request({
        url: baseConfig.BASE_URL + `/meco/nearStore`,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: wx.getStorageSync('SCRM_TOKEN'),
        },
        data: {
          'dto.param.longitude': resp[0],
          'dto.param.latitude': resp[1],
          'dto.param.openId': wx.getStorageSync('SCRM_VO').openId,
          'dto.param.actId': '1002154', //活动id，暂时写死
          'dto.param.city': that.data.city,
          "dto.pageNum": that.data.shopPage,
          "dto.pageSize": that.data.shopSize,
        },
        success(res) {
          wx.hideLoading()
          if (res.statusCode == 200) {
            that.setData({
              list: that.data.list.concat(res.data.data.data.list),
              shopTotalPages: res.data.data.data.total
            })
          }
        },
        fail() {
          wx.hideLoading()
        }
      });
    })

  },
  //百度转qq
  bMapToQQMapArr(posArr) {
    //调用百度地图地址解析
    let posStr = posArr.join(';')
    return new Promise((resolve, reject) => {
      wx.request({
        url: `https://api.map.baidu.com/geoconv/v1/?coords=${posStr}&from=5&to=3&ak=1lpeNEGZoAXNkKlAiQoGiMdkjjgHNslX`,
        method: 'GET',
        success: (result) => {
          if (result?.data?.status == 0) {
            const resResult = result?.data?.result
            resolve(resResult)
          } else {
            wx.showModal({
              title: '提示',
              content: '位置解析失败，请稍后再试',
            })
            reject()
          }
        },
        fail: (err) => {
          console.log(err, 'err')
          wx.showModal({
            title: '提示',
            content: JSON.stringify(err),
          })
          reject()
        },
      })
    })

  },
  //查看位置
  viewAddress(e) {
    let param = [`${e.currentTarget.dataset.longitude},${e.currentTarget.dataset.latitude}`, ]
    this.bMapToQQMapArr(param).then(res => {
      console.log(res);
      wx.openLocation({
        longitude: res[0].x,
        latitude: res[0].y,
        scale: 18
      })
    })
    return

  },
  //立即核销
  gotoVer(e) {
    const {
      promotionId,
      uuid,
      code,
      codeUri
    } = e.currentTarget.dataset.prizeinfo
    wx.navigateTo({
      url: '../mecoPrise/index?promotionId=' + promotionId + '&uuid=' + uuid + "&code=" + code + "&uri=" + codeUri + "&source=list",
    })
  },
  //首页直接跳转进来
  getUserLocation() {
    let that = this
    wx.getLocation({
      type: "gcj02",
      isHighAccuracy: true,
      highAccuracyExpireTime: 3000,
      success: function (res) {
        const latitude = res.latitude
        const longitude = res.longitude
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
        });
        wx.setStorageSync('mecoLatitude', latitude)
        wx.setStorageSync('mecoLongitude', longitude)
        that.setData({
          list: [],
          shopPage: 1,
          shopSize: 10
        })
        that.getNearShop()
      },
      fail: function (err) {
        wx.getSetting({
          success: (res) => {
            if (
              typeof res.authSetting["scope.userLocation"] != "undefined" &&
              !res.authSetting["scope.userLocation"]
            ) {
              // 用户拒绝了授权
              wx.showModal({
                title: "提示",
                content: "您拒绝了位置授权，可能无法参与部分活动，是否立即授权？",
                success: (res) => {
                  if (res.confirm) {
                    //打开设置 让用户点击允许 这样可以重新获取
                    wx.openSetting({
                      success: (res) => {
                        if (res.authSetting["scope.userLocation"]) {
                          // 授权成功，重新定位即可
                          wx.getLocation({
                            type: "gcj02",
                            isHighAccuracy: true,
                            highAccuracyExpireTime: 3000,
                            success: (res) => {

                              that.setData({
                                latitude: res.latitude,
                                longitude: res.longitude,
                              });
                              that.getNearShop()
                            },
                            fail: (err) => {
                              console.log(err);
                            },
                          });
                        } else {
                          // 没有允许定位权限
                          wx.showToast({
                            title: "没有允许定位权限",
                            icon: "none",
                          });
                        }
                      },
                    });
                  }
                },
              });
            } else {
              that.getNearShop()
            }
          },
        });
      },
    });
  },
  commonBack() {
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})