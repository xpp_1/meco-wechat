// customize/pages/mecoRule/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    uri: '',
    code: '',
    hideBtn: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // this.splitCode({
    //   q: 'HTTP://UT.AAX6.CN/TX/8-$:$-W6:B$WOL:RQ'
    // })
    this.splitCode({
      q: options.q
    })
    this.getUserLocation()
    if (options.hideBtn) {
      this.setData({
        hideBtn: false
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },
  accept() {
    wx.showLoading({
      title:'加载中'
    })
    let code = this.data.code
    let uri = this.data.uri
    wx.navigateTo({
      url: '../mecoPrise/index?code=' + code + '&uri=' + uri,
    })
    wx.hideLoading()
  },
  refuse() {
    wx.navigateBack()
    // wx.navigateBackMiniProgram()
  },
  splitCode(options) {
    if (options.q) { //扫码（U码）进入
      const arr = decodeURIComponent(options.q).split("/")
      const code = arr[4]
      const uri = arr[2] + '/' + arr[3]
      this.setData({
        uri,
        code,
      })
    }
    if (!options.q) {
      wx.reLaunch({
        url: "/customize/pages/home/index"
      })
    }
  },
  getUserLocation() {
    let that = this
    wx.getLocation({
      type: "gcj02",
      isHighAccuracy: true,
      highAccuracyExpireTime:3000,
      success: function (res) {
        const latitude = res.latitude
        const longitude = res.longitude
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
        });
        wx.setStorageSync('mecoLatitude', latitude)
        wx.setStorageSync('mecoLongitude', longitude)
      },
      fail: function (err) {
        wx.getSetting({
          success: (res) => {
            if (
              typeof res.authSetting["scope.userLocation"] != "undefined" &&
              !res.authSetting["scope.userLocation"]
            ) {
              // 用户拒绝了授权
              wx.showModal({
                title: "提示",
                content: "您拒绝了位置授权，可能无法参与部分活动，是否立即授权？",
                success: (res) => {
                  if (res.confirm) {
                    //打开设置 让用户点击允许 这样可以重新获取
                    wx.openSetting({
                      success: (res) => {
                        if (res.authSetting["scope.userLocation"]) {
                          // 授权成功，重新定位即可
                          wx.getLocation({
                            altitude: true,
                            isHighAccuracy: true,
                            success: (res) => {
                              that.setData({
                                latitude: res.latitude,
                                longitude: res.longitude,
                              });
                            },
                            fail: (err) => {
                              console.log(err);
                            },
                          });
                        } else {
                          // 没有允许定位权限
                          wx.showToast({
                            title: "没有允许定位权限",
                            icon: "none",
                          });
                        }
                      },
                    });
                  }
                },
              });
            }
          },
        });
      },
    });
  },
  commonBack() {
    wx.navigateBack()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})