// pages/map/map.js
// const app = getApp();
import heloo from '../../utils/heloo';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        url:""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this;
        let openid = heloo.globalData.openid;
        if(!openid){
            openid = heloo.globalData.userInfo.openid;
        }
        let session_key = heloo.globalData.userKey;
        wx.getWeRunData({
          success: (res) => {
            that.setData({
                url:`https://ssl.meco.chinaxpp.com/static/mecomap/index.html?openid=${openid}&session_key=${session_key}&encryptedData=${res.encryptedData}&iv=${res.iv}`
            })
          },
          fail(){
            wx.showModal({
                title: '获取步数失败',
                content: '尚未授权获取步数，是否前往？',
                cancelText:"否",
                confirmText:"是",
                success:function(res){
                  if(res.confirm){
                    wx.openSetting({
                      
                    })
                  }
                }
              })
          }
        })
        
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
      return {
        title: 'Meco真茶局',
        path: '/customize/pages/index/index',
        // imageUrl: "/images/shareicon.jpg"
      }
    }
})