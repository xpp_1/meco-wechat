import { Setting } from '../../../engine/index';

Page({
  data: {
    // 以下为 tabbar 所需 data fields
    tabbarSetting: null,
    tabbarIndex: -1,
    pageId: '',
  },
  async onLoad(options) {
    // 页面初始化 tab bar 方法
    await this.initTabbarSetting(options);
    // 其他业务代码
  },
  onShow() {
    wx.hideHomeButton();
  },
  async initTabbarSetting(options) {
    // const { mai } = getApp();
    const { usR } = getApp();
const mai = {
  member:usR
}
    if (!mai.member.getOpenId()) {
      await mai.member.signin();
    }

    const tabbarSetting = await Setting.getTabbarSetting();
    const pages = getCurrentPages();
    const pagePath = `/${pages[pages.length - 1].route}`;
    const tabbarIndex = Setting.getTabbarIndex(tabbarSetting, 'home', '', pagePath);
    this.setData({
      tabbarSetting,
      tabbarIndex,
      pageId: options.pageId || '',
    });
  },
});
