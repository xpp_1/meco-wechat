const http = require("../../prxppUtils/prxpp_httputil")
const util = require("../../prxppUtils/prxpp_util")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    award_list: [],
    prizeStr: '',
    isShowButton: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    http.prxpp_postRequest({
      url: 'answer/prize/list',
      success: data => {

        console.log(data);
        var alist = data.data.list;
        var myList = new Array();
        for (var i = 0; i < alist.length; i++) {
          var myAwardStr = '';
          if (alist[i].prize == 1) {
            myAwardStr = alist[i].prize_string + "积分"
          } else if (alist[i].prize == 2) {
            myAwardStr = alist[i].prize_string + "元"
          }
          myList.push({
            id: alist[i].id,
            award: myAwardStr,
            time: alist[i].created_at,
            type: alist[i].prize,
            state: alist[i].is_receive
          })
        }

        that.setData({
          award_list: myList
        })

      },
      failed: errMsg => {
        console.log(errMsg);
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (util.phoneNumber) {
      this.setData({
        isShowButton: false
      })
    } else {
      this.setData({
        isShowButton: true
      })
    }
  },

  getPhoneNumber(e) {
    console.log('------getPhoneNumber 1-----');
    var that = this;
    util.dncryptPhoneNumber(e.detail.encryptedData, e.detail.iv).then(_ => {
      console.log('------getPhoneNumber 2-----');
      that.setData({
        isShowButton: false
      })
    }).catch(err => {
      console.log('------getPhoneNumber 3-----');
      console.error(err)
    })
  },
  clickToGetPrize(e) {
    console.log('------clickToGetPrize-----');
    console.log('id = ' + e.currentTarget.dataset.id);
    wx.reLaunch({
      url: '../prxppGetPrize/prxppGetPrize?id=' + e.currentTarget.dataset.id
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})