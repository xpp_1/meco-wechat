// customize/pages/scanCode/scanCode.js
import heloo from "../../utils/heloo";
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    let currentTime = new Date().getTime();
    let url = "https://xpph7.happydoit.com/card/index.html?unionid=" + heloo.globalData.userInfo.unionid + "&currentTime=" + currentTime;
    that.setData({
      url:url
    })
    that.scanCode()
  },

  scanCode(){
    //兼容ios微信无法立即调起扫一扫
    // setTimeout(function () {
      wx.scanCode({//调用扫一扫
        success: function (res) {//扫码成功的回调函数
          console.log(res);
          let path = res.path;
          console.log(path);
          wx.navigateTo({
            url: '/' + path,
          })
        },
        error: function (err) {//扫码失败的回调函数
          console.log('err');
          // wx.redirectTo({//失败的话，直接重定向到页面，并且不带任何参数
          //   url: '../index/index?url=https://www.hxkj.vip/'
          // })
        }
      })
    // }, 500)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    let url = "https://xpph7.happydoit.com/card/index.html" + "?unionid=" + "";
    return {
      title: '集卡抽大奖',
      imageUrl:"https://meco-1259435766.cos.ap-shanghai.myqcloud.com/cardCollect/share.jpg",
      path: '/customize/pages/webView/webView?url=' + encodeURIComponent(url),
      success: (res) => {
        console.log("转发成功", e);
      },
      fail: (res) => {
        console.log("转发失败", e);
      }
    }
  }
})