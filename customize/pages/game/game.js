// pages/game/game.js
const app = getApp();
import heloo from '../../utils/heloo';
Page({
    data: {
        userInfo: null,
        list: [{
                img: "../../images/game/game_syn.png",
                url: "https://ssl.meco.chinaxpp.com/watermalon/index.html"
            }, 
            // {
            //     img: "../../images/game/game_question.png",
            //     url: "https://ssl.meco.chinaxpp.com/doll/game.html"
            // }
        ],
        list2: [
            // {
            //     img: '../../images/game/game_question.png',
            //     url: "/customize/pages/qwdt/qwdt"
            // },
            {
                img: '../../images/game/game_city.png',
                url: "../map/map"
            },
            // {
            //     img: 'https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/tea/game_yuanxiao.jpg',// 元宵
            //     url: "../exchangeWeb/index"
            // },
            // {
            //     img: '../../images/game/game_year.jpg',// 年夜饭拼图
            //     url: "../exchangeWeb/index"
            // }
        ]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            userInfo: heloo.globalData.userInfo
        })
    },

    onShareAppMessage: function () {
        return {
            title: 'Meco真茶局',
            path: '/customize/pages/index/index',
            // imageUrl: "/images/shareicon.jpg"
        }
    },
    // 手机号授权
    getPhone(e) {
        var that = this;
        wx.checkSession({
            success: function () {
                if (e.detail.iv) {
                    if (!that.data.userInfo) {
                        wx.showModal({
                            title: '授权失败',
                            content: '尚未授权注册',
                        })
                        return;
                    }
                    let index = e.currentTarget.dataset.index;
                    let phonepagetype = "5";
                    that.setData({
                        index: index
                    })
                    if(index == 1){
                        phonepagetype = "6";
                    }
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    wx.request({
                        url: heloo.globalData.url + '/pointProduct/getPhoneNum',
                        method: "POST",
                        header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        },
                        data: {
                            openid: that.data.userInfo.openid,
                            sessionKey: heloo.globalData.userKey,
                            phonepagetype:phonepagetype,
                            encryptedData: e.detail.encryptedData,
                            iv: e.detail.iv
                        },
                        success: function (res) {
                            wx.hideLoading();
                            if (res.data.status == "1") {
                                console.log(res)
                                heloo.globalData.userInfo = res.data.userinfo;
                                that.setData({
                                    userInfo: res.data.userinfo
                                })
                                if(index == 3){
                                    that.gomap();
                                }else{
                                    that.goPlay();
                                }
                            } else {
                                wx.showModal({
                                    title: '手机号解密失败',
                                    content: res.data.msg,
                                })
                            }
                        }
                    })
                }
            },
            fail: function () {
                wx.showModal({
                    title: '授权失败',
                    content: '登录状态过期，请重新授权！',
                    success: function () {
                        that.userLogin();
                    }
                })
            }
        })

    },
    start(e) {
        let index = e.currentTarget.dataset.index;
        this.setData({
            index: index
        })
        if (index == 0) {
            // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
            heloo.writeTime(5,"pages/game/game")
        } else {
            // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
            heloo.writeTime(6,"pages/game/game")
        }
        this.goPlay();
    },
    goPlay() {
        let that = this;
        let url = that.data.list[that.data.index].url;
        console.log(url)
        if (url) {
            url = encodeURIComponent(url + "?openid=" + heloo.globalData.userInfo.openid);
            wx.navigateTo({
                url: '../web/web?url=' + url
            })
        } else {
            wx.showToast({
                title: '敬请期待！',
                icon: "none"
            })
        }
    },
    list2Tap(e){
        const {item} = e.currentTarget.dataset
        wx.navigateTo({
            url: item.url,
        //   url: '../map/map',
        })
    }
})