// pages/package/package.js
// const app = getApp();
import heloo from '../../utils/heloo';
const umaUrl = "https://ucode-openapi.aax6.cn";
Page({

    /**
     * 页面的初始数据
     */
    data: {
        record: [],
        swiper: 0,
        pic: [],
        pageNo: 0,
        umToken: "",
        umSerialId: "",
        userInfo:null,
        // 中奖信息填写
        userName: "",
        userPhone: "",
        userCard: "",
        address: "",
        prize:null,
        isShow:false,
        uuid: "",
        prizeOrderNum: "",
        isGet:false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let that = this;
        that.setData({
            userInfo:heloo.globalData.userInfo
        })
        if(options.umToken){
            that.setData({
                umToken: options.umToken,
                umSerialId: options.umSerialId,
            })
            that.getRecord()
        }else{
            that.umaLogin();
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return {
            title: 'Meco真茶局',
            path: '/customize/pages/index/index',
            // imageUrl: "/images/shareicon.jpg"
        }
    },
    formatDate2(inputTime) {
        var date = new Date(inputTime);
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? '0' + m : m;
        var d = date.getDate();
        d = d < 10 ? '0' + d : d;
        var h = date.getHours();
        h = h < 10 ? '0' + h : h;
        var minute = date.getMinutes();
        var second = date.getSeconds();
        minute = minute < 10 ? '0' + minute : minute;
        second = second < 10 ? '0' + second : second;
        return y + '.' + m + '.' + d;
    },
    setName(e) {
        this.setData({
            userName: e.detail.value
        })
    },
    setCardId(e) {
        this.setData({
            userCard: e.detail.value
        })
    },
    setPhone(e) {
        this.setData({
            userPhone: e.detail.value
        })
    },
    setAdd(e) {
        this.setData({
            address: e.detail.value
        })
    },
    submit() {
        let that = this;
        if (that.data.userName == "") {
            wx.showToast({
                title: "请填写姓名！",
                icon: "none"
            })
            return;
        }
        if (that.data.userPhone.length != 11) {
            wx.showToast({
                title: "请填写正确手机号！",
                icon: "none"
            })
            return;
        }
        if (that.data.prize.prizeType == 'PHYSICAL' && that.data.address == "") {
            wx.showToast({
                title: "请填写详细地址！",
                icon: "none"
            })
            return;
        }
        if (that.data.prize.prizeType == 'CHANGE_OF_MONEY' || that.data.prize.prizeType == 'RED_PACKET') {
            if (that.data.prize.prizeRegister&&that.data.prize.prizeRegister.registerInfo && that.data.prize.prizeRegister.registerInfo.length > 0) {
                let _IDRe18 = /^([1-6][1-9]|50)\d{4}(18|19|20)\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
                let _IDre15 = /^([1-6][1-9]|50)\d{4}\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}$/
                // 校验身份证：
                if (_IDRe18.test(that.data.userCard) || _IDre15.test(that.data.userCard)) {

                } else {
                    wx.showToast({
                        title: "请填写正确身份证号！",
                        icon: "none"
                    })
                    return;
                }
            }
            if (that.data.prize.poolPrizeVo&&that.data.prize.poolPrizeVo.prizeRegister.registerInfo && that.data.prize.poolPrizeVo.prizeRegister.registerInfo.length > 0) {
                let _IDRe18 = /^([1-6][1-9]|50)\d{4}(18|19|20)\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
                let _IDre15 = /^([1-6][1-9]|50)\d{4}\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}$/
                // 校验身份证：
                if (_IDRe18.test(that.data.userCard) || _IDre15.test(that.data.userCard)) {

                } else {
                    wx.showToast({
                        title: "请填写正确身份证号！",
                        icon: "none"
                    })
                    return;
                }
            }
        }
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/meco/awardPresent',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: that.data.userInfo.openid,
                prizeUid: that.data.uuid,
                receiver: that.data.userName,
                phone: that.data.userPhone,
                address: that.data.address,
                userCard: that.data.userCard,
                award: that.data.prize.name,
                awardId: that.data.prize.prizeId,
                prizeType: that.data.prize.prizeType
            },
            success(data) {
                console.log(data)
                if (data.data.status == 1) {
                    that.getPrize();
                } else {
                    wx.showToast({
                        title: data.data.message,
                        icon: "none"
                    })
                }
            }
        })
    },
    getPic() {
        let that = this;
        if (that.data.pic && that.data.pic.length > 0) {
            return;
        }
        wx.request({
            url: heloo.globalData.url + '/api/lunbopicture/list',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                type: 3
            },
            success(data) {
                console.log(data)
                that.setData({
                    pic: data.data.data
                })
            }
        })

    },
    // 点击跳转
    jump(e) {
        let that = this;
        let type = e.currentTarget.dataset.type;
        let index = e.currentTarget.dataset.index;
        let list = that.data.pic;
        if (list[index].appid) {
            if (list[index].appid == "wxb8a223205e2083f0") {
                let path = list[index].linkurl;
                if (!path) {
                    path = list[index].linkPage;
                }
                wx.navigateTo({
                    url: "/customize"+path,
                })
            }else if (list[index].appid == "wxd570425c5356eaa9") {
                let path = list[index].linkurl;
                if (!path) {
                    path = list[index].linkPage;
                }
                wx.navigateTo({
                    url: "/"+path,
                })
            } else {
                let path = list[index].linkurl;
                if (!path) {
                    path = list[index].linkPage;
                }
                wx.navigateToMiniProgram({
                    appId: list[index].appid,
                    path: path,
                    fail(res) {
                        console.log(res.errMsg)
                    }
                })
            }

        } else if (list[index].linkurl) {
            wx.navigateTo({
                url: '../web/web?url=' + list[index].linkurl
            })
        } else if (list[index].linkPage) {
            wx.navigateTo({
                url: '../web/web?url=' + list[index].linkPage
            })
        }
    },
    // u码登录
    umaLogin() {
        var that = this;
        wx.login({
            success(res) {
                wx.request({
                    url: umaUrl + "/auth/mp/login", //仅为示例，并非真实接口地址。
                    data: {
                        appId: "wxb8a223205e2083f0",
                        code: res.code
                    },
                    method: 'GET',
                    header: {
                        'Content-Type': 'application/json' //自定义请求头信息
                    },
                    success: function success(um) {
                        wx.hideLoading();
                        console.log(res)
                        that.setData({
                            umToken: um.data.token,
                            umSerialId: um.data.serialId
                        })
                        that.getRecord();
                    },
                    fail: function fail(err) {
                        wx.hideLoading();
                        console.log('fail:' + JSON.stringify(err));
                    }
                })
            }
        })

    },
    // u码兑奖
    getPrize() {
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        if(that.data.userCard){
            var data = {
                uuid: that.data.uuid,
                prizeOrderNum: that.data.prizeOrderNum,
                register: {
                    consignee: that.data.userName,
                    phoneNumber: that.data.userPhone,
                    idCardNo: that.data.userCard
                }
            }
        }else if(that.data.address){
            var data = {
                uuid: that.data.uuid,
                prizeOrderNum: that.data.prizeOrderNum,
                register: {
                    consignee: that.data.userName,
                    address: that.data.address,
                    phoneNumber: that.data.userPhone
                }
            }
        }else{
            var data = {
                uuid: that.data.uuid,
                prizeOrderNum: that.data.prizeOrderNum
            }
        }
        console.log(data)
        wx.request({
            url: umaUrl + "/claim",
            data: data,
            method: 'POST',
            header: {
                'Authorization': 'Bearer ' + that.data.umToken, //Bearer后面必须接空格再加token
                'serialId': that.data.umSerialId,
                'Content-Type': 'application/json' //自定义请求头信息
            },
            success: function success(um) {
                wx.hideLoading();
                console.log("兑奖")
                console.log(um)
                if (um.data.prizeStatus == "CLAIM_SUCCESS") {
                    if (that.data.prize.prizeType == 'PHYSICAL') { //实物
                        that.setData({
                            userName: "",
                            userPhone: "",
                            address: "",
                            isGet: true,
                            isShow: false
                        })
                    } else {
                        wx.showToast({
                            title: '领取成功',
                            icon: "none",
                            success() {
                                if (that.data.prize.prizeType == 'CHANGE_OF_MONEY' || that.data.prize.prizeType == 'RED_PACKET') {
                                    setTimeout(() => {
                                        wx.redirectTo({
                                            url: '../index/index',
                                        })
                                    }, 1500);
                                }
                            }
                        })
                        that.setData({
                            isShow: false
                        })
                    }
                } else {
                    wx.showToast({
                        title: um.data.ecode + ":" + um.data.emsg,
                        icon: "none"
                    })
                }
            },
            fail: function fail(err) {
                wx.hideLoading();
                console.log('fail:' + JSON.stringify(err));
            }
        })
    },
    swiperChange(e) {
        this.setData({
            swiper: e.detail.current
        })
    },
    closePrize() {
        this.setData({
            isShow: false
        })
        wx.redirectTo({
            url: '../index/index',
        })
    },
    toUse() {
        this.setData({
            isShow: false
        })
        wx.redirectTo({
            url: '../index/index',
        })
    },
    toIndex() {
        this.setData({
            isShow: false
        })
        wx.redirectTo({
            url: '../index/index',
        })
    },
    closeGet() {
        this.setData({
            isGet: false
        })
        wx.reLaunch({
            url: '../index/index',
        })
    },
    getRecord() {
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: umaUrl + "/user/lotteryRecords?page=" + that.data.pageNo + "&size=20",
            data: {
                page: that.data.pageNo,
                size: 20
            },
            method: 'POST',
            header: {
                'Authorization': 'Bearer ' + that.data.umToken, //Bearer后面必须接空格再加token
                'serialId': that.data.umSerialId,
                'Content-Type': 'application/json' //自定义请求头信息
            },
            success: function success(um) {
                wx.hideLoading();
                console.log("记录")
                console.log(um)
                let pageNo = that.data.pageNo;
                let record = that.data.record;
                let list = um.data.content;
                list.map((item) => {
                    if (item.prizeType) {
                        if (item.prizeType == "COUPON" || item.prizeType == "POINT") {
                            item.prizeName = "50会员积分";
                            item.prizeStatus = "CLAIM_SUCCESS";
                        }
                    } else {
                        item.prizeName = "50会员积分";
                        item.prizeStatus = "CLAIM_SUCCESS";
                    }
                    item.time = that.formatDate2(item.lotteryTime)
                })
                if (list.length > 0) {
                    pageNo++;
                }
                that.setData({
                    record: record.concat(list),
                    pageNo: pageNo
                })
                that.getPic();
            },
            fail: function fail(err) {
                wx.hideLoading();
                console.log('fail:' + JSON.stringify(err));
            }
        })
    },
    setPrize(e){
        let index = e.currentTarget.dataset.index;
        let that = this;
        let prize = that.data.record[index];
        prize.name = prize.prizeName;
        that.setData({
            uuid: prize.uuid,
            prizeOrderNum: prize.prizeOrderNum,
            prize: prize,
            isShow: true,
            pageNo:0
        })
    },
    // 手机号授权
    getPhone(e) {
        var that = this;
        wx.checkSession({
            success: function () {
                if (e.detail.iv) {
                    if (!that.data.userInfo) {
                        wx.showModal({
                            title: '授权失败',
                            content: '尚未授权注册',
                        })
                        return;
                    }
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    wx.request({
                        url: heloo.globalData.url + '/pointProduct/getPhoneNum',
                        method: "POST",
                        header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        },
                        data: {
                            openid: that.data.userInfo.openid,
                            sessionKey: heloo.globalData.userKey,
                            phonepagetype:"2",
                            encryptedData: e.detail.encryptedData,
                            iv: e.detail.iv
                        },
                        success: function (res) {
                            wx.hideLoading();
                            if (res.data.status == "1") {
                                console.log(res)
                                heloo.globalData.userInfo = res.data.userinfo;
                                that.setData({
                                    userInfo: res.data.userinfo
                                })
                                that.getPrize();
                            } else {
                                wx.showModal({
                                    title: '手机号解密失败',
                                    content: res.data.msg,
                                })
                            }
                        }
                    })
                }
            },
            fail: function () {
                wx.showModal({
                    title: '授权失败',
                    content: '登录状态过期，请重新授权！',
                    success: function () {
                        that.userLogin();
                    }
                })
            }
        })

    },
})