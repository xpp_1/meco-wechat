// customize/pages/orchard/orchard.js
import heloo from "../../utils/heloo";
import FMAgent from "../../utils/fm-1.6.5-es.min"
Page({
  /**
   * 页面的初始数据
   */
  data: {
    url: "",
    userInfo: "",
    reference: "",
    referenceId: "", // 注册
    optionsOpenId: null, //注册并助力
    res:'',
    ip:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(heloo.globalData.userInfo)
    var fmagent = new FMAgent(heloo.globalData._fmOpt) // 这里需要传入一些必要配置
    var that = this;
    
  //  //获取用户ip地址
       fmagent.getInfo({
        page: this, // 请传入FMAgent所在的Page或Component对象
        openid:options.openid, // 请传入加密的用户openid
        // 如果您开通了unionid功能，请传入加密的用户unionid，
        // 否则留空即可
        unionid:heloo.globalData.userInfo.unionid, 
        noClipboard:true,
        success: function (res) {
          console.log(res)
          that.setData({
            res,
          })
          console.log(heloo.globalData.userInfo.openid)
        
        // 成功回调，res为blackbox字符串
    console.log(options);
    that.setData({
      referenceId: options.uid,
      optionsOpenId: options.openid,
    });
    if (heloo.globalData.userInfo.unionid) {
      // 用户已登录并且是注册过的
      console.log("用户已登录并且是注册过的");
      that.setData({
        userInfo: heloo.globalData.userInfo,
      });
      if (options.openid) {
        // 如果用户点击分享出去的链接进入网页 ,需要携带参数
        /**
         * openid 是分享人的opid
         * helpOpenid 是点击人的opid
         */
        // 分享点进来帮忙助力
        wx.login({
          success(res) {
            console.log(res.code);
            wx.request({
              url: heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
              method: "POST",
              data: {
                code: res.code,
              },
              success(result) {
                console.log("userIsNull调用成功");
                if (result.data.status == -1) {
                  wx.showToast({
                    title: "授权失败",
                    icon: "none",
                  });
                } else if (
                  result.data.data.message == "该用户openid还未注册！"
                ) {
                
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.openid,
                      unionid: result.data.data.unionid,
                      reference: options.uid,
                      source: "farm",
                    },
                    success(res) {
                      console.log(res);
                      that.setData({
                        url:
                          heloo.globalData.urlOrchard +
                          `?uid=${result.data.data.unionid}&openid=${options.openid}&opid=${options.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                      });
                    },
                  });
                } else if (result.data.message == "请先成为果园用户选取种子") {
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.openid,
                      unionid: result.data.data.unionid,
                    },
                    success(res) {
                      console.log(res);
                      that.setData({
                        url:
                          heloo.globalData.urlOrchard +
                          `?uid=${result.data.data.unionid}&openid=${options.openid}&opid=${options.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                      });
                    },
                  });
                } else {
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.wxuser.openid,
                      unionid: result.data.data.wxuser.unionid,
                    },
                    success(res) {
                      console.log(res);
                      that.setData({
                        url:
                          heloo.globalData.urlOrchard +
                          `?uid=${result.data.data.wxuser.unionid}&openid=${options.openid}&opid=${options.openid}&helpOpenid=${result.data.data.wxuser.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                      });
                    },
                  });
                }
              },
            });
          },
        });
      } else if (options.reference) {
        wx.login({
          success(res) {
            console.log(res.code);
            wx.request({
              url: heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
              method: "POST",
              data: {
                code: res.code,
              },
              success(result) {
                console.log("userIsNull调用成功", result);
                if (result.data.status == -1) {
                  wx.showToast({
                    title: "授权失败",
                    icon: "none",
                  });
                } else if (
                  result.data.data.message == "该用户openid还未注册！"
                ) {
               
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.openid,
                      unionid: result.data.data.unionid,
                      reference: options.uid,
                      source: "farm",
                    },
                    success(res) {
                      console.log(res);
                      that.setData({
                        url:
                          heloo.globalData.urlOrchard +
                          `?uid=${result.data.data.unionid}&opid=${result.data.data.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                      });
                    },
                  });
                } else if (result.data.message == "请先成为果园用户选取种子") {
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.openid,
                      unionid: result.data.data.unionid,
                    },
                    success(res) {
                      console.log(res);
                      that.setData({
                        url:
                          heloo.globalData.urlOrchard +
                          `?uid=${result.data.data.unionid}&opid=${result.data.data.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                      });
                    },
                  });
                } else {
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.wxuser.openid,
                      unionid: result.data.data.wxuser.unionid,
                    },
                    success(res) {
                      console.log(res);
                      that.setData({
                        url:
                          heloo.globalData.urlOrchard +
                          `?uid=${result.data.data.wxuser.unionid}&opid=${result.data.data.wxuser.openid}&helpOpenid=${result.data.data.wxuser.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                      });
                    },
                  });
                }
              },
            });
          },
        });
      } else {
        // 跳转微商城
        if (options.appid) {
          wx.showModal({
            title: "打开微商城",
            content: "将跳转至微商城",
            showCancel: false,
            success: function (res) {
              console.log(res);
              if (res.confirm) {
                wx.navigateToMiniProgram({
                  appId: options.appid,
                  path:
                    "pages/home/index?introduceID=990130481229&st=120&si=yibeiyima_zhongbuanniu3",
                  success(res) {
                    console.log(res, "打开成功");
                  },
                  fail(res) {
                    console.log(res.errMsg);
                    that.setData({
                      url:
                        heloo.globalData.urlOrchard +
                        `?uid=${heloo.globalData.userInfo.unionid}&opid=${heloo.globalData.userInfo.openid}&helpOpenid=${heloo.globalData.userInfo.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                    });
                  },
                });
              } else {
                wx.showToast({
                  title: "您已取消该操作",
                  icon: "none",
                });
              }
            },
          });
        } else {
          //正常跳转,opid发布助力任务参数
          console.log(that.data.res)
          that.setData({
            url:
              heloo.globalData.urlOrchard +
              `?uid=${options.uid}&opid=${that.data.userInfo.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
          });
        }
      }
    } else {
      if (heloo.globalData.register && heloo.globalData.register.unionid) {
        // 用户已登录未注册 弹出弹框提醒用户授权注册
        console.log("用户已登录未注册 弹出弹框提醒用户授权注册");
        wx.showModal({
          content: "您还未授权注册，是否进行注册？",
          confirmText: "是",
          cancelText: "否",
          success(res) {
            if (res.confirm) {
              that.getInfo();
            } else {
              wx.reLaunch({
                url: "../index/index",
              });
            }
          },
        });
      } else {
        heloo.wxLogin().then(
          () => {
            // 用户未登录，已注册
            console.log("用户未登录，已注册", heloo.globalData.userInfo);
            that.setData({
              userInfo: heloo.globalData.userInfo,
            });
            if (options.openid) {
              wx.login({
                success(res) {
                  console.log(res.code);
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
                    method: "POST",
                    data: {
                      code: res.code,
                    },
                    success(result) {
                      console.log(result);
                      console.log("userIsNull调用成功", options.uid);
                      if (result.data.status == -1) {
                        wx.showToast({
                          title: "授权失败",
                          icon: "none",
                        });
                      } else if (
                        result.data.data.message == "该用户openid还未注册！"
                      ) {
                      
                        wx.request({
                          url:
                            heloo.globalData.baseUrlOrchard +
                            "/farmApi/getunionid",
                          method: "POST",
                          data: {
                            userInfo: JSON.stringify(heloo.globalData.userInfo),
                            openid: result.data.data.openid,
                            unionid: result.data.data.unionid,
                            reference: options.reference,
                            source: "farm",
                          },
                          success(res) {
                            console.log(res);
                            that.setData({
                              url:
                                heloo.globalData.urlOrchard +
                                `?uid=${result.data.data.unionid}&openid=${options.openid}&opid=${options.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                            });
                          },
                        });
                      } else if (
                        result.data.message == "请先成为果园用户选取种子"
                      ) {
                        wx.request({
                          url:
                            heloo.globalData.baseUrlOrchard +
                            "/farmApi/getunionid",
                          method: "POST",
                          data: {
                            userInfo: JSON.stringify(heloo.globalData.userInfo),
                            openid: result.data.data.openid,
                            unionid: result.data.data.unionid,
                          },
                          success(res) {
                            console.log(res);
                            that.setData({
                              url:
                                heloo.globalData.urlOrchard +
                                `?uid=${result.data.data.unionid}&openid=${options.openid}&opid=${options.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                            });
                          },
                        });
                      } else {
                        wx.request({
                          url:
                            heloo.globalData.baseUrlOrchard +
                            "/farmApi/getunionid",
                          method: "POST",
                          data: {
                            userInfo: JSON.stringify(heloo.globalData.userInfo),
                            openid: result.data.data.wxuser.openid,
                            unionid: result.data.data.wxuser.unionid,
                          },
                          success(res) {
                            console.log(res);
                            that.setData({
                              url:
                                heloo.globalData.urlOrchard +
                                `?uid=${result.data.data.wxuser.unionid}&openid=${options.openid}&opid=${options.openid}&helpOpenid=${result.data.data.wxuser.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                            });
                          },
                        });
                      }
                    },
                  });
                },
              });
            } else if (options.reference) {
              wx.login({
                success(res) {
                  console.log(res.code);
                  wx.request({
                    url:
                      heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
                    method: "POST",
                    data: {
                      code: res.code,
                    },
                    success(result) {
                      console.log("userIsNull调用成功", result);
                      if (result.data.status == -1) {
                        wx.showToast({
                          title: "授权失败",
                          icon: "none",
                        });
                      } else if (
                        result.data.data.message == "该用户openid还未注册！"
                      ) {
                      
                        wx.request({
                          url:
                            heloo.globalData.baseUrlOrchard +
                            "/farmApi/getunionid",
                          method: "POST",
                          data: {
                            userInfo: JSON.stringify(heloo.globalData.userInfo),
                            openid: result.data.data.openid,
                            unionid: result.data.data.unionid,
                            reference: options.uid,
                            source: "farm",
                          },
                          success(res) {
                            console.log(res);
                            that.setData({
                              url:
                                heloo.globalData.urlOrchard +
                                `?uid=${result.data.data.unionid}&opid=${result.data.data.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                            });
                          },
                        });
                      } else if (
                        result.data.message == "请先成为果园用户选取种子"
                      ) {
                        wx.request({
                          url:
                            heloo.globalData.baseUrlOrchard +
                            "/farmApi/getunionid",
                          method: "POST",
                          data: {
                            userInfo: JSON.stringify(heloo.globalData.userInfo),
                            openid: result.data.data.openid,
                            unionid: result.data.data.unionid,
                            reference: options.uid,
                            source: "farm",
                          },
                          success(res) {
                            console.log(res);
                            that.setData({
                              url:
                                heloo.globalData.urlOrchard +
                                `?uid=${result.data.data.unionid}&opid=${result.data.data.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                            });
                          },
                        });
                      } else {
                        wx.request({
                          url:
                            heloo.globalData.baseUrlOrchard +
                            "/farmApi/getunionid",
                          method: "POST",
                          data: {
                            userInfo: JSON.stringify(heloo.globalData.userInfo),
                            openid: result.data.data.wxuser.openid,
                            unionid: result.data.data.wxuser.unionid,
                          },
                          success(res) {
                            console.log(res);
                            that.setData({
                              url:
                                heloo.globalData.urlOrchard +
                                `?uid=${result.data.data.wxuser.unionid}&opid=${result.data.data.wxuser.openid}&helpOpenid=${result.data.data.wxuser.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                            });
                          },
                        });
                      }
                    },
                  });
                },
              });
            } else {
              // 跳转微商城
              if (options.appid) {
                wx.showModal({
                  title: "打开微商城",
                  content: "将跳转至微商城",
                  showCancel: false,
                  success: function (res) {
                    console.log(res);
                    if (res.confirm) {
                      wx.navigateToMiniProgram({
                        appId: options.appid,
                        path:
                          "pages/home/index?introduceID=990130481229&st=120&si=yibeiyima_zhongbuanniu3",
                        success(res) {
                          console.log(res, "打开成功");
                        },
                        fail(res) {
                          console.log(res.errMsg);
                          that.setData({
                            url:
                              heloo.globalData.urlOrchard +
                              `?uid=${heloo.globalData.userInfo.unionid}&opid=${heloo.globalData.userInfo.openid}&helpOpenid=${heloo.globalData.userInfo.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                          });
                        },
                      });
                    } else {
                      wx.showToast({
                        title: "您已取消该操作",
                        icon: "none",
                      });
                    }
                  },
                });
              } else {
                //正常跳转,opid发布助力任务参数
                that.setData({
                  url:
                    heloo.globalData.urlOrchard +
                    `?uid=${options.uid}&opid=${that.data.userInfo.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                });
              }
            }
          },
          () => {
            // 用户未登录，未注册  弹出弹框提醒用户授权注册
            console.log("用户未登录，未注册  弹出弹框提醒用户授权注册");
            wx.showModal({
              content: "您还未授权注册，是否进行注册？",
              confirmText: "是",
              cancelText: "否",
              success(res) {
                if (res.confirm) {
                  that.getInfo();
                } else {
                  wx.reLaunch({
                    url: "../index/index",
                  });
                }
              },
            });
          }
        );
      }
    }
  },
  fail: function (res) {
    console.log(res)
  // 失败回调，res为各种exception对象
  },
  complete: function (res) {
  }
  })

  },
  handleGetMessage(e) {
    // 获取最后一次点击的分享按钮
    this.setData({
      help: e.detail.data[e.detail.data.length - 1].help,
    });
  },
  getInfo() {
    let that = this;
    if (wx.canIUse("getUserProfile")) {
      wx.getUserProfile({
        lang: "zh_CN",
        desc: "本次授权用于个人信息显示",
        success(res) {
          wx.showLoading({
            title: "加载中...",
            mask: true,
          });
          let unionid = "";
          let openid = "";
          if (that.data.userInfo) {
            unionid = that.data.userInfo.unionid;
            openid = that.data.userInfo.openid;
          } else {
            unionid = heloo.globalData.register.unionid;
            openid = heloo.globalData.register.openid;
          }
          wx.request({
            url: heloo.globalData.url + "/api/getunionid",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              userInfo: JSON.stringify(res.userInfo),
              unionid: unionid,
              openid: openid,
              source: "farm",
            },
            success(data) {
              wx.hideLoading();
              that.setData({
                userInfo: data.data.wxuser,
              });
              heloo.globalData.userInfo = data.data.wxuser;
              // console.log();
              // console.log(heloo.globalData.userInfo);
              // that.setPercent(data.data.wxuser.sumPoints);
              // 如果是分享助力
              if (that.data.optionsOpenId) {
                wx.login({
                  success(res) {
                    console.log(res.code);
                    wx.request({
                      url:
                        heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
                      method: "POST",
                      data: {
                        code: res.code,
                      },
                      success(result) {
                        console.log("userIsNull调用成功");
                        if (result.data.status == -1) {
                          wx.showToast({
                            title: "授权失败",
                            icon: "none",
                          });
                        } else if (
                          result.data.data.message == "该用户openid还未注册！"
                        ) {
                       
                          wx.request({
                            url:
                              heloo.globalData.baseUrlOrchard +
                              "/farmApi/getunionid",
                            method: "POST",
                            data: {
                              userInfo: JSON.stringify(
                                heloo.globalData.userInfo
                              ),
                              openid: result.data.data.openid,
                              unionid: result.data.data.unionid,
                              reference: that.data.referenceId,
                              source: "farm",
                            },
                            success(res) {
                              fmagent.getInfo({
                                page: that, // 请传入FMAgent所在的Page或Component对象
                                openid:heloo.globalData.userInfo.openid, // 请传入加密的用户openid
                                // 如果您开通了unionid功能，请传入加密的用户unionid，
                                // 否则留空即可
                                unionid:heloo.globalData.userInfo.unionid, 
                                noClipboard:true,
                                success: function (res) {
                                  console.log(res)
                                  var params = {
                                    black_box:res,
                                    account_login:heloo.globalData.userInfo.unionid,
                                    account_mobile:heloo.globalData.userInfo.phoneNumber,
                                    openid:heloo.globalData.userInfo.openid,
                                    mkt_cmpgn_typ:5,//活动ID或标识，用于区分不同活动。1：每日签到，2：宠粉大抽奖，3：会员互动，4：好友助力，5：邀请好友注册，6：微商场下单
                                 invite_id:heloo.globalData.userInfo.unionid
                             }
                             wx.request({
                              url: 'https://playbill.chinaxpp.com:8090/grouponApi0831/fraudCheck',
                              method: 'POST',
                              data:params,
                              headers: {
                                'content-type': 'application/x-www-form-urlencoded' // 默认值
                              },
                              success (res) {
                                console.log(res.data)
                              }
                            })
                                }
                              })
                              console.log(res);
                              that.setData({
                                url:
                                  heloo.globalData.urlOrchard +
                                  `?uid=${result.data.data.unionid}&openid=${that.data.optionsOpenId}&opid=${that.data.optionsOpenId}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                              });
                            },
                          });
                        } else if (
                          result.data.message == "请先成为果园用户选取种子"
                        ) {
                          wx.request({
                            url:
                              heloo.globalData.baseUrlOrchard +
                              "/farmApi/getunionid",
                            method: "POST",
                            data: {
                              userInfo: JSON.stringify(
                                heloo.globalData.userInfo
                              ),
                              openid: result.data.data.openid,
                              unionid: result.data.data.unionid,
                            },
                            success(res) {
                              fmagent.getInfo({
                                page: that, // 请传入FMAgent所在的Page或Component对象
                                noClipboard:true,
                                openid:heloo.globalData.userInfo.openid, // 请传入加密的用户openid
                                // 如果您开通了unionid功能，请传入加密的用户unionid，
                                // 否则留空即可
                                unionid:heloo.globalData.userInfo.unionid, 
                                success: function (res) {
                                  console.log(res)
                                  var params = {
                                    black_box:res,
                                    account_login:heloo.globalData.userInfo.unionid,
                                    account_mobile:heloo.globalData.userInfo.phoneNumber,
                                    openid:heloo.globalData.userInfo.openid,
                                    mkt_cmpgn_typ:5,//活动ID或标识，用于区分不同活动。1：每日签到，2：宠粉大抽奖，3：会员互动，4：好友助力，5：邀请好友注册，6：微商场下单
                                 invite_id:heloo.globalData.userInfo.unionid
                             }
                             wx.request({
                              url: 'https://playbill.chinaxpp.com:8090/grouponApi0831/fraudCheck',
                              method: 'POST',
                              data:params,
                              headers: {
                                'content-type': 'application/x-www-form-urlencoded' // 默认值
                              },
                              success (res) {
                                console.log(res.data)
                              }
                            })
                                }
                              })
                              console.log(res);
                              that.setData({
                                url:
                                  heloo.globalData.urlOrchard +
                                  `?uid=${result.data.data.unionid}&openid=${that.data.optionsOpenId}&opid=${that.data.optionsOpenId}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&res=${that.data.res}`,
                              });
                            },
                          });
                        } else {
                          wx.request({
                            url:
                              heloo.globalData.baseUrlOrchard +
                              "/farmApi/getunionid",
                            method: "POST",
                            data: {
                              userInfo: JSON.stringify(
                                heloo.globalData.userInfo
                              ),
                              openid: result.data.data.wxuser.openid,
                              unionid: result.data.data.wxuser.unionid,
                            },
                            success(res) {
                              console.log(res);
                              fmagent.getInfo({
                                page: that, // 请传入FMAgent所在的Page或Component对象
                                noClipboard:true,
                                openid:heloo.globalData.userInfo.openid, // 请传入加密的用户openid
                                // 如果您开通了unionid功能，请传入加密的用户unionid，
                                // 否则留空即可
                                unionid:heloo.globalData.userInfo.unionid, 
                                success: function (res) {
                                  console.log(res)
                                  var params = {
                                    black_box:res,
                                    account_login:heloo.globalData.userInfo.unionid,
                                    account_mobile:heloo.globalData.userInfo.phoneNumber,
                                    openid:heloo.globalData.userInfo.openid,
                                    mkt_cmpgn_typ:5,//活动ID或标识，用于区分不同活动。1：每日签到，2：宠粉大抽奖，3：会员互动，4：好友助力，5：邀请好友注册，6：微商场下单
                                 invite_id:heloo.globalData.userInfo.unionid
                             }
                             wx.request({
                              url: 'https://playbill.chinaxpp.com:8090/grouponApi0831/fraudCheck',
                              method: 'POST',
                              data:params,
                              headers: {
                                'content-type': 'application/x-www-form-urlencoded' // 默认值
                              },
                              success (res) {
                                console.log(res.data)
                              }
                            })
                                }
                              })
                              that.setData({
                                url:
                                  heloo.globalData.urlOrchard +
                                  `?uid=${result.data.data.wxuser.unionid}&openid=${that.data.optionsOpenId}&opid=${that.data.optionsOpenId}&helpOpenid=${result.data.data.wxuser.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                              });
                            },
                          });
                        }
                      },
                    });
                  },
                });
              } else {
                wx.login({
                  success(res) {
                    console.log(res.code);
                    wx.request({
                      url:
                        heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
                      method: "POST",
                      data: {
                        code: res.code,
                      },
                      success(result) {
                        console.log("userIsNull调用成功");
                        if (result.data.status == -1) {
                          wx.showToast({
                            title: "授权失败",
                            icon: "none",
                          });
                        } else if (
                          result.data.data.message == "该用户openid还未注册！"
                        ) {
                         
                          wx.request({
                            url:
                              heloo.globalData.baseUrlOrchard +
                              "/farmApi/getunionid",
                            method: "POST",
                            data: {
                              userInfo: JSON.stringify(
                                heloo.globalData.userInfo
                              ),
                              openid: result.data.data.openid,
                              unionid: result.data.data.unionid,
                              reference: that.data.referenceId,
                              source: "farm",
                            },
                            success(res) {
                              console.log(res);
                              fmagent.getInfo({
                                page: that, // 请传入FMAgent所在的Page或Component对象
                                noClipboard:true,
                                openid:heloo.globalData.userInfo.openid, // 请传入加密的用户openid
                                // 如果您开通了unionid功能，请传入加密的用户unionid，
                                // 否则留空即可
                                unionid:heloo.globalData.userInfo.unionid, 
                                success: function (res) {
                                  console.log(res)
                                  var params = {
                                    black_box:res,
                                    account_login:heloo.globalData.userInfo.unionid,
                                    account_mobile:heloo.globalData.userInfo.phoneNumber,
                                    openid:heloo.globalData.userInfo.openid,
                                    mkt_cmpgn_typ:5,//活动ID或标识，用于区分不同活动。1：每日签到，2：宠粉大抽奖，3：会员互动，4：好友助力，5：邀请好友注册，6：微商场下单
                                 invite_id:heloo.globalData.userInfo.unionid
                             }
                             wx.request({
                              url: 'https://playbill.chinaxpp.com:8090/grouponApi0831/fraudCheck',
                              method: 'POST',
                              data:params,
                              headers: {
                                'content-type': 'application/x-www-form-urlencoded' // 默认值
                              },
                              success (res) {
                                console.log(res.data)
                              }
                            })
                                }
                              })
                              that.setData({
                                
                                url:
                                  heloo.globalData.urlOrchard +
                                  `?uid=${result.data.data.unionid}&opid=${result.data.data.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                              });
                            },
                          });
                        } else if (
                          result.data.message == "请先成为果园用户选取种子"
                        ) {
                          wx.request({
                            url:
                              heloo.globalData.baseUrlOrchard +
                              "/farmApi/getunionid",
                            method: "POST",
                            data: {
                              userInfo: JSON.stringify(
                                heloo.globalData.userInfo
                              ),
                              openid: result.data.data.openid,
                              unionid: result.data.data.unionid,
                              reference: that.data.referenceId,
                              source: "farm",
                            },
                            success(res) {
                              fmagent.getInfo({
                                page: that, // 请传入FMAgent所在的Page或Component对象
                                noClipboard:true,
                                openid:heloo.globalData.userInfo.openid, // 请传入加密的用户openid
                                // 如果您开通了unionid功能，请传入加密的用户unionid，
                                // 否则留空即可
                                unionid:heloo.globalData.userInfo.unionid, 
                                success: function (res) {
                                  console.log(res)
                                  var params = {
                                    black_box:res,
                                    account_login:heloo.globalData.userInfo.unionid,
                                    account_mobile:heloo.globalData.userInfo.phoneNumber,
                                    openid:heloo.globalData.userInfo.openid,
                                    mkt_cmpgn_typ:5,//活动ID或标识，用于区分不同活动。1：每日签到，2：宠粉大抽奖，3：会员互动，4：好友助力，5：邀请好友注册，6：微商场下单
                                 invite_id:heloo.globalData.userInfo.unionid
                             }
                             wx.request({
                              url: 'https://playbill.chinaxpp.com:8090/grouponApi0831/fraudCheck',
                              method: 'POST',
                              data:params,
                              headers: {
                                'content-type': 'application/x-www-form-urlencoded' // 默认值
                              },
                              success (res) {
                                console.log(res.data)
                              }
                            })
                                }
                              })
                              console.log(res);
                              that.setData({
                                url:
                                  heloo.globalData.urlOrchard +
                                  `?uid=${result.data.data.unionid}&opid=${result.data.data.openid}&helpOpenid=${result.data.data.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                              });
                            },
                          });
                        } else {
                          wx.request({
                            url:
                              heloo.globalData.baseUrlOrchard +
                              "/farmApi/getunionid",
                            method: "POST",
                            data: {
                              userInfo: JSON.stringify(
                                heloo.globalData.userInfo
                              ),
                              openid: result.data.data.wxuser.openid,
                              unionid: result.data.data.wxuser.unionid,
                              reference: that.data.referenceId,
                              source: "farm",
                            },
                            success(res) {
                              fmagent.getInfo({
                                page: that, // 请传入FMAgent所在的Page或Component对象
                                noClipboard:true,
                                openid:heloo.globalData.userInfo.openid, // 请传入加密的用户openid
                                // 如果您开通了unionid功能，请传入加密的用户unionid，
                                // 否则留空即可
                                unionid:heloo.globalData.userInfo.unionid, 
                                success: function (res) {
                                  console.log(res)
                                  var params = {
                                    black_box:res,
                                    account_login:heloo.globalData.userInfo.unionid,
                                    account_mobile:heloo.globalData.userInfo.phoneNumber,
                                    openid:heloo.globalData.userInfo.openid,
                                    mkt_cmpgn_typ:5,//活动ID或标识，用于区分不同活动。1：每日签到，2：宠粉大抽奖，3：会员互动，4：好友助力，5：邀请好友注册，6：微商场下单
                                 invite_id:heloo.globalData.userInfo.unionid
                             }
                             wx.request({
                              url: 'https://playbill.chinaxpp.com:8090/grouponApi0831/fraudCheck',
                              method: 'POST',
                              data:params,
                              headers: {
                                'content-type': 'application/x-www-form-urlencoded' // 默认值
                              },
                              success (res) {
                                console.log(res.data)
                              }
                            })
                                }
                              })
                              console.log(res);
                              that.setData({
                                url:
                                  heloo.globalData.urlOrchard +
                                  `?uid=${result.data.data.wxuser.unionid}&opid=${result.data.data.wxuser.openid}&helpOpenid=${result.data.data.wxuser.openid}&phone=${heloo.globalData.userInfo.phoneNumber}&ip=${that.data.ip}&res=${that.data.res}`,
                              });
                            },
                          });
                        }
                      },
                    });
                  },
                });
              }
            },
          });
        },
      });
    } else {
      wx.showModal({
        content: "微信客户端版本过低，请更新后重试！",
        showCancel: false,
        confirmText: "我知道了",
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    if (this.data.help === 5) {
      // 用户助力或注册
      return {
        title: "宝，快帮我助力，一起种果树领Meco吧！",
        imageUrl:
          "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/farm/gyfx.jpg",
        path: `/customize/pages/orchard/orchard?uid=${this.data.userInfo.unionid}&openid=${this.data.userInfo.openid}`,
      };
    } else {
      return {
        title: "宝，快来和我一起种果树领Meco果汁茶！",
        imageUrl:
          "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/farm/gyfx.jpg",
        path: `/customize/pages/orchard/orchard?uid=${this.data.userInfo.unionid}&reference=${this.data.userInfo.unionid}`,
      };
    }
  },
  // 用户点击分享朋友圈
  onShareTimeline() {},
});
