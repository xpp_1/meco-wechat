/**
 * @description 来电界面
 */
import heloo from "../../utils/heloo";
import apiConstants from "../../server/apiConstants";
import util from "../../utils/utils";
import {
  christmasBgList,
  callMusic,
  defaultSenderAvatar,
} from "../../utils/constants";
// const { mai } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
Page({
  data: {
    senderMsg: "Meco",
    header: "",
    bgType: "", // 壁纸类型 default、1、2、3、4
    bgList: christmasBgList,
    sponsorSeq: "", // 祝福活动key
    shareType: "default", // default:默认祝福 share:好友祝福
    timer: null, //振动定时器
    vibrateTime: 0,
    backgroundAudioManager: null,
    innerAudioContext: null, // 电话铃声
    options: {},
    isActivated: null, // 是否会员
    isLogin: null, // 是否登录授权获取信息
    isLoading: false, // 初始加载loading
    isMiniScreen: false, // 适配小屏
  },
  computed: {
    callWrap() {
      const cls = "callWrap";
      switch (this.bgType) {
        case "1":
          cls + " bg1";
          break;
        case "2":
          cls + " bg2";
          break;
        case "3":
          cls + " bg3";
          break;
        case "4":
          cls + " bg4";
          break;
        default:
          break;
      }
      return cls;
    },
  },
  onLoad(options) {
    console.log(wx.globalSystemInfo, "wx.globalSystemInfo");
    const { screenHeight } = wx.globalSystemInfo;
    this.setData({ isMiniScreen: screenHeight < 780 });
    let {
      sponsorSeq = "",
      shareType = "default",
      sponsorAvatar = "",
      sponsorName = "",
      bgType = "default",
    } = options;
    if (!sponsorSeq) {
      // Meco默认祝福
      bgType = "default";
      sponsorAvatar = defaultSenderAvatar;
      sponsorName = "Meco";
    }
    this.setData({
      options,
      sponsorSeq,
      shareType,
      sponsorAvatar,
      sponsorName,
      bgType,
    });
  },
  onShow() {
    this.loadInit();
    // 振动
    this.data.timer = setInterval(() => {
      let time = this.data.vibrateTime + 500;
      if (time < 4000) {
        this.setData({
          vibrateTime: time,
        });
        wx.vibrateLong();
      } else {
        this.setData({
          vibrateTime: 0,
        });
        clearInterval(this.data.timer);
      }
    }, 1000);
    const innerAudioContext = wx.createInnerAudioContext();
    innerAudioContext.autoplay = true;
    innerAudioContext.loop = true;
    innerAudioContext.src = callMusic; // 电话铃声
    innerAudioContext.onPlay(() => {
      console.log("开始播放");
    });
    this.setData({
      innerAudioContext,
    });
  },
  onHide() {
    clearInterval(this.data.timer);
    this.data.innerAudioContext.pause();
    this.data.innerAudioContext.destroy();
    this.setData({
      innerAudioContext: null,
    });
  },
  onUnload() {
    clearInterval(this.data.timer);
    this.data.innerAudioContext.pause();
    this.data.innerAudioContext.destroy();
    this.setData({
      innerAudioContext: null,
    });
  },
  /**
   * 分享
   */
  onShareAppMessage: function () {
    const { options } = this.data;
    // sponsorSeq:祝福活动详情 sponsorAvatar:发起人头像  sponsorName:昵称  shareType:分享/默认
    let sharePath;
    if (options.shareType !== "share") {
      sharePath = "/customize/pages/christmasCall/index";
    } else {
      sharePath = `/customize/pages/christmasCall/index?sponsorSeq=${options.sponsorSeq}&shareType=${options.shareType}&sponsorAvatar=${options.sponsorAvatar}&sponsorName=${options.sponsorName}&bgType=${options.bgType}`;
    }
    console.log(sharePath, "sharePath");
    return {
      title: "宝！快接听一起抢红包吧>>",
      imageUrl:
        "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/shareBg.png",
      path: sharePath,
    };
  },

  async loadInit() {
    let _this = this;
    _this.setData({
      isLoading: true,
    });
    // 授权信息判断
    heloo.wxLogin({ loadType: "async" }).then(
      async (res) => {
        console.log(1111);
        _this.setData({
          isLogin: true,
        });
        console.log("已登录授权", res);
        const status = await _this.getToken();
        if (status !== true) {
          wx.hideLoading();
          return;
        }
        // wx.showLoading({
        //   title: "加载中...",
        //   icon: "none",
        // });
        if (!mai.member.getOpenId()) {
          await mai.member.signin();
        }
        const member = await mai.member.getDetail(["Card", "Properties"]);
        console.log(member, "member________");
        heloo.globalData["xpp_birthday"] =
          member.birthday > 0 ? util.formatTime(member.birthday, "Y-M-D") : ""; // 从群脉接口获取
        heloo.globalData["xpp_phone"] = member.phone; // 从群脉接口获取
        heloo.globalData["xpp_isActivated"] = member.isActivated;
        _this.setData({
          userInfo: heloo.globalData.userInfo,
          isActivated: member.isActivated,
          // isLoading: false,
        });
        wx.hideLoading();
        if (member) {
          _this.setData({
            isLoading: false,
          });
        }
      },
      (res) => {
        wx.hideLoading();
        _this.setData({
          isLogin: false,
          isLoading: false,
        });
        console.log("未登录授权", res);
      }
    );
  },
  getToken() {
    // 手动计算签名，后端进行校验
    const token = "wxTreasure88";
    const timestamp = Date.now();
    const randomNum = Math.floor(Math.random() * 9999 + 999);
    const nonce = randomNum + heloo.globalData.userInfo.openid;
    const arr = [token, timestamp, nonce];
    const sortArr = arr.sort();
    const originSignature = sortArr.join("");
    const signature = util.sha1(originSignature);
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_GET_TOKEN,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          signature,
          timestamp,
          nonce: randomNum,
          openid: heloo.globalData.userInfo.openid,
        },
        success(data) {
          if (data.data.status == 1) {
            resolve(true);
          } else {
            console.log("error___");
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject(false);
          }
        },
        fail(res) {
          console.log(res, "error");
          reject(false);
        },
      });
    });
  },
  getActivityDetail(id, openid) {
    let _this = this;
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_GET_CHRISTMAS_CALL_DETAIL,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          sponsorSeq: id,
          openid,
        },
        success(data) {
          if (data.data.status == 1) {
            const usersArr = data.data.data.users;
            const hasAttend = usersArr.some((item) => item.openid === openid);
            if (hasAttend) {
              // 参加过该祝福活动
              wx.redirectTo({
                // url:
                //   "/customize/pages/christmasCallReceive/index?bgType=" +
                //   _this.data.bgType +"&sponsorSeq=" + _this.data.sponsorSeq +
                //   "&shareType=" +
                //   _this.data.shareType,
                url: `/customize/pages/christmasCallReceive/index?bgType=${_this.data.bgType}&sponsorSeq=${_this.data.sponsorSeq}&shareType=${_this.data.shareType}`,
              });
            }
            _this.setData({
              bgType: data.data.data.resource.backgroundPictureId,
            });
            resolve();
          } else {
            console.log("error___");
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
          }
        },
        fail(res) {
          console.log(res, "error");
          _this.setData({
            loading: false,
          });
        },
      });
    });
  },
  // 更新助力情况
  refreshPower(usersArr, openid) {
    let sponsor = {};
    let primaryUser = [];
    let restUser = [];
    if (usersArr && usersArr.length > 0) {
      usersArr.forEach((user) => {
        if (user.sponsorType === "1") {
          sponsor = user;
        } else {
          primaryUser.push(user);
        }
        if (user.openid === openid) {
          // 判断是否已助力用户
          this.setData({
            powerFlag: false,
          });
        }
      });

      if (primaryUser.length < this.data.memberNum) {
        restUser = new Array(this.data.memberNum - primaryUser.length); // 仍需助力人数
      }
      this.setData({
        sponsor,
        hasRedBag: sponsor.sponsorBagType === "1", // 1:带有分享红包 2:超出分享次数限制
        senderMsg: sponsor.nickname,
        header: sponsor.header,
        primaryUser,
        restUser,
      });
    }
  },
  goHome() {
    wx.redirectTo({
      url: "/customize/pages/index/index",
    });
  },
  async receiveCall() {
    console.log("receiveCall", this.data.isLoading);
    if (this.data.isLoading) return;
    let _this = this;
    // wx.showLoading({
    //   title: "加载中...",
    //   icon: "none",
    // });
    if (_this.data.isLogin) {
      if (_this.data.isActivated) {
        // 会员
        wx.redirectTo({
          url:
            "/customize/pages/christmasCallReceive/index?bgType=" +
            this.data.bgType +
            "&sponsorSeq=" +
            this.data.sponsorSeq +
            "&shareType=" +
            this.data.shareType,
        });
      } else {
        wx.showModal({
          title: "",
          content: "您还未注册会员,请前往注册",
          confirmText: "立即前往",
          success(res) {
            if (res.confirm) {
              _this.linkToRegister();
            }
          },
        });
      }
    } else {
      _this.getInfo();
    }
  },
  linkToRegister() {
    wx.navigateTo({
      url: "/pages/register-member/index?completeType=back",
    });
  },
  // 头像授权
  getInfo() {
    let that = this;
    if (wx.canIUse("getUserProfile")) {
      wx.getUserProfile({
        lang: "zh_CN",
        desc: "本次授权用于个人信息显示",
        success(res) {
          wx.showLoading({
            title: "加载中...",
            mask: true,
          });
          let unionid = "";
          let openid = "";
          if (that.data.userInfo) {
            unionid = that.data.userInfo.unionid;
            openid = that.data.userInfo.openid;
          } else {
            unionid = heloo.globalData.register.unionid;
            openid = heloo.globalData.register.openid;
          }
          wx.request({
            url: heloo.globalData.url + "/api/getunionid",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              userInfo: JSON.stringify(res.userInfo),
              unionid: unionid,
              openid: openid,
              reference: that.data.fromId,
              source: that.data.source,
            },
            success: async (data) => {
              wx.hideLoading();
              that.setData({
                userInfo: data.data.wxuser,
              });
              heloo.globalData.userInfo = data.data.wxuser;
              await that.loadInit();
              // that.setData({
              //   isLoading: false,
              // });
              that.receiveCall();
            },
          });
        },
      });
    } else {
      wx.showModal({
        content: "微信客户端版本过低，请更新后重试！",
        showCancel: false,
        confirmText: "我知道了",
      });
    }
  },
  linkToActivity() {
    wx.navigateTo({
      url: "/customize/pages/christmasActivity/index",
    });
  },
});
