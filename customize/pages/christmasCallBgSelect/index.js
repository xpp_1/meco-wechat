/**
 * @description 来电背景选择
 */
import heloo from "../../utils/heloo";
Page({
  data: {
    barHeight: 2, // 导航栏高度
    userInfo: {}, // 当前用户信息
    curBgUrl: "",
    proList: [
      {
        id: 1,
        type: "default",
        selected: true,
        isDefault: true,
      },
      {
        id: 2,
        type: "1",
        selected: false,
      },
      {
        id: 3,
        type: "2",
        selected: false,
      },
      {
        id: 4,
        type: "3",
        selected: false,
      },
      {
        id: 5,
        type: "4",
        selected: false,
      },
    ],
  },
  onLoad() {
    this.setData({
      userInfo: heloo.globalData.userInfo,
    });
  },
  //滑动获取选中商品
  getSelectItem: function (e) {
    var that = this;
    var itemWidth = (e.detail.scrollWidth - 50) / that.data.proList.length; //每个商品的宽度
    var scrollLeft = e.detail.scrollLeft; //滚动宽度
    var curIndex = Math.round(scrollLeft / itemWidth); //通过Math.round方法对滚动大于一半的位置进行进位 并且位置设为居中
    for (var i = 0, len = that.data.proList.length; i < len; ++i) {
      that.data.proList[i].selected = false;
    }
    that.data.proList[curIndex].selected = true;
    that.setData({
      proList: that.data.proList,
      giftNo: this.data.proList[curIndex].id,
    });
  },
  // 来电背景选择
  onSelectBg() {
    this.setData({
      isShow: true,
    });
  },
  onSubmit() {
    var url = "";
    this.data.proList.forEach((item) => {
      if (item.selected) {
        heloo.globalData.christmasBg = item.type;
      }
    });
    wx.navigateBack({
      url: "/customize/pages/christmasCallMake/index",
    });
  },
  getNavBarHeight(e) {
    this.setData({ barHeight: e.detail.barHeight });
  },
});
