// h5唤起微信支付中间页（快闪）
import apiConstants from "../../../../server/apiConstants";
Page({
  data: {
    payOption: null,
  },
  onLoad(options) {
    console.log(options, "options");
    const { payParam } = options;
    let _this = this;
    _this.pay(payParam);
  },
  pay(payParam) {
    let _this = this;
    console.log(payParam, "payParam");
    // var objPay = payParam ? payParam : {};
    var objPay = payParam ? JSON.parse(decodeURIComponent(payParam)) : {};
    console.log("objPay_____", objPay);
    const order = objPay.package.slice(10);
    this.setData({
      payOption: objPay,
    });
    wx.requestPayment({
      appId: objPay.appId,
      timeStamp: objPay.timeStamp,
      nonceStr: objPay.nonceStr,
      package: objPay.package,
      signType: "RSA", //微信签名方式
      paySign: objPay.paySign,
      // appId: "wxb8a223205e2083f0", //appId，由商户传入
      // timeStamp: "1686292311933", //时间戳，单位s
      // nonceStr: "ohTlw7gc48KcYMYIIiANcjTCbam8vmrs", //随机串
      // package: "prepay_id=wx09143151850083e5a265fac17c93810000",
      // signType: "RSA", //微信签名方式
      // paySign:
      //   "eTyjcbTsRq0ssu5JBFO30nU4puFPgP3mhhDC6BVmHoJIvyqv23aCcxPkpwn2TvsFVryJ4eHWkEpznDiZFAQwh4mtZT3wh+LAHUUFPvXh2oK0Cy1YBJN3kgbxUY2d8eOSLAcu+CD/xKJrtwsiyFPnbTL+QbEEJVYP0gpkFs6nCs0sVr6RPiBXXN5N9M76KOMCZAWCKcYp7lj8r6AVm8LNT9LYfve6ITjWthOiyagbCZsAzwizl4h6jqTPGqS8V28E4E0x3kyE/GU/6wOyMG7ys8VkXqGkgvzVuYtpQ/XT+YHnx9644eiJQC0lu7nWQ4w8mppMuThgS6Ul3G3UMMpMlQ==", //微信签名
      //小程序微信支付成功的回调通知
      success: function (res) {
        console.log("支付成功");
        _this.setPayStatus(1, objPay.openId, order);
        //微信支付成功页
        // wx.redirectTo({
        //   url:
        //     "/customize/pages/offlineActivity/flashActivity/index?scene=" +
        //     objPay.actNo +
        //     "enterType=scan",
        // });
      },
      //小程序支付失败的回调通知
      fail: function (res) {
        console.log("支付失败");
        console.log(res);
        _this.setPayStatus(0, objPay.openId, order);
        wx.redirectTo({
          url:
            "/customize/pages/offlineActivity/flashActivity/index?scene=" +
            _this.data.payOption.actNo +
            "&enterType=scan",
        });
        // wx.navigateBack();
        // wx.redirectTo({
        //   url:
        //     "/customize/pages/offlineActivity/flashActivity/index?scene=" +
        //     objPay.actNo +
        //     "&enterType=scan",
        // });
      },
    });
  },

  setPayStatus(status, openId, order) {
    let _this = this;
    wx.request({
      url: apiConstants.HOST_NAME_SET_PROMOTION_PAY_STATUS,
      method: "POST",
      header: {
        // "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        mediaOrder: order, //微信订单号
        openId: openId,
        payStatus: status,
      },
      success(data) {
        if (status == 1) {
          wx.redirectTo({
            url:
              "/customize/pages/offlineActivity/flashActivity/index?scene=" +
              _this.data.payOption.actNo +
              "&enterType=scan",
          });
        }
      },
      fail(res) {},
    });
  },
});
