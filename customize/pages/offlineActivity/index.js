/**
 * @description 快闪活动
 */
import heloo from "../../utils/heloo";
// const { mai } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
Page({
  data: {
    needNum: 4, // 所需助力人数
    activityByMeList: [],
    activityByOthersList: [],
    curTab: 0, // 0 我发起的 1 我收到的
    tabList: ["我发出的", "我收到的"],
    isActivated: null, // 是否会员
    barHeight: 0,
    src: null,
    options: null,
    webPageType: "",
    isWebShow: true,
    version: "",
  },
  // onShareAppMessage: function (res) {
  //   //点击按钮
  //   console.log(res, "res", this.data.options);
  //   if (res.from == "button") {
  //     const { scene, shareCode } = this.data.options;
  //     const sharePath =
  //       "/customize/pages/offlineActivity/index?enterType=share&scene=" +
  //       scene +
  //       "&shareCode=" +
  //       shareCode;
  //     console.log(sharePath, "sharePath");

  //     return {
  //       path: sharePath,
  //       title: "Meco·蜜谷果汁茶校园派饮季",
  //       imageUrl:
  //         "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/lfyDY/mecoSchool/shareCardBg.jpg",
  //     };
  //   }
  // },
  async onLoad(options) {
    console.log("[options]:", options);
    const { webPageType = "" } = options;
    this.setData({
      options,
      webPageType,
    });
    console.log(this.data.webPageType, "type");

    let _this = this;
    // 授权信息判断
    heloo.wxLogin({ loadType: "async" }).then(
      async (res) => {
        console.log("已注册", res);

        _this.getWebUrl();

        // this.getCurLocation();

        wx.hideLoading();
      },
      (res) => {
        _this.getWebUrl();
        wx.hideLoading();
        console.log("未注册", res);
      }
    );
  },
  async getWebUrl() {
    let _this = this;
    //授权
    const maiOpenId = await mai.member.getOpenId();
    if (!maiOpenId) {
      await mai.member.signin();
    }
    const openId = await mai.member.getOpenId();
    const unionId = await mai.member.getUnionId();
    const member = await mai.member.getDetail(["Card", "Properties"]);
    console.log(member, "member", unionId);

    wx.request({
      url: heloo.globalData.url + "/api/getUserInfoByUnionid",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        unionid: unionId,
      },
      success(data) {
        console.log("头像昵称", data);
        const header = data?.data?.data?.header || "";
        const nickname = data?.data?.data?.nickname || "";
        const actNo = _this.data.options?.scene || "";
        let baseUrl =
          "https://playbill.chinaxpp.com/promotion-h5/#/pages/home/index";
        const accountInfo = wx.getAccountInfoSync();
        if (
          accountInfo.miniProgram.envVersion === "trial" ||
          accountInfo.miniProgram.envVersion === "develop"
        ) {
          // 体验版
          baseUrl =
            "https://playbill.chinaxpp.com/promotion-h5_test/#/pages/home/index"; // 测试
        }

        let web_url =
          baseUrl +
          "?openId=" +
          openId +11+
          "&avatar=" +
          header +
          "&name=" +
          nickname;
        if (_this.data.options?.webPageType == "share") {
          //显示分享中间页面
          _this.setData({
            isWebShow: false,
          });
        } else {
          if (_this.data.options?.enterType == "share") {
            //点击分享卡片进入
            const { scene, shareCode } = _this.data.options;
            web_url =
              web_url +
              "&actNo=" +
              scene +
              "&shareCode=" +
              shareCode +
              "&enterType=share";
          } else {
            web_url = web_url + "&actNo=" + actNo;
          }
          _this.setData({
            isWebShow: true,
          });
        }

        console.log(web_url, "web_url");
        _this.setData({
          src: web_url,
        });
      },
    });
  },
  getCurLocation() {
    let _this = this;
    return new Promise((resolve, reject) => {
      wx.getLocation({
        type: "wgs84",
        success(res) {
          console.log(res, "resaddress");
          resolve(res);
        },
        fail: function (info) {
          //失败回调
          console.log(info, "获取地址失败");
          wx.hideLoading();
          wx.showModal({
            title: "温馨提示",
            content:
              "无法获取您的地理位置，请打开系统定位和微信定位，并允许小程序获取您的位置",
            showCancel: false,
            success(res) {
              if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: "授权成功",
                        icon: "success",
                        duration: 1000,
                      });
                      //再次授权，调用getLocationt的API
                      _this.getCurLocation();
                    } else {
                      wx.showToast({
                        title: "授权失败，即将返回上一页",
                        icon: "none",
                        duration: 2000,
                      });
                      setTimeout(() => {
                        wx.navigateBack({
                          delta: 1,
                        });
                      }, 2000);
                    }
                  },
                });
              }
            },
          });
        },
      });
    });
  },
  getH5Message(e) {
    console.log(e, "获取到的message");
  },
});
