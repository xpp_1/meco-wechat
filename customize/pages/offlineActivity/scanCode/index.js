/**
 * @description 促销活动-一元乐享 进入方式：普通链接、分享链接、扫描太阳码 enterType:scan 扫码 share:分享卡片 primary:普通链接
 */
import heloo from "../../../utils/heloo";
// const { mai } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
Page({
  data: {
    enterType: "",
    scene: "",
    shareCode: "",
  },

  async onLoad(options) {
    console.log(options, "scanoptions");
    console.log(JSON.parse(options.urlParams), "scanoptionsJSon");
    const { enterType, scene, shareCode, checkId } = JSON.parse(
      options.urlParams
    );
    let _this = this;
    _this.setData({
      enterType,
      scene,
      shareCode,
    });

    wx.scanCode({
      success(res) {
        console.log(res.path, "res.path");
        //截取核销门店的活动编码
        const findStr = "scene=";
        const checkIdx = res.path.indexOf(findStr) + findStr.length;
        const checkActNo = res.path.substring(checkIdx, res.path.length);
        console.log(checkActNo, "checkActNo");
        if (checkActNo && checkIdx) {
          const pageUrl =
            "/customize/pages/offlineActivity/oneShare/index?scene=" +
            scene +
            "&enterType=" +
            enterType +
            "&shareCode=" +
            shareCode +
            "&checkAct=" +
            checkActNo +
            "&checkId=" +
            checkId;
          console.log(pageUrl, "pageUrl");
          wx.redirectTo({
            url: pageUrl,
          });
        } else {
          _this.onBack();
        }
      },
      fail(res) {
        console.log("fail", res);
        _this.onBack();
      },
    });
  },

  //返回原有页面
  onBack() {
    const { scene, enterType, shareCode } = this.data;
    const pageUrl =
      "/customize/pages/offlineActivity/oneShare/index?scene=" +
      scene +
      "&enterType=" +
      enterType +
      "&shareCode=" +
      shareCode;
    console.log(pageUrl, "pageUrlback");
    wx.redirectTo({
      url: pageUrl,
    });
  },
});
