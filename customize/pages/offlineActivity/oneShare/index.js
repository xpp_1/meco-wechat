/**
 * @description 促销活动-一元乐享 进入方式：普通链接、分享链接、扫描太阳码 enterType:scan 扫码 share:分享卡片 primary:普通链接
 */
import heloo from "../../../utils/heloo";
// const { mai } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
Page({
  data: {
    src: null,
    options: null,
    webPageType: "",
    isWebShow: true,
    version: "",
    isScanInner: false, // 是否扫码进入
  },
  onShareAppMessage: function (res) {
    //点击按钮
    console.log(res, "res", this.data.options);
    if (res.from == "button") {
      const { scene, shareCode } = this.data.options;
      const sharePath =
        "/customize/pages/offlineActivity/oneShare/index?enterType=share&scene=" +
        scene +
        "&shareCode=" +
        shareCode;
      console.log(sharePath, "sharePath");
      return {
        path: sharePath,
        title: "Meco·蜜谷果汁茶1元乐享活动",
        imageUrl:
          "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/lfyDY/mecoOne/shareCardBg.jpg",
      };
    }
  },
  async onLoad(options) {
    const pages = getCurrentPages();
    console.log(pages, "pages");

    const appOptions = wx.getLaunchOptionsSync();
    console.log(appOptions, "[options]:", options);

    if (options.enterType == "scan") {
      // 测试用，正式版本可删除
      this.setData({
        isScanInner: true,
      });
    }
    //正式
    if (
      [1011, 1012, 1013, 1047, 1048, 1049].includes(appOptions.scene) &&
      options.enterType !== "primary" &&
      options.enterType !== "share"
    ) {
      // 扫描二维码进入
      this.setData({
        isScanInner: true,
      });
    }
    const { webPageType = "" } = options;
    this.setData({
      options,
      webPageType,
    });
    let _this = this;
    // 授权信息判断
    heloo.wxLogin({ loadType: "async" }).then(
      async (res) => {
        console.log("已注册", res);
        _this.getWebUrl();
        wx.hideLoading();
      },
      (res) => {
        _this.getWebUrl();
        wx.hideLoading();
        console.log("未注册", res);
      }
    );
  },
  async getWebUrl() {
    let _this = this;
    //授权
    const maiOpenId = await mai.member.getOpenId();
    if (!maiOpenId) {
      await mai.member.signin();
    }
    const openId = await mai.member.getOpenId();
    const unionId = await mai.member.getUnionId();
    const member = await mai.member.getDetail(["Card", "Properties"]);
    console.log(member, "member", unionId);

    wx.request({
      url: heloo.globalData.url + "/api/getUserInfoByUnionid",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        unionid: unionId,
      },
      success(data) {
        console.log("头像昵称", data);
        const header = data?.data?.data?.header || "";
        const nickname = data?.data?.data?.nickname || "";
        const checkId = _this.data.options.checkId || "";
        const checkAct = _this.data.options.checkAct || "";
        let baseUrl =
          "https://playbill.chinaxpp.com/promotion-h5/#/pages/oneHome/index"; //正式
        const accountInfo = wx.getAccountInfoSync();
        if (
          accountInfo.miniProgram.envVersion === "trial" ||
          accountInfo.miniProgram.envVersion === "develop"
        ) {
          // 体验版
          baseUrl =
            "https://playbill.chinaxpp.com/promotion-h5_test/#/pages/oneHome/index"; // 测试
        }

        let web_url =
          baseUrl +
          "?openId=" +
          openId +
          "&avatar=" +
          header +
          "&name=" +
          nickname +
          "&checkId=" +
          checkId +
          "&checkAct=" +
          checkAct;
        if (_this.data.options?.webPageType == "share") {
          //显示分享中间页面
          _this.setData({
            isWebShow: false,
          });
        } else {
          const {
            scene = "",
            shareCode = "",
            enterType = "",
          } = _this.data.options;
          web_url = web_url + "&actNo=" + scene;
          if (_this.data.isScanInner || enterType == "scan") {
            //扫码进入
            web_url = web_url + "&enterType=scan";
          } else {
            if (enterType == "share") {
              //点击分享卡片进入
              web_url =
                web_url + "&shareCode=" + shareCode + "&enterType=share";
            } else if (enterType == "primary") {
              //普通链接进入
              web_url = web_url + "&enterType=primary";
            }
          }

          _this.setData({
            isWebShow: true,
          });
        }

        console.log(web_url, "web_url");
        _this.setData({
          src: web_url,
        });
      },
    });
  },
  getH5Message(e) {
    console.log(e, "获取到的message");
  },
});
