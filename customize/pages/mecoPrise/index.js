import {
  baseConfig
} from "../../utils/mecoBaseConfig.js"
var QRCode = require("../../utils/weapp.qrcode.min")

import {
  wxloinSaveData
} from "../../utils/mecoUserInfo.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    priseResult: 0,
    uri: '',
    code: '',
    codeImg: '',
    activityId: '',
    uuid: '',
    source: '', //从哪个页面跳转
    codeStatus: true, //二维码状态
    hasHit: false,
    isHited: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getUserLocation()
    wxloinSaveData().then(res => {
      // console.log(res);
      if (!options.source) {
        this.checkCode()
      }

    })
    if (options.uri && options.code) {
      this.setData({
        uri: options.uri,
        code: options.code
      })
    }
    if (options.promotionId && options.uuid && options.code && options.uri) {
      this.setData({
        activityId: options.promotionId,
        uuid: options.uuid,
        code: options.code,
        uri: options.uri,
      })
    }
    // if (this.data.code || this.data.uri) {
    // }

    if (options.source) {
      this.setData({
        source: options.source,
        priseResult: 3
      })
      this.generateQRCode(`HTTP://AAX6.CN/MECO?activityId=${this.data.activityId}&uuid=${this.data.uuid}&code=${this.data.code}&uri=${this.data.uri}`)

    }

  },
  closeDialog() {
    if (this.data.source == "list") {
      wx.navigateBack()
    } else {
      // this.setData({
      //   priseResult: 0
      // })
      // return
      if (this.data.isHited) {
        this.setData({
          priseResult: 1
        })
      } else {
        this.setData({
          priseResult: 2
        })
      }

    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },
  //立即抽奖
  drawNow() {
    let that = this
    //判断登录、授权位置
    //如果没有授权地理位置
    if (!wx.getStorageSync('mecoLatitude')) {
      this.getUserLocation()
    }
    if (!that.data.codeStatus) {
      wx.showToast({
        title: '二维码已使用',
        icon: 'error'
      })
      return
    }
    if (that.data.code == "" && that.data.uri == "") {
      wx.showToast({
        title: '已抽奖',
        icon: "error"
      })
      return
    }
    if (that.data.hasHit) {
      wx.showToast({
        title: '已抽奖',
        icon: "error"
      })
      return
    }
    wx.showLoading()
    //查询二维码信息和活动信息
    wx.request({
      url: baseConfig.UCODE_URL + `/tag/v2/promotion?longitude=${wx.getStorageSync('mecoLongitude')}&latitude=${wx.getStorageSync('mecoLatitude')}`,
      method: "GET",
      header: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + wx.getStorageSync('UMTOKEN'),
        serialId: wx.getStorageSync('SESSION_INFO').serialId,
      },
      data: {
        code: this.data.code,
        uri: this.data.uri,
        // appId: baseConfig.appId
      },
      success(res) {
        if (res.statusCode == 200 && res.data.publishStatus == "PROCESSING") {
          that.setData({
            activityId: res.data.id
          })
          //抽奖
          wx.request({
            url: baseConfig.UCODE_URL + `/lottery/v2/tag?longitude=${wx.getStorageSync('mecoLongitude')}&latitude=${wx.getStorageSync('mecoLatitude')}`,
            method: "POST",
            header: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + wx.getStorageSync('UMTOKEN'),
              serialId: wx.getStorageSync('SESSION_INFO').serialId,
            },
            data: {
              code: that.data.code,
              uri: that.data.uri,
              xppMeco: true
            },
            success(resp) {
              wx.hideLoading()
              if (res.statusCode == 200 && resp.data.isHit) {
                wx.hideLoading()
                that.setData({
                  priseResult: 1,
                  uuid: resp.data.uuid,
                  hasHit: true,
                  isHited: true
                })
              } else {
                that.setData({
                  priseResult: 2,
                  hasHit: true,
                  isHited: false
                })
              }
            },
            fail(err) {
              wx.hideLoading()
              that.setData({
                priseResult: 2
              })
            }
          });
        } else if (res.data.publishStatus == "NO_STARTED") {
          wx.showToast({
            title: '活动未开始',
          })
          wx.hideLoading()

        } else if (res.data.publishStatus == "PAUSE") {
          wx.showToast({
            title: '活动已暂停',
          })
          wx.hideLoading()

        } else if (res.data.publishStatus == "ENDED") {
          wx.showToast({
            title: '活动已结束',
          })
          wx.hideLoading()

        } else {
          wx.hideLoading()
          wx.showToast({
            title: res.data.emsg,
            icon: 'error'
          })
        }
      },
      fail(err) {
        wx.hideLoading()
        wx.showToast({
          title: err.data.emsg,
        })
      },
      complete() {}
    });

  },
  //判断二维码状态
  checkCode() {
    let that = this
    //判断登录、授权位置
    //如果没有授权地理位置
    if (!wx.getStorageSync('mecoLatitude')) {
      this.getUserLocation()
    }
    wx.showLoading()
    //查询二维码信息和活动信息
    wx.request({
      url: baseConfig.UCODE_URL + `/tag/v2/promotion?longitude=${wx.getStorageSync('mecoLongitude')}&latitude=${wx.getStorageSync('mecoLatitude')}`,
      method: "GET",
      header: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + wx.getStorageSync('UMTOKEN'),
        serialId: wx.getStorageSync('SESSION_INFO').serialId,
      },
      data: {
        code: this.data.code,
        uri: this.data.uri,
        // appId: baseConfig.appId
      },
      success(res) {
        wx.hideLoading()
        if (res.statusCode == 200) {
          if (!res.data.publishStatus) {
            wx.showToast({
              title: '您不在活动区域，不能参与抽奖~',
              icon: 'none',
              duration: 2000
            })
            that.setData({
              codeStatus: false,
            })
            return
          }
          if (res.data.tagInfo.status == "DEAD") {
            wx.showToast({
              title: '二维码已失效',
              icon: 'error',
            })
            that.setData({
              codeStatus: false,
            })
            return
          } else if (res.data.tagInfo.status == "LOTTERY") {
            wx.showToast({
              title: '二维码已被使用',
              icon: 'error'
            })
            that.setData({
              codeStatus: false,
            })
          } else if (res.data.tagInfo.status == "SCRAP") {
            wx.showToast({
              title: '二维码已报废',
              icon: 'error'
            })
            that.setData({
              codeStatus: false,
            })
          } else if (res.data.tagInfo.status == "NO_ACT") {
            wx.showToast({
              title: '二维码未激活',
              icon: 'error'
            })
            that.setData({
              codeStatus: false,
            })
          } else {
            that.setData({
              codeStatus: true,
            })
            // that.drawNow()
          }
        }
      },
      fail(err) {
        // wx.hideLoading()
        wx.showToast({
          title: err.data.emsg,
        })
      },
      complete() {
        // wx.hideLoading()
      }
    });
  },
  //立即核销
  goVer() {
    this.generateQRCode(`HTTP://AAX6.CN/MECO?activityId=${this.data.activityId}&uuid=${this.data.uuid}&code=${this.data.code}&uri=${this.data.uri}`)

    this.setData({
      priseResult: 3
    })
  },
  // 生成二维码
  generateQRCode(text) {
    wx.showLoading()
    const W = wx.getSystemInfoSync().windowWidth;
    const rate = 750.0 / W;
    const qrcode_w = 320 / rate;
    new QRCode({
      canvasId: 'qrcode',
      text: text,
      width: qrcode_w,
      height: qrcode_w,
    })
    wx.hideLoading()
    return
  },
  goShop() {
    wx.navigateToMiniProgram({
      appId: 'wx52b0f10de9c8e0bb',
      // path: '/member/pages/goods-list/index',
      path: 'goods/goods-detail/goods-detail?id=366757788835776',
      envVersion: 'release', //体验版 trial 开发版 develop  正式版 release
    })
  },
  //规则
  viewRule() {
    wx.navigateTo({
      url: '../mecoRule/index?hideBtn=true',
    })
  },
  //券包
  priseRecord() {
    wx.navigateTo({
      url: '../mecoCardShop/index?selectId=0',
    })
  },
  //附近店铺
  viewShops() {
    wx.navigateTo({
      url: '../mecoCardShop/index?selectId=1',
    })
  },
  close() {
    this.setData({
      priseResult: 0
    })
  },
  getUserLocation() {
    let that = this
    wx.getLocation({
      type: "gcj02",
      isHighAccuracy: true,
      highAccuracyExpireTime: 3000,
      success: function (res) {
        const latitude = res.latitude
        const longitude = res.longitude
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
        });
        wx.setStorageSync('mecoLatitude', latitude)
        wx.setStorageSync('mecoLongitude', longitude)
        // wx.openLocation({
        //   latitude,
        //   longitude,
        //   scale: 18
        // })
      },
      fail: function (err) {
        wx.getSetting({
          success: (res) => {
            if (
              typeof res.authSetting["scope.userLocation"] != "undefined" &&
              !res.authSetting["scope.userLocation"]
            ) {
              // 用户拒绝了授权
              wx.showModal({
                title: "提示",
                content: "您拒绝了位置授权，可能无法参与部分活动，是否立即授权？",
                success: (res) => {
                  if (res.confirm) {
                    //打开设置 让用户点击允许 这样可以重新获取
                    wx.openSetting({
                      success: (res) => {
                        if (res.authSetting["scope.userLocation"]) {
                          // 授权成功，重新定位即可
                          wx.getLocation({
                            type: "gcj02",
                            isHighAccuracy: true,
                            highAccuracyExpireTime: 3000,
                            success: (res) => {
                              that.setData({
                                latitude: res.latitude,
                                longitude: res.longitude,
                              });
                            },
                            fail: (err) => {
                              console.log(err);
                            },
                          });
                        } else {
                          // 没有允许定位权限
                          wx.showToast({
                            title: "没有允许定位权限",
                            icon: "none",
                          });
                        }
                      },
                    });
                  }
                },
              });
            }
          },
        });
      },
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})