// pages/rotate/rotate.js
import heloo from "../../utils/heloo";
// const { mai } = getApp();
var timer = 0,
  times = 0,
  codeTimer = null,
  speed = 200;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    flag: false, // 点击开始旋转(节流)
    isTrue0: null, // 获得到的奖品
    deg: 0, // 旋转之后的度数
    userInfo: null, // 用户信息
    drawList: [], // 奖品列表
    integral: 234, //积分数量
    drawNum: 0, // 抽奖次数
    free: 0, // 免费抽奖次数
    prizeType: null, // 中奖类型
    fromId: null, // 发送邀请人id
    isPrize: true, // 隐藏中奖弹窗
    prize: {}, // 中奖物品
    adv: [], // 轮播
    region: ["", "", ""], // 省市区
    address: "", // 详情地址
    userName: "", // 收货人
    userPhone: "", // 手机号
    shareFriend: [], // 中奖人公告
    tel: "", //验证码的手机号
    code: "", // 验证码
    getCodeText: "获取验证码",
    time: 60,
    // 一元换购券
    isShowOne: false, // 中奖弹出层
    isNuclearSales: false, // 核销券弹窗
    latitude: "", // 维度
    longitude: "", // 经度
    source: "", // 来源
    buildingList: [], // 门店列表
    isWrite: false, // 核销
    qrCodeImg: "", // 二维码
    testUrl: "https://ssl.meco.chinaxpp.com", // heloo.globalData.url
    // testUrl: "http://192.168.3.36:8081",
    prizeList: [],
    activeNum: 0, //转动位置
    stopNum: 0, //停止位置
    chance: 0,
    rotateBtnUrl: "",
    phoneshow:false,
    phoneNumber:''
    // isActivated: true,
  },
  // computed: {
  //   rotateBtnUrl: () => {
  //     return this.data.chance + this.data.free == 0
  //       ? "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/luckyDraw/rotateBtnActive.png"
  //       : "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/luckyDraw/rotateBtn.png";
  //   },
  // },
  /**
   * 生命周期函数--监听页面加载
   */
  async onLoad(options) {
    let that = this;
    console.log(options);
    console.log('抽奖页面');
    if (options.scene) {
      // 扫码进入
      this.setData({
        source: 9,
      });
      console.log("扫码进入");
      // 用户已登录并且是注册过的
      if (heloo.globalData.userInfo.unionid) {
        console.log("扫码,用户已登录并且是注册过的");
        that.setData({
          userInfo: heloo.globalData.userInfo,
        });
        that.userLogin();
      } else {
        if (heloo.globalData.register && heloo.globalData.register.unionid) {
          // 用户已登录未注册 弹出弹框提醒用户授权注册
          console.log("扫码,用户已登录未注册 弹出弹框提醒用户授权注册");
          wx.showModal({
            content: "您还未授权注册，是否进行注册？",
            confirmText: "是",
            cancelText: "否",
            success(res) {
              if (res.confirm) {
                that.getInfo();
              } else {
                wx.reLaunch({
                  url: "../index/index",
                });
              }
            },
          });
        } else {
          heloo.wxLogin().then(
            () => {
              // 用户未登录，已注册
              console.log("扫码,用户未登录，已注册");
              that.setData({
                userInfo: heloo.globalData.userInfo,
              });
              that.userLogin();
            },
            () => {
              // 用户未登录，未注册  弹出弹框提醒用户授权注册
              wx.showModal({
                content: "您还未授权注册，是否进行注册？",
                confirmText: "是",
                cancelText: "否",
                success(res) {
                  if (res.confirm) {
                    that.getInfo();
                  } else {
                    wx.reLaunch({
                      url: "../index/index",
                    });
                  }
                },
              });
            }
          );
        }
      }
    } else if (options.fromId) {
      console.log("分享链接进入");
      if (heloo.globalData.userInfo.unionid) {
        // 用户已登录并且是注册过的
        console.log("用户已登录并且是注册过的");
        that.setData({
          userInfo: heloo.globalData.userInfo,
        });
        that.userLogin();
      } else {
        if (heloo.globalData.register && heloo.globalData.register.unionid) {
          // 用户已登录未注册 弹出弹框提醒用户授权注册
          console.log("用户已登录未注册 弹出弹框提醒用户授权注册");
          wx.showModal({
            content: "您还未授权注册，是否进行注册？",
            confirmText: "是",
            cancelText: "否",
            success(res) {
              if (res.confirm) {
                that.getInfo();
              } else {
                wx.reLaunch({
                  url: "../index/index",
                });
              }
            },
          });
        } else {
          heloo.wxLogin().then(
            () => {
              // 用户未登录，已注册
              console.log("用户未登录，已注册");
              that.setData({
                userInfo: heloo.globalData.userInfo,
              });
              that.userLogin();
            },
            () => {
              // 用户未登录，未注册  弹出弹框提醒用户授权注册
              console.log("用户未登录，未注册  弹出弹框提醒用户授权注册");
              wx.showModal({
                content: "您还未授权注册，是否进行注册？",
                confirmText: "是",
                cancelText: "否",
                success(res) {
                  if (res.confirm) {
                    that.getInfo();
                  } else {
                    wx.reLaunch({
                      url: "../index/index",
                    });
                  }
                },
              });
            }
          );
        }
      }
      that.setData({
        fromId: options.fromId,
      });
      setTimeout(() => {
        // 邀请好友助力
        that.friendHelp();
      }, 3000);
    } else if (options.latitude && options.longitude) {
      this.setData({
        latitude: options.latitude,
        longitude: options.longitude,
        source: options.source,
      });
      that.userLogin();
      setTimeout(() => {
        that.getAddressList();
      }, 2500);
      // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
      heloo.writeTime(3, "/customize/pages/index/index");
    } else {
      console.log("主页面进入");
      await that.userLogin();
      // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
      heloo.writeTime(4, "/customize/pages/index/index");
    }
  },
  onUnload: function () {
    clearInterval(codeTimer);
  },
  async onShow() {
    var that = this;

    // const member = await mai.member.getDetail(["Card"]);
    // console.log(member.isActivated, "round");
    // this.setData({
    //   isActivated: member.isActivated,
    // });
    // 获取位置
    wx.getLocation({
      type: "wgs84",
      success: function (res) {
        that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
        });
      },
      fail: function (err) {
        wx.getSetting({
          success: (res) => {
            if (
              typeof res.authSetting["scope.userLocation"] != "undefined" &&
              !res.authSetting["scope.userLocation"]
            ) {
              // 用户拒绝了授权
              wx.showModal({
                title: "提示",
                content:
                  "您拒绝了位置授权，可能无法参与部分活动，是否立即授权？",
                success: (res) => {
                  if (res.confirm) {
                    //打开设置 让用户点击允许 这样可以重新获取
                    wx.openSetting({
                      success: (res) => {
                        if (res.authSetting["scope.userLocation"]) {
                          // 授权成功，重新定位即可
                          wx.getLocation({
                            type: "wgs84",
                            altitude: true,
                            isHighAccuracy: true,
                            success: (res) => {
                              that.setData({
                                latitude: res.latitude,
                                longitude: res.longitude,
                              });
                            },
                            fail: (err) => {
                              console.log(err);
                            },
                          });
                        } else {
                          // 没有允许定位权限
                          wx.showToast({
                            title: "没有允许定位权限",
                            icon: "none",
                          });
                        }
                      },
                    });
                  }
                },
              });
            }
          },
        });
      },
    });
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === "button") {
      // 来自页面内转发按钮
      return {
        title: "Meco真茶局",
        path:
          "/customize/pages/roundRotate/roundRotate?fromId=" +
          this.data.userInfo.id,
        // imageUrl:"/images/shareicon.jpg"
      };
    }
    return {
      title: "Meco真茶局",
      path: "/customize/pages/index/index",
      // imageUrl: "/images/shareicon.jpg"
    };
  },
  // 获取门店列表
  getAddressList() {
    let that = this;
    console.log(that.data.userInfo);
    wx.request({
      url: this.data.testUrl + "/api/xpppresent/range",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        longitude: that.data.longitude,
        latitude: that.data.latitude,
      },
      success(res) {
        if (res.data.status == 1) {
          // 处理距离信息
          res.data.data.forEach((el) => {
            let num = parseInt(Number(el.distance));
            el.distance = num > 1000 ? parseInt(num / 1000) + "km" : num + "m";
          });
          // console.log(res.data);
          that.setData({
            buildingList: res.data.data,
          });
        }
      },
    });
  },
  // 获取二维码
  getQrCode(prizeId) {
    let that = this;
    wx.request({
      url: this.data.testUrl + "/api/xpppresent/verificationList",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        longitude: this.data.longitude,
        latitude: this.data.latitude,
        presentAddrId: prizeId,
      },
      success(res) {
        console.log("qrcode", res.data.data);
        that.setData({
          qrCodeImg: res.data.data,
        });
      },
    });
  },
  // 去兑换
  goExchange() {
    this.setData({
      isShowOne: false,
      isWrite: true,
    });
  },
  //返回主页
  back() {
    wx.redirectTo({
      url: "../index/index",
    });
  },
  //点击开始旋转
  rotate() {
    let that = this;
    let num = that.data.activeNum;
    times++;
    if (
      times > 50 &&
      that.data.prize &&
      that.data.activeNum == that.data.stopNum
    ) {
      clearTimeout(timer); // 清除转动定时器，停止转动
      times = 0;
      timer = 0;
      speed = 200;
      that.setData({
        isStart: false,
        stopNum: "",
        isPrize: false,
      });
    } else {
      if (times < 50) {
        speed -= 20; // 加快转动速度
      } else {
        speed += 20;
      }
      if (speed < 40) {
        speed = 40;
      }
      num++;
      if (num > 7) {
        num = 0;
      }

      that.setData({
        activeNum: num,
      });
      timer = setTimeout(that.rotate, speed);
    }
  },
  // 抽奖
  start() {
    let that = this;
    // if (that.data.isActivated) {
    if (that.data.userInfo) {
      if (that.data.chance > 0 || that.data.free > 0) {
        if (!that.data.isStart) {
          that.setData({
            isStart: true,
          });
          that.rotate();
          that.getPrize();
        }
      } else {
        wx.showToast({
          title: "次数不足",
          icon: "none",
        });
      }
    } else {
      that.getInfo();
    }
    // } else {
    //   wx.navigateTo({
    //     url: "/pages/register-member/index?completeType=back",
    //   });
    // }
  },
  // 活动规则
  goRule(e) {
    let type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: "../prizeRule/prizeRule?type=" + type,
    });
  },
  // 登录
  userLogin() {
    let that = this;
    wx.login({
      success(res) {
        wx.request({
          url: heloo.globalData.url + "/api/userIsNull",
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: {
            code: res.code,
          },
          success(data) {
            console.log('登录');
            console.log(data);
            if (data.data.status == 0) {
              let userInfo = {};
              userInfo.openid = data.data.openid;
              userInfo.unionid = data.data.unionid;
              userInfo.key = heloo.decryptDES(data.data.session_key_secret);
              heloo.globalData.register = userInfo;
              heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
              that.setData({
                isLogin: false,
              });
              // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
              heloo.writeTime(1, "pages/roundRotate/roundRotate");
              heloo.writeTime(4, "pages/roundRotate/roundRotate");
              that.getList();
            } else {
              that.setData({
                userInfo: data.data.wxuser,
              });
              console.log('状态码不为0');
              console.log(that.data.userInfo);
              heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
              heloo.globalData.userInfo = data.data.wxuser;
              that.getList();
              that.getAdv();
            }
          },
          fail(res) {
            console.log(res);
          },
        });
      },
    });
  },
  //奖品列表
  getList() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    let openid = "";
    if (that.data.userInfo) {
      openid = that.data.userInfo.openid;
      console.log(openid);
      if (that.data.source == 9) {
        that.scanRecord(openid);
      }
    }
    wx.request({
      url: this.data.testUrl + "/api/xpppresent/list",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        openid: openid,
      },
      success(data) {
        wx.hideLoading();
        console.log(heloo.globalData.userInfo);
        if (data.data.status == 1) {
          let row = {
            name: "start",
            isBtn: true,
          };
          let arr = [0, 1, 2, 7, 3, 6, 5, 4];
          let list = data.data.data.drawPresentList;
          list.map((item, index) => {
            item.active = arr[index];
          });
          list.splice(4, 0, row);
          const { freeDrawChance, drawChance } = data.data.data;
          that.setData({
            prizeList: list,
            free: freeDrawChance,
            chance: drawChance,
            rotateBtnUrl:
              freeDrawChance + drawChance == 0
                ? "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/luckyDraw/rotateBtnActive.png"
                : "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/luckyDraw/rotateBtn.png",
          });
        } else {
          wx.showToast({
            title: "活动过于火爆，请稍后重试", // 转盘偶发性空白
            icon: "none",
          });
        }
      },
      fail(res) {
        console.log(res);
      },
    });
  },
  // /api/meco/scanRecordLog
  scanRecord(opid) {
    wx.request({
      url: this.data.testUrl + `/api/meco/scanRecordLog?openid=${opid}`,
      method: "GET",
      success(res) {
        console.log(res.data);
      },
    });
  },
  //抽奖
  getPrize() {
    let that = this;

    wx.request({
      url: this.data.testUrl + "/api/xpppresent/draw",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        // wxid: "1",
        source: this.data.source,
        longitude: this.data.longitude,
        latitude: this.data.latitude,
        // source: 9,
        // longitude: "112.989855",
        // latitude: "28.112525",
      },
      success(data) {
        if (data.data.status == 1) {
          let list = that.data.prizeList;
          let prize = data.data.data;
          list.map((item) => {
            if (item.id == prize.drawPresentId) {
              prize.number = item.number;
              that.setData({
                stopNum: item.active,
                prize: prize,
              });
            }
          });
          if (that.data.free > 0) {
            let free = that.data.free;
            free--;
            that.setData({
              free: free,
            });
          } else if (that.data.chance > 0) {
            let chance = that.data.chance;
            let userInfo = that.data.userInfo;
            // userInfo.points = userInfo.points - 50;
            chance--;
            that.setData({
              chance: chance,
              userInfo: userInfo,
            });
            heloo.globalData.userInfo = userInfo;
          }
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
          });
          clearTimeout(timer); // 清除转动定时器，停止转动
          times = 0;
          timer = 0;
          speed = 200;
          that.setData({
            activeNum: 0,
            stopNum: "",
            isStart: false,
          });
        }
      },
    });
  },
  // 去核销
  goToExchange() {
    this.setData({
      isPrize: true,
      isNuclearSales: true,
    });
  },
  // 关闭核销
  closeNuclearSales() {
    this.setData({
      isNuclearSales: false,
    });
  },
  // 确定核销
  goToNuclearSales() {
    let that = this;
    wx.showLoading({
      title: "加载中",
      mask: true,
    });
    wx.request({
      url: `${that.data.testUrl}/manualWriteOff?wxid=${
        that.data.userInfo.id || that.data.wxId
      }&presentAddrId=${that.data.prize.id}`,
      method: "get",
      success(res) {
        if (res.data.code == 200) {
          wx.hideLoading({
            success() {
              wx.showToast({
                title: "核销成功",
              });
            },
          });
          that.setData({
            isNuclearSales: false,
          });
        } else {
          wx.hideLoading({
            success() {
              wx.showToast({
                title: "核销失败",
                icon: "none",
              });
            },
          });
        }
      },
      fail(err) {
        wx.showToast({
          title: "核销失败",
          icon: "none",
        });
      },
    });
  },
  // 授权
  getInfo() {
    let that = this;
    if (wx.canIUse("getUserProfile")) {
      wx.getUserProfile({
        lang: "zh_CN",
        desc: "本次授权用于个人信息显示",
        success(res) {
          console.log(res);
          wx.showLoading({
            title: "加载中...",
            mask: true,
          });
          let unionid = "";
          let openid = "";
          if (that.data.userInfo) {
            unionid = that.data.userInfo.unionid;
            openid = that.data.userInfo.openid;
          } else {
            unionid = heloo.globalData.register.unionid;
            openid = heloo.globalData.register.openid;
          }
          wx.request({
            url: heloo.globalData.url + "/api/getunionid",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              userInfo: JSON.stringify(res.userInfo),
              unionid: unionid,
              openid: openid,
              source: that.data.source == 9 ? "draw" : "nine",
            },
            success(data) {
              console.log(data);
              wx.hideLoading();
              that.setData({
                userInfo: data.data.wxuser,
              });
              heloo.globalData.userInfo = data.data.wxuser;
              that.userLogin();
            },
          });
        },
      });
    } else {
      wx.showModal({
        content: "微信客户端版本过低，请更新后重试！",
        showCancel: false,
        confirmText: "我知道了",
      });
    }
  },
  // 轮播
  getAdv() {
    let that = this;
    wx.request({
      url:
        heloo.globalData.url +
        `/api/xpppresent/getWinningRecord?wxid=${this.data.userInfo.id}`,
      method: "GET",
      success(res) {
        console.log(res.data);
        that.setData({
          shareFriend: res.data.heard,
          adv: res.data.data,
        });
      },
    });
  },
  // 关闭中奖弹窗
  closePrize() {
    this.setData({
      isPrize: true,
    });
  },
  // 地址信息
  bindRegionChange(e) {
    this.setData({
      region: e.detail.value,
    });
  },
  // 名字
  setName(e) {
    this.setData({
      userName: e.detail.value,
    });
  },
  // 手机号
  setPhone(e) {
    this.setData({
      userPhone: e.detail.value,
    });
  },
  // 详情地址
  setAdd(e) {
    this.setData({
      address: e.detail.value,
    });
  },
  //领取积分和红包
  getPoints() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/xpppresent/getPointsRedPack",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        presentAddrId: that.data.prize.id,
      },
      success(data) {
        wx.hideLoading();
        console.log(data.data);
        if (that.data.prize.presentName == "100积分") {
          let userInfo = that.data.userInfo;
          let integral = userInfo.points + 100;
          that.setData({
            "userInfo.points": integral,
          });
        } else if (that.data.prize.presentName == "5积分") {
          let userInfo = that.data.userInfo;
          let integral = userInfo.points + 5;
          that.setData({
            "userInfo.points": integral,
          });
        }
        if (data.data.status == 1) {
          that.setData({
            isPrize: true,
            prize: null,
          });

          wx.showToast({
            title: "领取成功！",
            icon: "success",
          });
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
          });
        }
      },
    });
  },
  	//获取输入的手机号
	getphoneNumber(e){
    console.log(e);
    this.setData({
      phoneNumber:e.detail.value &&e.detail.value.trim()
    })
  },
      	//校验手机号输入
	confirmphone(){
		let that = this
		var phoneReg = /^1[3456789]\d{9}$/;
		if (phoneReg.test(that.data.phoneNumber)) {
			heloo.globalData.userInfo.phoneNumber = that.data.phoneNumber
      console.log("手机号格式正确");
      wx.request({
        url: heloo.globalData.url + "/pointProduct/getPhoneNum",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          openid: that.data.userInfo.openid,
          sessionKey: heloo.globalData.userKey,
          phone:that.data.phoneNumber,
        },
        success: function (res) {
          wx.hideLoading();
          if (res.data.status == "1") {
            console.log(res);
            heloo.globalData.userInfo = res.data.userinfo;
            that.setData({
              userInfo: res.data.userinfo,
              phoneshow:false,
            })
             that.getPrize();
          } else {
            wx.showModal({
              title: "注册失败",
              content: res.data.msg,
            });
          }
        },
      });

		  } else {
        wx.showToast({
          title: '手机号格式不正确',
          icon:'none'
        })
		  }
	},
  getPhone() {
    console.log(that.data.userInfo);
    var that = this;
    if(that.data.userInfo.phoneNumber){
that.getPrize()
    }else {
      that.setData({
        phoneshow:true,
      })
    }

    // wx.checkSession({
    //   success: function () {
    //     if (e.detail.iv) {
    //       if (!that.data.userInfo) {
    //         wx.showModal({
    //           title: "授权失败",
    //           content: "尚未授权注册",
    //         });
    //         return;
    //       }
    //       wx.showLoading({
    //         title: "加载中...",
    //         mask: true,
    //       });
    //       wx.request({
    //         url: heloo.globalData.url + "/pointProduct/getPhoneNum",
    //         method: "POST",
    //         header: {
    //           "Content-Type": "application/x-www-form-urlencoded",
    //         },
    //         data: {
    //           openid: that.data.userInfo.openid,
    //           sessionKey: heloo.globalData.userKey,
    //           phonepagetype: "4",
    //           encryptedData: e.detail.encryptedData,
    //           iv: e.detail.iv,
    //         },
    //         success: function (res) {
    //           wx.hideLoading();
    //           if (res.data.status == "1") {
    //             setTimeout(() => {
    //               wx.request({
    //                 url: heloo.globalData.url + "/api/synchroUserinfo",
    //                 method: "POST",
    //                 header: {
    //                   "Content-Type": "application/x-www-form-urlencoded",
    //                 },
    //                 data: {
    //                   unionid: res.data.userinfo.unionid,
    //                 },
    //                 success(result) {
    //                   console.log(result.data.data);
    //                   heloo.globalData.userInfo = result.data.data;
    //                   that.setData({
    //                     userInfo: result.data.data,
    //                   });
    //                 },
    //               });
    //             }, 2000);
    //             setTimeout(() => {
    //               wx.request({
    //                 url: heloo.globalData.url + "/api/synchroUserinfo",
    //                 method: "POST",
    //                 header: {
    //                   "Content-Type": "application/x-www-form-urlencoded",
    //                 },
    //                 data: {
    //                   unionid: res.data.userinfo.unionid,
    //                 },
    //                 success(result) {
    //                   console.log(result.data.data);
    //                   heloo.globalData.userInfo = result.data.data;
    //                   that.setData({
    //                     userInfo: result.data.data,
    //                   });
    //                 },
    //               });
    //             }, 5000);
    //           } else {
    //             wx.showModal({
    //               title: "手机号解密失败",
    //               content: res.data.msg,
    //             });
    //           }
    //         },
    //       });
    //     }
    //   },
    //   fail: function () {
    //     wx.showModal({
    //       title: "授权失败",
    //       content: "登录状态过期，请重新授权！",
    //       success: function () {
    //         that.userLogin();
    //       },
    //     });
    //   },
    // });
  },
  //刷新积分 /
  refreshIntegral() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/synchroUserinfo",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        unionid: that.data.userInfo.unionid,
      },
      success(result) {
        console.log();
        if (result.data.status == 1) {
          heloo.globalData.userInfo = result.data.data;
          that.setData({
            userInfo: result.data.data,
          });
          wx.hideLoading({
            success: () => {
              wx.showToast({
                title: "刷新成功",
              });
            },
          });
        } else {
          wx.hideLoading({
            success: () => {
              wx.showToast({
                title: "刷新失败",
                icon: "none",
              });
            },
          });
        }
      },
      fail: () => {},
      complete: () => {},
    });
  },
  // 领取实物
  submitInfo() {
    let that = this;
    if (that.data.userName == "") {
      wx.showToast({
        title: "请填写姓名！",
        icon: "none",
      });
      return;
    }
    if (that.data.userPhone.length != 11) {
      wx.showToast({
        title: "请填写正确手机号！",
        icon: "none",
      });
      return;
    }
    if (that.data.region[2] == "") {
      wx.showToast({
        title: "请选择省市区！",
        icon: "none",
      });
      return;
    }
    if (that.data.address == "") {
      wx.showToast({
        title: "请填写详细地址！",
        icon: "none",
      });
      return;
    }
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/present/update",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        presentAddrId: that.data.prize.id,
        name: that.data.userName,
        phone: that.data.userPhone,
        province:
          that.data.region[0] + that.data.region[1] + that.data.region[2],
        shipaddr: that.data.address,
      },
      success(data) {
        wx.hideLoading();
        if (data.data.status == 1) {
          that.setData({
            userName: "",
            userPhone: "",
            address: "",
            region: ["", "", ""],
            isPrize: true,
          });
          wx.showToast({
            title: "信息已保存，静待发货",
            icon: "none",
          });
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
          });
        }
      },
    });
  },
  // 领取微商城券
  getcard() {
    wx.navigateToMiniProgram({
      appId: heloo.globalData.wm_shop_app_id,
      path: "/cms_design/index?isqdzz=1&tracepromotionid=100077529&productInstanceId=11970818643&vid=0",
    });
  },
  // 邀请好友
  friendHelp() {
    console.log("助力接口请求开始", heloo.globalData.userInfo.id);
    wx.request({
      url:
        heloo.globalData.url +
        `/api/xpppresent/inviteFriends?wxid=${heloo.globalData.userInfo.id}&launchId=${this.options.fromId}`,
      method: "GET",
      success(res) {
        console.log("助力接口请求结束");
        console.log(res.data.message);
      },
    });
  },
  // 验证码手机号
  setTel(e) {
    this.setData({
      tel: e.detail.value,
    });
  },
  // 点击获取验证码
  getCodeBtn() {
    let that = this;
    if (that.data.getCodeText == "获取验证码") {
      if (that.data.tel.length != 11) {
        wx.showToast({
          title: "请填写正确手机号！",
          icon: "none",
        });
        return;
      }
      that.setData({
        getCodeText: 60 + "s",
      });
      that.getCode();
      codeTimer = setInterval(() => {
        if (that.data.time < 1) {
          clearInterval(codeTimer);
          that.setData({
            getCodeText: "获取验证码",
            time: 60,
          });
        } else {
          let times = that.data.time;
          times--;
          that.setData({
            getCodeText: times + "s",
            time: times,
          });
        }
      }, 1000);
    }
  },
  // 发请求获取code
  getCode() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/xpppresent/getVilifyCode",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        phone: that.data.tel,
      },
      success: function (res) {
        wx.hideLoading();
        console.log(res);
        if (res.data.status == "1") {
          wx.showToast({
            title: "短信已发送",
          });
        } else {
          wx.showModal({
            title: "验证码失败",
            content: res.data.message,
          });
        }
      },
    });
  },
  // 获取到的验证码
  sendCode(e) {
    this.setData({
      code: e.detail.value,
    });
  },
  // 验证码领取奖品
  codeSubmit() {
    let that = this;
    if (that.data.tel.length != 11) {
      wx.showToast({
        title: "请填写正确手机号！",
        icon: "none",
      });
      return;
    }
    if (that.data.code == "") {
      wx.showToast({
        title: "请填写验证码！",
        icon: "none",
      });
      return;
    }
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/xpppresent/getMonthCardCode",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id,
        phone: that.data.tel,
        presentAddrId: that.data.prize.id,
        code: that.data.code,
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.status == "1") {
          wx.showToast({
            title: "领取成功！",
            icon: "success",
          });
          that.setData({
            isPrize: true,
            code: "",
            codePhone: "",
          });
        } else {
          wx.showModal({
            title: "领取失败",
            content: res.data.message,
          });
        }
      },
    });
  },
  // 一元换购
  closeOne() {
    this.setData({
      isShowOne: false,
      isWrite: false,
    });
  },
  // 打电话
  callTel(e) {
    wx.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.tel + "",
      success: function () {
        console.log("拨打电话成功！");
      },
    });
  },
});
