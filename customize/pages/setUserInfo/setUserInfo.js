const app = getApp()
const defaultAvatarUrl = 'https://mmbiz.qpic.cn/mmbiz/icTdbqWNOwNRna42FI242Lcia07jQodd2FJGIYQfG0LAJGFxM4FbnQP6yfMxBgJ0F3YRqJCJ1aPAK2dQagdusBZg/0'
import heloo from '../../utils/heloo';
Page({
  data: {
    // 本地获取的头像
    avatarUrl: defaultAvatarUrl,
    // 上传后获取的头像地址
    avatarUrl1: '',
    theme: wx.getSystemInfoSync().theme,
    unionid: null,
    nickname:''

  },
  onLoad(options) {
    this.setData({
      unionid: options.unionid
    })
    // 获取上一次
   this.getUserInfos()
    wx.onThemeChange((result) => {
      this.setData({
        theme: result.theme
      })
    })
  },
  onChooseAvatar(e) {
    const {
      avatarUrl
    } = e.detail
    this.setData({
      avatarUrl,
    })
    console.log('头像', this.data.avatarUrl)
    this.uploadImg()
  },
  formSubmit(e) {
    this.setData({
      nickname:e.detail.value.nickname
    })
    const that = this
    console.log(this.data.avatarUrl1,e.detail.value.nickname.length ,this.data.nickname );
    // if ( this.data.avatarUrl1.length === 0 || this.data.avatarUrl.length === 0) {
    //   wx.showToast({
    //     title: "请填写完整信息！",
    //     icon: "none",
    //   });
    //   return false
    // }
    if ( e.detail.value.nickname.length <= 0  ||  e.detail.value.nickname.length >= 15 ) {
      wx.showToast({
        title: "请填写呢称最长15个字符",
        icon: "none",
      });
      return false
    }
   
    console.log('昵称：', e.detail.value.nickname, '头像:', this.data.avatarUrl1)

    wx.request({
      url: heloo.globalData.url + "/api/updateUserInfo",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        unionid: that.data.unionid,
        nickname: e.detail.value.nickname,
        header: that.data.avatarUrl1,
      },
      success(data) {
        console.log(data);
        if (data.data.message == "更新成功") {
          // wx.showToast({
          //   title: "提交成功",
          //   icon: "none",
          // });
          wx.navigateBack({
            delta: 1
          });
        } else {
          title: "提交失败",
          wx.navigateBack({
            delta: 1
          });
        }
      },
    });
  },
  // 上传头像
  uploadImg() {
    var that = this;
    wx.showLoading({
      title: '上传中...',
      mask: true
    })
    console.log(that.data.avatarUrl, that.data.avatarUrl.split("http://tmp/")[1], that.data.avatarUrl.split("wxfile://")[1]);
    wx.uploadFile({
      url: heloo.globalData.url + '/imageUpWXTX', //仅为示例，非真实的接口地址
      filePath: that.data.avatarUrl,
      name: 'upfile',
      formData: {
        "fileName": that.data.avatarUrl.split("wxfile://")[1]
      },
      success(res) {
        wx.hideLoading();
        let data = JSON.parse(res.data);
        console.log(res,data);
        if (data.state == "SUCCESS") {
          //console.log(data.url);
          that.setData({
            avatarUrl1: data.url
          })

        }
      }
    })


  },
  getNick(e){
    console.log(e.detail.value);
    this.setData({
      nickname:e.detail.value
    })
  },
  // 获取上次头像
  getUserInfos(e){
    const that = this
    wx.request({
      url: heloo.globalData.url + "/api/getUserInfoByUnionid",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        unionid: this.data.unionid,
      },
      success(data) {
        console.log(data,data.data.data.header);
        that.setData({
          avatarUrl:data.data.data.header,
          nickname:data.data.data.nickname
        })
        console.log(that.data.avatarUrl);
      },
    });
  },
})