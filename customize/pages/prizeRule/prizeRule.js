// pages/prizeRule/prizeRule.js
// const app = getApp();
import heloo from "../../utils/heloo";
var codeTimer = null;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    type: 0,
    userInfo: null,
    prizeList: [],
    isPrize: true,
    prize: null,
    // 中奖信息填写
    region: ["", "", ""],
    userName: "",
    userPhone: "",
    address: "",
    // 兑换码
    code_text: "获取验证码",
    codePhone: "",
    code: "",
    codeTime: 60,
    wxId: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      type: options.type,
      userInfo: heloo.globalData.userInfo,
      wxId: options?.wxId, // 一杯一码红包页面进入的抽奖规则采用携带的wxId
    });

    if (options.type == 1) {
      this.getRecord();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(codeTimer);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: "Meco真茶局",
      path: "/customize/pages/index/index",
      // imageUrl: "/images/shareicon.jpg"
    };
  },
  getRecord() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      //heloo.globalData.url
      url: heloo.globalData.url + "/api/present/list",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        //type=1 大转盘type =2一杯一码 不传 全部
        // that.data.userInfo.id
        wxid: that.data.userInfo.id || that.data.wxId, //
        type: "1",
      },
      success(data) {
        wx.hideLoading();
        if (data.data.status == 1) {
          let list = data.data.data.myPresent;
          list.map((item) => {
            item.time = item.createTime.substring(0, 10);
          });
          that.setData({
            prizeList: list,
          });
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
          });
        }
      },
    });
  },
  getPrize(e) {
    let index = e.currentTarget.dataset.index;
    let prize = this.data.prizeList[index];
    // console.log(prize);
    this.setData({
      prize: prize,
      isPrize: false,
    });
  },
  bindRegionChange(e) {
    this.setData({
      region: e.detail.value,
    });
  },
  closePrize() {
    this.setData({
      isPrize: true,
    });
  },
  setName(e) {
    this.setData({
      userName: e.detail.value,
    });
  },
  setPhone(e) {
    this.setData({
      userPhone: e.detail.value,
    });
  },
  setAdd(e) {
    this.setData({
      address: e.detail.value,
    });
  },
  submitInfo() {
    let that = this;
    if (that.data.userName == "") {
      wx.showToast({
        title: "请填写姓名！",
        icon: "none",
      });
      return;
    }
    if (that.data.userPhone.length != 11) {
      wx.showToast({
        title: "请填写正确手机号！",
        icon: "none",
      });
      return;
    }
    if (that.data.region[2] == "") {
      wx.showToast({
        title: "请选择省市区！",
        icon: "none",
      });
      return;
    }
    if (that.data.address == "") {
      wx.showToast({
        title: "请填写详细地址！",
        icon: "none",
      });
      return;
    }
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/present/update",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id || that.data.wxId,
        presentAddrId: that.data.prize.id,
        name: that.data.userName,
        phone: that.data.userPhone,
        province:
          that.data.region[0] + that.data.region[1] + that.data.region[2],
        shipaddr: that.data.address,
      },
      success(data) {
        wx.hideLoading();
        if (data.data.status == 1) {
          that.setData({
            userName: "",
            userPhone: "",
            address: "",
            region: ["", "", ""],
            isPrize: true,
          });
          that.getRecord();
          wx.showToast({
            title: "信息已保存，静待发货",
            icon: "none",
          });
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
          });
        }
      },
    });
  },
  getPoints() {
    //领取积分和红包
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/xpppresent/getPointsRedPack",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id || that.data.wxId,
        presentAddrId: that.data.prize.id,
      },
      success(data) {
        wx.hideLoading();
        if (data.data.status == 1) {
          that.setData({
            isPrize: true,
            prize: null,
          });
          wx.showToast({
            title: "领取成功！",
            icon: "success",
          });
          that.getRecord();
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
          });
        }
      },
    });
  },
  getcard() {
    // wx.navigateToMiniProgram({
    //   appId: "wx32be38d26e316e2c",
    //   path:
    //     "pages/home/index?introduceID=990134291769&st=120&si=yibeiyima3_20210325",
    // });
    wx.navigateToMiniProgram({
      appId: heloo.globalData.wm_shop_app_id,
      path: "/cms_design/index?isqdzz=1&tracepromotionid=100077529&productInstanceId=11970818643&vid=0",
    });
  },
  // 兑换码领取
  setCodePhone(e) {
    this.setData({
      codePhone: e.detail.value,
    });
  },
  setCode(e) {
    this.setData({
      code: e.detail.value,
    });
  },
  sendCode() {
    let that = this;
    if (that.data.code_text == "获取验证码") {
      if (that.data.codePhone.length != 11) {
        wx.showToast({
          title: "请填写正确手机号！",
          icon: "none",
        });
        return;
      }
      let time = that.data.codeTime;
      that.setData({
        code_text: time + "s",
      });
      that.getCode();
      codeTimer = setInterval(() => {
        let time = that.data.codeTime - 1;
        if (time < 0) {
          that.setData({
            code_text: "获取验证码",
            codeTime: 60,
          });
          clearInterval(codeTimer);
        } else {
          that.setData({
            code_text: time + "s",
            codeTime: time,
          });
        }
      }, 1000);
    }
  },
  getCode() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/xpppresent/getVilifyCode",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id || that.data.wxId,
        phone: that.data.codePhone,
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.status == "1") {
          wx.showToast({
            title: "短信已发送",
          });
        } else {
          wx.showModal({
            title: "验证码失败",
            content: res.data.message,
          });
        }
      },
    });
  },
  codeSubmit() {
    let that = this;
    if (that.data.codePhone.length != 11) {
      wx.showToast({
        title: "请填写正确手机号！",
        icon: "none",
      });
      return;
    }
    if (that.data.code == "") {
      wx.showToast({
        title: "请填写验证码！",
        icon: "none",
      });
      return;
    }
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.request({
      url: heloo.globalData.url + "/api/xpppresent/getMonthCardCode",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        wxid: that.data.userInfo.id || that.data.wxId,
        phone: that.data.codePhone,
        presentAddrId: that.data.prize.id,
        code: that.data.code,
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.status == "1") {
          wx.showToast({
            title: "领取成功！",
            icon: "success",
          });
          that.setData({
            isPrize: true,
            code: "",
            codePhone: "",
          });
          that.getRecord();
        } else {
          wx.showModal({
            title: "领取失败",
            content: res.data.message,
          });
        }
      },
    });
  },
  //
  closeOnePrize(value) {
    console.log(value);
    this.setData({
      isPrize: value.detail.isShowOne,
    });
  },
  goToNuclearSales() {
    let that = this;
    that.setData({
      isPrize: true,
    });
    wx.showLoading({
      title: "加载中",
      mask: true,
    });
    wx.request({
      url: `${heloo.globalData.url}/manualWriteOff?wxid=${
        that.data.userInfo.id || that.data.wxId
      }&presentAddrId=${that.data.prize.id}`,
      method: "get",
      success(res) {
        if (res.data.code == 200) {
          wx.hideLoading({
            success() {
              wx.showToast({
                title: "核销成功",
              });
            },
          });
          that.getRecord();
        } else {
          wx.hideLoading({
            success() {
              wx.showToast({
                title: "核销失败",
                icon: "none",
              });
            },
          });
        }
      },
      fail() {
        wx.hideLoading({
          success() {
            wx.showToast({
              title: "核销失败",
              icon: "none",
            });
          },
        });
      },
    });
  },
});
