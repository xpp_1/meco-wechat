const http = require("../../prxppUtils/prxpp_httputil")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShowModal: false,
    prizeStr: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('prize id = ' + options.id);
    var that = this
    http.prxpp_postRequest({
      url: 'answer/prize/receive',
      params: {
        id: options.id
      },
      success: data => {
        console.log(data);
        if (data.data.prize == 1) {
          that.setData({
            isShowModal: true,
            prizeStr: "抢到" + data.data.prize_string + "积分"
          })
        } else if (data.data.prize == 2) {
          that.setData({
            isShowModal: true,
            prizeStr: "抢到" + data.data.prize_string + "元"
          })
        }

      },
      failed: errMsg => {
        console.log(errMsg);
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  clikcToSplash() {
    console.log('----clikcToSplash----');
    wx.reLaunch({
      url: '../qwdt/qwdt'
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})