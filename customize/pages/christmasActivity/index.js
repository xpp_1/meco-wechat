/**
 * @description 圣诞活动主页面
 */
import heloo from "../../utils/heloo";
import apiConstants from "../../server/apiConstants";
Page({
  data: {
    needNum: 4, // 所需助力人数
    activityByMeList: [],
    activityByOthersList: [],
    curTab: 0, // 0 我发起的 1 我收到的
    tabList: ["我发出的", "我收到的"],
    isActivated: null, // 是否会员
    barHeight: 0,
  },
  onShow() {
    console.log(heloo.globalData, "userInfo");
    const { xpp_isActivated } = heloo.globalData;
    if (xpp_isActivated) {
      this.setData({
        isActivated: xpp_isActivated,
      });
      // 会员
      console.log("当前为会员");
      this.getActivityList();
    } else {
      return;
    }
  },
  getActivityList() {
    let _this = this;
    _this.setData({
      loading: true,
    });
    wx.showLoading({
      title: "加载中...",
      icon: "none",
    });
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_GET_ACTIVITY_LIST,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          // openid: "oPwQG5mR_P6DLxqkPwy_OjC867YA", // 测试ek
          // openid: "oPwQG5uPhjJsc3NApgv5ayCSMCJo", // 测试
          openid: heloo.globalData.userInfo.openid,
          activityId: 9, // 写死
        },
        success(data) {
          _this.setData({
            loading: false,
          });
          wx.hideLoading();
          if (data.data.status > 0) {
            const dataSource = data.data.data || [];
            let activityByMeList = []; // 我发起的
            let activityByOthersList = []; // 我收到的

            dataSource.forEach((item) => {
              const num = item?.helpUserList.length;
              const activityObj = {
                ...item,
                powerStatus: num === _this.data.needNum,
                powerNum: num,
                powerNeedNum: _this.data.needNum - num,
              };
              if (item.openid === heloo.globalData.userInfo.openid) {
                // 我发起的
                activityObj.method = "byMe";
                activityByMeList.push(activityObj);
              } else {
                //我收到的
                activityByOthersList.push(activityObj);
              }
            });

            _this.setData({
              activityByMeList,
              activityByOthersList,
            });
            resolve();
          } else {
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
          }
        },
        fail(res) {
          _this.setData({
            loading: false,
          });
          wx.hideLoading();
          console.log(res, "error");
        },
      });
    });
  },
  toggleTab(e) {
    const cur = e.currentTarget.dataset.tab;
    this.setData({
      curTab: cur,
    });
  },
  linkToMake() {
    heloo.globalData.christmasVoice = "";
    heloo.globalData.christmasBg = "";
    wx.navigateTo({
      url: "/customize/pages/christmasCallMake/index",
    });
  },
  linkToDetail(e) {
    const sponsorSeq = e.currentTarget.dataset.id;
    wx.navigateTo({
      url:
        "/customize/pages/christmasCallReceive/index?&sponsorSeq=" +
        sponsorSeq +
        "&shareType=share",
    });
  },
  getNavBarHeight(e) {
    this.setData({ barHeight: e.detail.barHeight });
  },
});
