const util = require("../../prxppUtils/prxpp_util");
const http = require("../../prxppUtils/prxpp_httputil");

Page({
  /**
   * 页面的初始数据
   */
  data: {
    text_share: "",
    answer_count: -1,
    check_list: [],
    currentChooseIndex: -1,
    rule_list: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("splash-onLoad");
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log("splash-onReady");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log("splash-onShow");
    var that = this;

    if (util.unionId) {
      console.log("----unionId exist------");
      that.getSplashData();
    } else {
      console.log("----unionId not exist------");
      util
        .login()
        .then((result) => {
          console.log("----getSplashData 1------");
          that.getSplashData();
        })
        .catch((error) => {
          console.error(error);
        });
    }
  },

  getSplashData() {
    console.log("----getSplashData 2------");
    var that = this;
    http.prxpp_postRequest({
      url: "answer/get",
      success: (data) => {
        var ansList = data.data.answer;
        var myList = new Array();
        for (var i = 0; i < ansList.length; i++) {
          myList.push({
            value: i,
            id: ansList[i].id,
            name: ansList[i].subject,
            isFinish: !ansList[i].can_answer,
            isChecked: false,
          });
        }

        that.setData({
          check_list: myList,
          text_share: data.data.power_string,
          answer_count: data.data.answer_count,
          rule_list: data.data.rule,
          currentChooseIndex: -1,
        });
      },
      failed: (errMsg) => {
        console.log(errMsg);
      },
    });
  },

  check_click(e) {
    if (e.currentTarget.dataset.finish) {
      wx.showToast({
        title: "该题已完成",
        icon: "none",
        duration: 1000,
      });
      return;
    }

    var data = this.data.check_list;

    for (let i = 0; i < data.length; i++) {
      data[i].isChecked = false;
    }

    data[e.currentTarget.dataset.index].isChecked = true;
    this.setData({
      check_list: data,
    });

    this.data.currentChooseIndex = this.data.check_list[
      e.currentTarget.dataset.index
    ].id;
    console.log("current id : " + this.data.currentChooseIndex);
  },

  answer: function () {
    console.log("answer");

    if (this.data.answer_count <= 0) {
      wx.showToast({
        title: "今日答题机会已用完！",
        icon: "none",
        duration: 1000,
      });
      return;
    }

    if (this.data.currentChooseIndex == -1) {
      wx.showToast({
        title: "请先选择一个题目类型！",
        icon: "none",
        duration: 1000,
      });
      return;
    }

    util
      .checkUserInfo()
      .then((_) => {
        wx.navigateTo({
          url:
            "../prxppAnswer/prxppAnswer?ansid=" + this.data.currentChooseIndex,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  },

  share: function () {
    console.log("share");
    util
      .checkUserInfo()
      .then((_) => {
        http.prxpp_postRequest({
          url: "answer/help/join",
          success: (data) => {
            console.log(data);
            wx.navigateTo({
              url: "../prxppShare/prxppShare?id=" + data.data.id,
            });
          },
          failed: (errMsg) => {
            console.log(errMsg);
          },
        });
      })
      .catch((error) => {
        console.error(error);
      });
  },
  myaward: function () {
    console.log("myaward");
    util
      .checkUserInfo()
      .then((_) => {
        wx.navigateTo({
          url: "../prxppMyaward/prxppMyaward",
        });
      })
      .catch((error) => {
        console.error(error);
      });
  },
  clickToLuck() {
    wx.navigateTo({
      url: "/customize/pages/roundRotate/roundRotate",
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log("splash-onHide");
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log("splash-onUnload");
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
});
