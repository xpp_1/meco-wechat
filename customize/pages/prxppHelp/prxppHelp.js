const http = require("../../prxppUtils/prxpp_httputil")
const util = require("../../prxppUtils/prxpp_util")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    activityID: -1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.activityID = options.id
    util.login().then(result => {

    }).catch(error => {
      console.error(error)
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  help_click: function () {
    http.prxpp_postRequest({
      url: 'answer/help/power',
      params: {
        activity_id: this.data.activityID
      },
      success: data => {
        console.log(data);
        wx.showToast({
          title: '助力成功！',
          icon: 'none',
          duration: 1000
        })
      },
      failed: errMsg => {
        console.log(errMsg);
      }
    })
  },

  answer_click: function () {
    wx.navigateTo({
      url: '../qwdt/qwdt'
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})