// pages/share/share.js
// const app = getApp();
import heloo from '../../utils/heloo';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ewm: "",
    ewmImg:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (heloo.globalData.userInfo.qrcodeImg) {
      this.setData({
        ewm: heloo.globalData.userInfo.qrcodeImg
      })
      this.drawCanvas();
    } else {
      this.getCode();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: 'Meco真茶局',
      path: '/customize/pages/index/index',
      // imageUrl: "/images/shareicon.jpg"
    }
  },
  getCode() {
    let that = this;
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
    wx.request({
      url: heloo.globalData.url + '/api/meco/getQRCode',
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      data: {
        openid: heloo.globalData.userInfo.openid,
        scene: "scene=poster&id=" + heloo.globalData.userInfo.id,
        page: "pages/redEnvelope/home",
        isHyaline: "1"
      },
      success(data) {
        wx.hideLoading();
        if (data.data.status == 1) {
          that.setData({
            ewm: data.data.data
          })
          that.drawCanvas();
        } else {
          wx.showToast({
            title: data.data.message,
            icon: "none",
            duration: 2000
          })
        }
      }
    })

  },
  drawCanvas() {
    let that = this;
    wx.showLoading({
      title: '生成中...',
    })
    wx.getSystemInfo({
      success: (result) => {
        const rem = result.screenWidth / 375;
        const ctx = wx.createCanvasContext('mycanvas');
        ctx.drawImage("../../images/sharebg.png", 0, 0, 375 * rem, 732 * rem);
        ctx.arc(187.5 * rem, 536 * rem, 41.5 * rem, 0, 2 * Math.PI);
        ctx.setFillStyle('#F7CF5E');
        ctx.fill();
        ctx.beginPath();
        ctx.arc(187.5 * rem, 536 * rem, 38 * rem, 0, 2 * Math.PI);
        ctx.setFillStyle('#fff');
        ctx.fill();
        ctx.save();
        wx.getImageInfo({
          src: that.data.ewm,
          success(res) {
            wx.downloadFile({
              url: that.data.ewm,
              success: function (ewm) {
                ctx.beginPath();
                ctx.arc(187.5 * rem, 536 * rem, 35 * rem, 0, 2 * Math.PI);
                ctx.clip();
                ctx.drawImage(ewm.tempFilePath, 152.5 * rem, 501 * rem, 70 * rem, 70 * rem);
                ctx.restore();
                ctx.draw();
                setTimeout(() => {
                  wx.canvasToTempFilePath({
                    canvasId: 'mycanvas',
                    success: function (res) {
                      // that.saveImg(res.tempFilePath)
                      wx.hideLoading()
                      that.setData({
                        ewmImg:res.tempFilePath
                      })
                    },
                    fail: function (res) {
                      console.log(res)
                    }
                  })
                }, 100);
                
              },
              fail(res) {
                console.log(res)
                that.getCode();
              }
            })
          },
          fail(res) {
            wx.showToast({
              title: '分享码获取失败！',
            })
            that.getCode();
          }
        })

      },
    })
  },
  save() {
    var that = this;
    wx.canvasToTempFilePath({
      canvasId: 'mycanvas',
      success: function (res) {
        that.saveImg(res.tempFilePath)

      },
      fail: function (res) {
        console.log(res)
      }
    })
  },
  saveImg(shareImg) {
    // 获取用户是否开启用户授权相册
    wx.getSetting({
      success(res) {
        console.log(res)
        // 如果没有则获取授权
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {
              wx.saveImageToPhotosAlbum({
                filePath: shareImg,
                success() {
                  wx.showToast({
                    title: '海报保存成功,快去分享吧',
                    icon: 'none'
                  })
                },
                fail() {
                  wx.showToast({
                    title: '保存失败',
                    icon: 'none'
                  })
                }
              })
            },
            fail(res) {
              console.log(res);
              // 如果用户拒绝过或没有授权，则再次打开授权窗口
              //（ps：微信api又改了现在只能通过button才能打开授权设置，以前通过openSet就可打开，下面有打开授权的button弹窗代码）
              wx.showModal({
                title: '授权',
                content: '需要获取相册授权,才能保存图片',
                success(res) {
                  if (res.confirm) {
                    wx.openSetting();
                  } else if (res.cancel) {

                  }
                }
              })
            }
          })
        } else {
          // 有则直接保存
          wx.saveImageToPhotosAlbum({
            filePath: shareImg,
            success() {
              wx.showToast({
                title: '海报保存成功,快去分享吧',
                icon: 'none'
              })
            },
            fail(res) {
              console.log(res)
              wx.showToast({
                title: '保存失败',
                icon: 'none'
              })
            }
          })
        }
      },
      fail(res) {
        console.log(res)
      }
    })
  },
})