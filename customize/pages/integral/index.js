const {
  usR
} = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    index: 1,
    array: ['全部', '今年', '本月', '本周'],
    selectIndex: 0,
    total: 0,
    params: {
      pageNum: 1,
      pageSize: 20,
      param: {
        endTime: '',
        memberId: "",
        startTime: '',
        changeType: ''
      }
    },

    info: {},
    dataList: []
  },
  changeNavbar(e) {
    let i = e.target.dataset.index
    this.setData({
      index: i
    })
    let changeType = i == 1 ? '' : i == 2 ? 'increase' : 'decrease'
    this.setData({
      params: {
        ...this.data.params,
        pageNum: 1,
        param: {
          ...this.data.params.param,
          changeType,
        }
      }
    })
    this.getList()
  },
  bindPickerChange(e) {
    let i = e.detail.value
    this.setData({
      selectIndex: i
    })
    let start = '',
      end = '',
      obj = {}
    if (i == 1) {
      obj = this.getThisYearRange()
    } else if (i == 2) {
      obj = this.getThisMonthRange()
    } else if (i == 3) {
      obj = this.getThisWeekRange()
    }
    if (i != 0) {
      start = obj.start.toISOString().split('T')[0] + ' 00:00:00'
      end = obj.end.toISOString().split('T')[0] + ' 23:59:59'
    }
    this.setData({
      params: {
        ...this.data.params,
        pageNum: 1,
        param: {
          ...this.data.params.param,
          endTime: end,
          startTime: start,
        }
      }
    })
    this.getList()
  },
  scrollFn() {
    if (this.data.total <= this.data.dataList.length) return
    let i = this.data.params.pageNum
    this.setData({
      params: {
        ...this.data.params,
        pageNum: i + 1,
      }
    })
    this.getList()
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      params: {
        ...this.data.params,
        param: {
          memberId: usR.userInfo.id
        }
      },
      info: usR.getUserInfo()
    })
    this.getList()
  },
  async getList() {
    wx.showLoading({
      title: '加载中...',
    })
    if (this.data.params.pageNum == 1) {
      this.setData({
        dataList: []
      })
    }
    let res = {}
    try {
      res = await usR.scoreHistories(this.data.params)
    } catch (error) {
      console.log(error);
      if(error.total && error.list?.length){
          res = error
      }
    }
    console.log(1,res,res.total,this.data.dataList.length);
    if (res.total > this.data.dataList.length) {
      console.log(2);
      this.setData({
        dataList: [...this.data.dataList, ...res.list],
        total: res.total
      })
    }
    wx.hideLoading()
  },
  getThisYearRange() {
    const now = new Date();
    const startOfYear = new Date(now.getFullYear(), 0, 2); // 注意月份是从0开始的  
    const endOfYear = new Date(now.getFullYear(), 11, 31, 23, 59, 59, 999);
    return {
      start: startOfYear,
      end: endOfYear
    };
  },
  getThisMonthRange() {
    const now = new Date();
    const startOfMonth = new Date(now.getFullYear(), now.getMonth(), 2);
    const endOfMonth = new Date(now.getFullYear(), now.getMonth() + 1, 0, 23, 59, 59, 999);
    return {
      start: startOfMonth,
      end: endOfMonth
    };
  },
  getThisWeekRange() {
    const now = new Date();
    const day = now.getDay() || 7; // 如果周日是0，则将其设为7以便计算  
    const startOfWeek = new Date(now.getFullYear(), now.getMonth(), now.getDate() - day + 2);
    const endOfWeek = new Date(now.getFullYear(), now.getMonth(), now.getDate() - day + 7, 23, 59, 59, 999);
    return {
      start: startOfWeek,
      end: endOfWeek
    };
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})