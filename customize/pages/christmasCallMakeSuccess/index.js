/**
 * @description 祝福制作完成页面
 */
import heloo from "../../utils/heloo";
import apiConstants from "../../server/apiConstants";
import { christmasBgList } from "../../utils/constants";
Page({
  data: {
    userInfo: {}, // 用户信息
    navTitle: "Meco真茶局",
    innerAudioContext: null,
    senderMsg: "来自MECO的祝福",
    audioUrl: "",
    audioTime: null, // 语音总时长 单位:ms
    audioStatus: "start", // 播放状态 播放中:playing 开始:start 结束:end  暂停:pause 重新播放:rePlay
    curAudioTime: 0, // 当前播放时长
    recordStatus: "ready", // 播放状态
    timer: null,
    secondes: 0,
    memberNum: 3, // 需助力人数
    envelopStatus: "receivedAll", // progress 待集齐 success 待领取 received 已领取 receivedAll 已领完
    count: 0,
    bgType: "default",
    bgList: christmasBgList,
    sponsorSeq: null, // 祝福活动id
    sponsor: {}, // 发起人
    primaryUser: [], // 助力用户
    restUser: [], // 仍需助力用户
    number: 0, // 发起的祝福次数:超过两次不再分享红包
    hasRedBag: false, // 是否有分享红包
  },
  /**
   *监听页面滚动
   */
  onPageScroll(e) {
    if (e.scrollTop > 30) {
      this.setData({
        navTitle: "",
      });
    } else {
      this.setData({
        navTitle: "Meco真茶局",
      });
    }
  },
  onLoad(options) {
    const { bgType, sponsorSeq, number } = options;
    this.getActivityDetail(sponsorSeq);
    this.setData({
      sponsorSeq,
      bgType,
      envelopStatus: "", // 默认祝福接听 不展示红包信息
      userInfo: heloo.globalData.userInfo,
    });
  },
  onReady() {
    const innerAudioContext = wx.createInnerAudioContext({
      useWebAudioImplement: true,
    });
    innerAudioContext.onPlay(() => {
      console.log("开始播放");
      //开始播放计时
      this.countDown();
      this.setData({
        audioStatus: "playing",
      });
    });
    innerAudioContext.onStop(() => {
      console.log("已播放结束");
      this.setData({
        audioStatus: "stop",
      });
    });
    innerAudioContext.onError((res) => {
      console.log(res.errMsg);
      console.log(res.errCode);
    });
    this.setData({
      innerAudioContext,
    });
  },
  /**
   * 分享
   */
  onShareAppMessage: function () {
    // sponsorSeq:祝福活动详情 sponsorAvatar:发起人头像  sponsorName:昵称  shareType:分享/默认
    const { header, nickname } = this.data.userInfo;
    const sharePath = `/customize/pages/christmasCall/index?sponsorSeq=${this.data.sponsorSeq}&shareType=share&sponsorAvatar=${header}&sponsorName=${nickname}&bgType=${this.data.bgType}`;
    console.log("sharePath__________", sharePath);
    return {
      title: "宝！快接听一起抢红包吧>>",
      imageUrl:
        "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/christmas/shareBg.png",
      path: sharePath,
    };
  },
  getActivityDetail(id) {
    let _this = this;
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_GET_CHRISTMAS_CALL_DETAIL,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          sponsorSeq: id,
          openid: heloo.globalData.userInfo.openid,
        },
        success(data) {
          if (data.data.status == 1) {
            const usersArr = data.data.data.users;
            _this.setData({
              audioTime: data.data.data?.resource?.soundLen * 1000,
              audioUrl: data.data.data?.resource?.soundResourcesId,
            });
            _this.refreshPower(usersArr);
            resolve();
          } else {
            console.log("error___");
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
          }
        },
        fail(res) {
          console.log(res, "error");
          _this.setData({
            loading: false,
          });
        },
      });
    });
  },
  // 更新助力情况
  refreshPower(usersArr) {
    let sponsor = {};
    let primaryUser = [];
    let restUser = [];
    if (usersArr && usersArr.length > 0) {
      usersArr.forEach((user) => {
        if (user.sponsorType === "1") {
          sponsor = user;
        } else {
          primaryUser.push(user);
        }
        if (user.openid === this.data.userInfo.openid) {
          // 判断是否已助力用户
          this.setData({
            powerFlag: false,
          });
        }
      });

      if (primaryUser.length < this.data.memberNum) {
        restUser = new Array(this.data.memberNum - primaryUser.length); // 仍需助力人数
      }
      this.setData({
        sponsor,
        hasRedBag: true,
        // hasRedBag: sponsor.sponsorBagType === "1", // 1:带有分享红包 2:超出分享次数限制 此规则废弃
        primaryUser,
        restUser,
        senderAvatar: sponsor.header,
        senderName: sponsor.nickname,
      });
    }
  },
  goHome() {
    wx.redirectTo({
      url: "/customize/pages/index/index",
    });
  },
  // 播放
  onPlayAudio() {
    const { audioStatus } = this.data;
    switch (audioStatus) {
      case "start":
        this.data.innerAudioContext.autoplay = true;
        this.data.innerAudioContext.src = this.data.audioUrl;
        break;
      case "playing":
        this.data.innerAudioContext.pause();
        this.setData({
          audioStatus: "pause",
        });
        clearInterval(this.data.timer);
        break;
      case "pause":
        this.data.innerAudioContext.play();
        break;
      case "stop":
        this.setData({
          curAudioTime: 0,
          progressWidth: 0,
          secondes: 0,
        });
        this.data.innerAudioContext.play();
        break;
    }
  },
  countDown() {
    this.data.timer = setInterval(() => {
      this.data.secondes += 100;
      if (this.data.secondes > this.data.audioTime) {
        clearInterval(this.data.timer);
        this.data.innerAudioContext.stop();
        this.setData({
          audioStatus: "stop",
        });
        return;
      }
      const progressWidth = (this.data.secondes / this.data.audioTime) * 100;
      this.setData({
        curAudioTime: Math.round(this.data.secondes / 1000),
        progressWidth: progressWidth >= 100 ? 100 : progressWidth,
      });
    }, 100);
  },
  linkToMake() {
    heloo.globalData.christmasVoice = "";
    heloo.globalData.christmasBg = "";
    wx.navigateTo({
      url: "/customize/pages/christmasCallMake/index",
    });
  },
  linkToChristmasRule() {
    wx.navigateTo({
      url: "/customize/pages/christmasRule/index",
    });
  },
});
