// pages/rotate/rotate.js
//废弃
// const app = getApp();
const mecoAPI = require('webapi.js');
import heloo from '../../utils/heloo';
var timer = 0,
    times = 0,
    codeTimer=null,
    speed = 200;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        region: ['', '', ''],
        isStart: false,
        isLogin: true,
        prize: null,
        isRule: true,
        isRecord: true,
        isPrize: true,
        userInfo: null,
        activeNum: 0, //转动位置
        stopNum: 0, //停止位置
        chance: 0,
        free: 0,
        fromId: "",
        // 中奖信息填写
        userName: "",
        userPhone: "",
        address: "",
        // 兑换码
        code_text:"获取验证码",
        codePhone:"",
        code:"",
        codeTime:60
    },

    /**
     * 生命周期函数--监听页面加载
     */
    async onLoad(options) {
        let that = this;
        if (!heloo.globalData.userInfo) {
            await heloo.wxLogin().then((res) => { //已注册

            }, (res) => { //未注册

            })
        }
        that.setData({
            userInfo: heloo.globalData.userInfo
        })
        if (options.fromId) {
            that.setData({
                fromId: options.fromId
            })
            console.log(options.fromId)
            that.userLogin();
        } else {
            that.getList();
            // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
            heloo.writeTime(4, "/customize/pages/index/index")
        }
        var _this = this;
        mecoAPI.post({
            url: "/openseller/GetSellerCouponPlugsinsParams",
            success: function (r) {
                console.log("GetSellerCouponPlugsinsParams", r)
                if (r.Code != 1) return;
                _this.setData(r.Data)
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
        clearTimeout(timer)
    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        clearInterval(codeTimer)
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function (res) {
        console.log(res)
        if (res.from === 'button') {
            // 来自页面内转发按钮
            return {
                title: 'Meco真茶局',
                path: '/customize/pages/rotate/rotate?fromId=' + this.data.userInfo.id,
                // imageUrl:"/images/shareicon.jpg"
            }
        }
        return {
            title: 'Meco真茶局',
            path: '/customize/pages/index/index',
            // imageUrl: "/images/shareicon.jpg"
        }
    },
    bindRegionChange(e) {
        this.setData({
            region: e.detail.value
        })
    },
    closePrize() {
        this.setData({
            isPrize: true
        })
    },
    closeWait() {
        this.setData({
            isLogin: true
        })
    },
    setName(e) {
        this.setData({
            userName: e.detail.value
        })
    },
    setPhone(e) {
        this.setData({
            userPhone: e.detail.value
        })
    },
    setAdd(e) {
        this.setData({
            address: e.detail.value
        })
    },
    goRule(e) {
        let type = e.currentTarget.dataset.type;
        wx.navigateTo({
            url: '../prizeRule/prizeRule?type=' + type,
        })
    },
    getcard() {
        heloo.writeTime("4", "pages/rotate/rotate", "pages/home/index?introduceID=990134291769&st=120&si=yibeiyima3_20210325")
        wx.navigateToMiniProgram({
            appId: heloo.globalData.wm_shop_app_id,
            path: "/cms_design/index?isqdzz=1&tracepromotionid=100077529&productInstanceId=11970818643&vid=0",
          });
    },
    start() {
        let that = this;
        if (that.data.userInfo) {
            if (that.data.chance > 0 || that.data.free > 0) {
                if (!that.data.isStart) {
                    that.setData({
                        isStart: true
                    })
                    that.rotate();
                    that.getPrize();
                }
            } else {
                wx.showToast({
                    title: '次数不足',
                    icon: "none"
                })
            }
        } else {
            that.getInfo();
        }


    },
    rotate() {
        let that = this;
        let num = that.data.activeNum;
        times++;
        if (times > 50 && that.data.prize && that.data.activeNum == that.data.stopNum) {
            clearTimeout(timer) // 清除转动定时器，停止转动
            times = 0;
            timer = 0;
            speed = 200;
            that.setData({
                isStart: false,
                stopNum: "",
                isPrize: false
            })
        } else {
            if (times < 50) {
                speed -= 20 // 加快转动速度
            } else {
                speed += 20
            }
            if (speed < 40) {
                speed = 40
            }
            num++;
            if (num > 7) {
                num = 0
            }

            that.setData({
                activeNum: num
            })
            timer = setTimeout(that.rotate, speed)
        }
    },
    getList() { //奖品列表
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        let openid = "";
        if (that.data.userInfo) {
            openid = that.data.userInfo.openid
        }
        wx.request({
            url: heloo.globalData.url + '/api/xpppresent/list',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: openid
            },
            success(data) {
                console.log(data)
                wx.hideLoading();
                if (data.data.status == 1) {
                    let row = {
                        name: "start",
                        isBtn: true
                    }
                    let arr = [0, 1, 2, 7, 3, 6, 5, 4]
                    let list = data.data.data.drawPresentList;
                    list.map((item, index) => {
                        item.active = arr[index]
                    })
                    list.splice(4, 0, row);
                    that.setData({
                        prizeList: list,
                        chance: data.data.data.drawChance,
                        free: data.data.data.freeDrawChance
                    })
                } else {
                    wx.showToast({
                        title: "活动过于火爆，请稍后重试", // 转盘偶发性空白
                        icon: "none",
                      });
                    // wx.showToast({
                    //     title: data.data.message,
                    //     icon: "none"
                    // })
                }
            },
            fail(res) {
                console.log(res)
            }
        })
    },
    getPrize() { //抽奖
        let that = this;
        wx.request({
            url: heloo.globalData.url + '/api/xpppresent/draw',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                wxid: that.data.userInfo.id
            },
            success(data) {
                if (data.data.status == 1) {
                    let list = that.data.prizeList;
                    let prize = data.data.data;
                    list.map((item) => {
                        if (item.id == prize.drawPresentId) {
                            prize.number = item.number;
                            that.setData({
                                stopNum: item.active,
                                prize: prize
                            })
                        }
                    })
                    if (that.data.free > 0) {
                        let free = that.data.free;
                        free--;
                        that.setData({
                            free: free
                        })
                    } else if (that.data.chance > 0) {
                        let chance = that.data.chance;
                        let userInfo = that.data.userInfo;
                        // userInfo.points = userInfo.points - 50;
                        chance--;
                        that.setData({
                            chance: chance,
                            userInfo: userInfo
                        })
                        heloo.globalData.userInfo = userInfo;
                    }
                    
                } else {
                    wx.showToast({
                        title: data.data.message,
                        icon: "none"
                    })
                    clearTimeout(timer) // 清除转动定时器，停止转动
                    times = 0;
                    timer = 0;
                    speed = 200;
                    that.setData({
                        activeNum: 0,
                        stopNum: "",
                        isStart: false
                    })
                }
            }
        })
    },
    getPoints() { //领取积分和红包
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/xpppresent/getPointsRedPack',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                wxid: that.data.userInfo.id,
                presentAddrId: that.data.prize.id
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    that.setData({
                        isPrize: true,
                        prize: null
                    })
                    wx.showToast({
                        title: "领取成功！",
                        icon: "success"
                    })
                } else {
                    wx.showToast({
                        title: data.data.message,
                        icon: "none"
                    })
                }
            }
        })
    },
    submitInfo() {
        let that = this;
        if (that.data.userName == "") {
            wx.showToast({
                title: "请填写姓名！",
                icon: "none"
            })
            return;
        }
        if (that.data.userPhone.length != 11) {
            wx.showToast({
                title: "请填写正确手机号！",
                icon: "none"
            })
            return;
        }
        if (that.data.region[2] == "") {
            wx.showToast({
                title: "请选择省市区！",
                icon: "none"
            })
            return;
        }
        if (that.data.address == "") {
            wx.showToast({
                title: "请填写详细地址！",
                icon: "none"
            })
            return;
        }
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/present/update',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                wxid: that.data.userInfo.id,
                presentAddrId: that.data.prize.id,
                name: that.data.userName,
                phone: that.data.userPhone,
                province: that.data.region[0] + that.data.region[1] + that.data.region[2],
                shipaddr: that.data.address
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    that.setData({
                        userName: "",
                        userPhone: "",
                        address: "",
                        region: ["", "", ""],
                        isPrize: true
                    })
                    wx.showToast({
                        title: "信息已保存，静待发货",
                        icon: "none"
                    })
                } else {
                    wx.showToast({
                        title: data.data.message,
                        icon: "none"
                    })
                }
            }
        })
    },
    // 登录
    userLogin() {
        let that = this;
        wx.login({
            success(res) {
                wx.request({
                    url: heloo.globalData.url + '/api/userIsNull',
                    method: "POST",
                    header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    data: {
                        code: res.code
                    },
                    success(data) {
                        console.log(data)
                        if (data.data.status == 0) {
                            let userInfo = {};
                            userInfo.openid = data.data.openid;
                            userInfo.unionid = data.data.unionid;
                            userInfo.key = heloo.decryptDES(data.data.session_key_secret);
                            heloo.globalData.register = userInfo;
                            heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
                            that.setData({
                                isLogin: false
                            })
                            // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
                            heloo.writeTime(1, "pages/rotate/rotate");
                            heloo.writeTime(4, "pages/rotate/rotate");
                            that.getList();
                        } else {
                            that.setData({
                                userInfo: data.data.wxuser
                            })
                            heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
                            heloo.globalData.userInfo = data.data.wxuser;
                            that.getList();
                        }
                    },
                    fail(res) {
                        console.log(res)
                    }
                })
            }
        })
    },
    // 头像授权
    getInfo() {
        let that = this;
        if (wx.canIUse('getUserProfile')) {
            wx.getUserProfile({
                lang: "zh_CN",
                desc: "本次授权用于个人信息显示",
                success(res) {
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    let unionid = "";
                    let openid = "";
                    if (that.data.userInfo) {
                        unionid = that.data.userInfo.unionid;
                        openid = that.data.userInfo.openid;
                    } else {
                        unionid = heloo.globalData.register.unionid;
                        openid = heloo.globalData.register.openid;
                    }
                    wx.request({
                        url: heloo.globalData.url + '/api/getunionid',
                        method: "POST",
                        header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        },
                        data: {
                            userInfo: JSON.stringify(res.userInfo),
                            unionid: unionid,
                            openid: openid,
                            reference: that.data.fromId,
                            source: "nine"
                        },
                        success(data) {
                            wx.hideLoading();
                            if (data.data.status == 1) {
                                that.setData({
                                    userInfo: data.data.wxuser,
                                    isLogin: true
                                })
                                heloo.globalData.userInfo = data.data.wxuser;
                                that.start();
                            } else {
                                wx.showToast({
                                    title: data.data.message,
                                    icon: "none"
                                })
                            }


                        }
                    })
                }
            })
        } else {
            wx.showModal({
                content: "微信客户端版本过低，请更新后重试！",
                showCancel: false,
                confirmText: "我知道了",
            })
        }
    },
    // 手机号授权
    getPhone(e) {
        var that = this;
        wx.checkSession({
            success: function () {
                if (e.detail.iv) {
                    if (!that.data.userInfo) {
                        wx.showModal({
                            title: '授权失败',
                            content: '尚未授权注册',
                        })
                        return;
                    }
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    wx.request({
                        url: heloo.globalData.url + '/pointProduct/getPhoneNum',
                        method: "POST",
                        header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        },
                        data: {
                            openid: that.data.userInfo.openid,
                            sessionKey: heloo.globalData.userKey,
                            phonepagetype: "4",
                            encryptedData: e.detail.encryptedData,
                            iv: e.detail.iv
                        },
                        success: function (res) {
                            wx.hideLoading();
                            if (res.data.status == "1") {
                                console.log(res)
                                heloo.globalData.userInfo = res.data.userinfo;
                                that.setData({
                                    userInfo: res.data.userinfo
                                })
                                that.start();
                            } else {
                                wx.showModal({
                                    title: '手机号解密失败',
                                    content: res.data.msg,
                                })
                            }
                        }
                    })
                }
            },
            fail: function () {
                wx.showModal({
                    title: '授权失败',
                    content: '登录状态过期，请重新授权！',
                    success: function () {
                        that.userLogin();
                    }
                })
            }
        })

    },
    getcoupon(params) {
        var _this = this;
        console.log(params);
        if (this.data.getted) {
            return;
        }
        if (params.detail.errcode != "OK") {
            console.log(params.detail);
            wx.showModal({
                title: "领取失败",
                content: params.detail.msg,
                confirmText: "我知道了",
                showCancel: false
            });
            return;
        }

        if (!params.detail.send_coupon_result) {
            console.log(params.detail);
            if (_this.data.clickindex == "0") {
                wx.showModal({
                    title: "领取失败",
                    content: "没有可领取的商家券",
                    confirmText: "我知道了",
                    showCancel: false
                })
            }
            return;
        }

        var reqList = [];
        for (var i = 0; i < params.detail.send_coupon_result.length; i++) {
            var send_coupon = params.detail.send_coupon_result[i];
            if (send_coupon.code == "SUCCESS") {
                var req = {};
                req.coupon_code = send_coupon.coupon_code;
                req.out_request_no = send_coupon.out_request_no;
                req.stock_id = send_coupon.stock_id;
                reqList.push(req);

            }
        }

        if (reqList.length == 0) return;

        mecoAPI.post({
            showErrorMessage: false,
            url: "/openseller/SaveReceiveSellerCouponRecord",
            data: {
                openID: "小程序openid",
                list: reqList
            },
            success: function (r) {

                if (r.Code == 1 && r.Data) {
                    _this.getcard()
                    setTimeout(() => {
                        // wx.showToast({
                        //     title: '已加入卡包！',
                        //     icon: 'success',
                        //     duration: 2000
                        // });
                        _this.setData({
                            getted: true
                        });
                    }, 1000)
                } else {
                    if (_this.data.clickindex == "0") {
                        wx.showModal({
                            title: "温馨提示",
                            content: r.Message,
                            confirmText: "我知道了",
                            showCancel: false
                        })
                    }
                }
            }
        });
    },
    setCodePhone(e){
        this.setData({
            codePhone:e.detail.value
        })
    },
    setCode(e){
        this.setData({
            code:e.detail.value
        })
    },
    sendCode(){
        let that = this;
        if(that.data.code_text == "获取验证码"){
            if(that.data.codePhone.length != 11){
                wx.showToast({
                    title: "请填写正确手机号！",
                    icon: "none"
                })
                return;
            }
            let time = that.data.codeTime;
            that.setData({
                code_text:time+"s"
            })
            that.getCode();
            codeTimer=setInterval(()=>{
                let time = that.data.codeTime - 1;
                if(time<0){
                    that.setData({
                        code_text:"获取验证码",
                        codeTime:60
                    })
                    clearInterval(codeTimer)
                    
                }else{
                    that.setData({
                        code_text:time+"s",
                        codeTime:time
                    })
                }
                
            },1000)
        }
    },
    getCode(){
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/xpppresent/getVilifyCode',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                wxid: that.data.userInfo.id,
                phone:that.data.codePhone
            },
            success: function (res) {
                wx.hideLoading();
                if (res.data.status == "1") {
                   wx.showToast({
                     title: '短信已发送',
                   })
                } else {
                    wx.showModal({
                        title: '验证码失败',
                        content: res.data.message,
                    })
                }
            }
        })
    },
    codeSubmit(){
        let that = this;
        if(that.data.codePhone.length != 11){
            wx.showToast({
                title: "请填写正确手机号！",
                icon: "none"
            })
            return;
        }
        if(that.data.code==""){
            wx.showToast({
                title: "请填写验证码！",
                icon: "none"
            })
            return;
        }
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/xpppresent/getMonthCardCode',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                wxid: that.data.userInfo.id,
                phone:that.data.codePhone,
                presentAddrId:that.data.prize.id,
                code:that.data.code
            },
            success: function (res) {
                wx.hideLoading();
                if (res.data.status == "1") {
                   wx.showToast({
                     title: '领取成功！',
                     icon:"success"
                   })
                   that.setData({
                       isPrize:true,
                       code:"",
                       codePhone:""
                   })
                } else {
                    wx.showModal({
                        title: '领取失败',
                        content: res.data.message,
                    })
                }
            }
        })
    }
})