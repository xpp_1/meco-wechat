// pages/code/code.js
// const app = getApp();
import heloo from '../../utils/heloo';
Page({
    data: {
        systemInfo: null,
        safeTop: 20,
        menuButtonBounding: 30, 
        userInfo: null,
        region: ['', '', ''],
        isRule: true,
        isPrize: true,
        isLogin: true,
        record: [],
        prev: [],
        myCode: [],
        now: [],
        friendList: [],
        fromId: "",
        historyPrize: null,
        // 中奖信息填写
        userName: "",
        userPhone: "",
        address: "",
        cardimg1: "",
        cardimg2: "",
        open:{
            wait:true,
            day:"00",
            hour:['00','00','00'] //"00:00:00"
        }
    },

    onLoad: function (options) {
        
        wx.getSystemInfo({
          success: result => {
            console.log("设备信息", result);
            this.setData({
              systemInfo: result,
              safeTop:result.safeArea.top,
              menuButtonBounding: wx.getMenuButtonBoundingClientRect().height  // 胶囊高度
              // statusBarHeight: result.statusBarHeight,
              // navHeight: result.statusBarHeight + 44, //导航高度
            })
          },
        });

        let that = this;
        if (options.fromId||!heloo.globalData.userInfo) {
            that.setData({
                fromId: options.fromId
            })
            that.userLogin();
        } else {
            that.setData({
                userInfo: heloo.globalData.userInfo
            })
            that.getDetail();
        }
    },

    onShow: function () {

    },

    onShareAppMessage: function (res) {
        if (res.from === 'button') {
            // 来自页面内转发按钮
            return {
                title: 'Meco真茶局',
                path: '/customize/pages/code/code?fromId=' + this.data.userInfo.id,
                // imageUrl:"/images/shareicon.jpg"
            }
        }
        return {
            title: 'Meco真茶局',
            path: '/customize/pages/index/index',
            // imageUrl: "/images/shareicon.jpg"
        }
    },

    backTap(){ wx.navigateBack() },

    bindRegionChange(e) {
        this.setData({
            region: e.detail.value
        })
    },
    setName(e) {
        this.setData({
            userName: e.detail.value
        })
    },
    setPhone(e) {
        this.setData({
            userPhone: e.detail.value
        })
    },
    setAdd(e) {
        this.setData({
            address: e.detail.value
        })
    },
    showRule() {
        this.setData({
            isRule: false
        })
    },
    hideRule() {
        this.setData({
            isRule: true
        })
    },
    // 获取抽奖码
    getCode() {
        let that = this;
        if (that.data.userInfo) {
            if(!that.data.userInfo.phoneNumber){
                wx.showToast({
                  title: '请继续点击授权手机号，方便中奖后发货',
                  duration:2000,
                  icon:"none"
                })
                return;
            }
            wx.requestSubscribeMessage({
              tmplIds: ['5wv5A-Lw0hD0WjwgICRjkMvcFp5yXhXHTf75IdmdRQY'],
              complete:()=>{
                wx.showLoading({
                    title: '加载中...',
                    mask: true
                })
                wx.request({
                    url: heloo.globalData.url + '/api/meco/getCode',
                    method: "POST",
                    header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    data: {
                        openid: heloo.globalData.userInfo.openid
                    },
                    success(data) {
                        wx.hideLoading();
                        if (data.data.status == 1) {
                            let myCode = [];
                            let row = {};
                            row.code = data.data.code;
                            myCode.push(row);
                            that.setData({
                                myCode: myCode
                            })
                        } else {
                            wx.showToast({
                                title: data.data.message,
                                icon: "none"
                            })
                        }
                    }
                })
              }
            })
            

        } else {
            that.getInfo();
        }

    },
    // 历史信息
    getDetail() {
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        let openid = "";
        if (that.data.userInfo) {
            openid = that.data.userInfo.openid
        } else {
            openid = heloo.globalData.register.openid
        }
        wx.request({
            url: heloo.globalData.url + '/api/meco/getBatchList',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: openid
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    let record = data.data.MecoBatchList;
                    let prev = data.data.lastBatched;
                    let myCode = data.data.myMecoCode;
                    let now = data.data.thisBatch;
                    record.map((item) => {
                        item.num = that.setPhone2(item.mobile)
                    })
                    prev.num = that.setPhone2(prev.mobile)
                    now.startTime = now.startTime.substring(0,10);
                    now.endTime = now.endTime.substring(0,10);
                    let prizeTip = data.data.newAlert;
                    if(prizeTip.phone){
                        prizeTip.phone = that.setPhone2(prizeTip.phone);
                    }
                    if(now.endTimeStamp){
                        that.getEndTime(now.endTimeStamp)//结束倒计时
                    }
                    that.setData({
                        record: record,
                        prev: prev,
                        myCode: myCode,
                        now: now,
                        historyPrize: data.data.presentMecoCode,
                        friendList: data.data.friendList,
                        prizeTip:prizeTip
                    })
                    if (data.data.presentMecoCode && data.data.presentMecoCode.presentName) {
                        that.setData({
                            isPrize: false
                        })
                    }
                } else {
                    wx.showToast({
                        title: data.data.message,
                    })
                }
            }
        })

    },
    setPhone2(phone) {
        if (phone) {
            return phone.substring(0, 3) + "****" + phone.substring(7, 11)
        } else {
            return "****"
        }

    },
    closeTip(){
        this.setData({
            prizeTip:null
        })
    },
    // 登录
    userLogin() {
        let that = this;
        wx.login({
            success(res) {
                wx.request({
                    url: heloo.globalData.url + '/api/userIsNull',
                    method: "POST",
                    header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                    },
                    data: {
                        code: res.code
                    },
                    success(data) {
                        console.log(data)
                        
                        if (data.data.status == 0) {
                            let userInfo = {};
                            userInfo.openid = data.data.openid;
                            userInfo.unionid = data.data.unionid;
                            userInfo.key = heloo.decryptDES(data.data.session_key_secret);
                            heloo.globalData.register = userInfo;
                            heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
                            that.setData({
                                isLogin: false
                            })
                            // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
                            heloo.writeTime(1, "pages/code/code");
                            that.getDetail();
                        } else {
                            that.setData({
                                userInfo: data.data.wxuser
                            })
                            heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
                            heloo.globalData.userInfo = data.data.wxuser;
                            that.getDetail();
                        }
                    }
                })
            }
        })
    },
    // 头像授权
    getInfo() {
        let that = this;
        if (wx.canIUse('getUserProfile')) {
            wx.getUserProfile({
                lang: "zh_CN",
                desc: "本次授权用于个人信息显示",
                success(res) {
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    let unionid = "";
                    let openid = "";
                    if(that.data.userInfo){
                        unionid = that.data.userInfo.unionid;
                        openid = that.data.userInfo.openid;
                    }else{
                        unionid= heloo.globalData.register.unionid;
                        openid= heloo.globalData.register.openid;
                    }
                    wx.request({
                        url: heloo.globalData.url + '/api/getunionid',
                        method: "POST",
                        header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        },
                        data: {
                            userInfo: JSON.stringify(res.userInfo),
                            unionid: unionid,
                            batchId:that.data.now.id,
                            openid: openid,
                            reference: that.data.fromId,
                            source: "code"
                        },
                        success(data) {
                            wx.hideLoading();
                            if (data.data.status == 1) {
                                that.setData({
                                    userInfo: data.data.wxuser,
                                    isLogin: true
                                })
                                heloo.globalData.userInfo = data.data.wxuser;
                                that.getCode();
                            } else {
                                wx.showToast({
                                    title: data.data.message,
                                    icon: "none"
                                })
                            }


                        }
                    })
                }
            })
        } else {
            wx.showModal({
                content: "微信客户端版本过低，请更新后重试！",
                showCancel: false,
                confirmText: "我知道了",
            })
        }
    },
    // 手机号授权
    getPhone(e) {
        var that = this;
        wx.checkSession({
            success: function () {
                if (e.detail.iv) {
                    if (!that.data.userInfo) {
                        wx.showModal({
                            title: '授权失败',
                            content: '尚未授权注册',
                        })
                        return;
                    }
                    wx.showLoading({
                        title: '加载中...',
                        mask: true
                    })
                    wx.request({
                        url: heloo.globalData.url + '/pointProduct/getPhoneNum',
                        method: "POST",
                        header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                        },
                        data: {
                            openid: that.data.userInfo.openid,
                            sessionKey: heloo.globalData.userKey,
                            phonepagetype: "7",
                            encryptedData: e.detail.encryptedData,
                            iv: e.detail.iv
                        },
                        success: function (res) {
                            wx.hideLoading();
                            if (res.data.status == "1") {
                                console.log(res)
                                heloo.globalData.userInfo = res.data.userinfo;
                                that.setData({
                                    userInfo: res.data.userinfo,
                                    isLogin:true
                                })
                                that.getCode();
                            } else {
                                wx.showModal({
                                    title: '手机号解密失败',
                                    content: res.data.msg,
                                })
                            }
                        }
                    })
                }
            },
            fail: function () {
                wx.showModal({
                    title: '授权失败',
                    content: '登录状态过期，请重新授权！',
                    success: function () {
                        that.userLogin();
                    }
                })
            }
        })

    },
    // 领取好友分享所得抽奖码
    getFriend(e) {
        let that = this;
        let id = e.currentTarget.dataset.id;
        let batch = e.currentTarget.dataset.batch;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/getunionid',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: heloo.globalData.register.openid,
                batchId: batch,
                mecoFriendUserId: id
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    let list = that.data.friendList;
                    list.map((item) => {
                        if (item.id == id) {
                            item.status = 1;
                        }
                    })
                    that.setData({
                        friendList: list
                    })
                } else {
                    wx.showToast({
                        title: data.data.message,
                        icon: "none"
                    })
                }
            }
        })
    },
    submitInfo() {
        let that = this;
        if (that.data.userName == "") {
            wx.showToast({
                title: "请填写姓名！",
                icon: "none"
            })
            return;
        }
        if (that.data.userPhone.length != 11) {
            wx.showToast({
                title: "请填写正确手机号！",
                icon: "none"
            })
            return;
        }
        if (that.data.region[2] == "") {
            wx.showToast({
                title: "请选择省市区！",
                icon: "none"
            })
            return;
        }
        if (that.data.address == "") {
            wx.showToast({
                title: "请填写详细地址！",
                icon: "none"
            })
            return;
        }
        if (that.data.cardimg1 == "") {
            wx.showToast({
                title: "请上传身份证人像面！",
                icon: "none"
            })
            return;
        }
        if (that.data.cardimg2 == "") {
            wx.showToast({
                title: "请上传身份证国徽面！",
                icon: "none"
            })
            return;
        }
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/meco/getBatchPresent',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: that.data.userInfo.openid,
                reciverName: that.data.userName,
                phone: that.data.userPhone,
                province: that.data.region[0] + that.data.region[1] + that.data.region[2],
                address: that.data.address,
                positiveCard: that.data.cardimg1,
                negativeCard: that.data.cardimg2
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    that.setData({
                        userName: "",
                        userPhone: "",
                        address: "",
                        cardimg1: "",
                        cardimg2: "",
                        region: ["", "", ""],
                        isPrize: true
                    })
                    wx.showToast({
                        title: "信息已保存，静待发货",
                        icon: "none"
                    })
                } else {
                    wx.showToast({
                        title: data.data.message,
                        icon: "none"
                    })
                }
            }
        })
    },
    // 上传身份证
    uploadImg(e) {
        var that = this;
        let type = e.currentTarget.dataset.type;
        wx.chooseImage({
            count: 1,
            sizeType: ['original', 'compressed'],
            sourceType: ['camera'],
            success(res) {
                console.log(res)
                const path = res.tempFilePaths;
                wx.showLoading({
                    title: '上传中...',
                    mask: true
                })
                wx.uploadFile({
                    url: heloo.globalData.url + '/imageUpWXTX', //仅为示例，非真实的接口地址
                    filePath: path[0],
                    name: 'upfile',
                    formData: {
                      "fileName": path[0].split("wxfile://")[1]
                        // "fileName": path[0].split("http://tmp/")[1]
                    },
                    success(res) {
                      wx.hideLoading();
                      let data = JSON.parse(res.data);
                      if (data.state == "SUCCESS") {
                        if(type == 1){
                            that.setData({
                                cardimg1:data.url
                            })
                        }else{
                            that.setData({
                                cardimg2:data.url
                            })
                        }
                      }
                    }
                  })
            },
            fail(res) {
                console.log(res)
            }
        })
    },
    getEndTime(endtime){
        let time = new Date().getTime();
        let that = this;
        let leave = parseInt((endtime - time)/1000);
        let open = {
            wait:true,
            day:"00",
            hour:"00:00:00",
        }
        if(leave > 0){
            let day = parseInt(leave/(24*60*60));
            let hour = parseInt((leave%(24*60*60))/3600);
            let minute = parseInt((leave%3600)/60);
            let second = leave%60;
            hour = hour<10?'0'+hour:hour;
            minute = minute<10?'0'+minute:minute;
            second = second<10?'0'+second:second;
            open.day = day; 
            // open.hour = hour+":"+minute+":"+second;
            open.hour = [hour, minute, second];
            that.setData({
                open:open
            })
            setTimeout(()=>{
                that.getEndTime(endtime)
            },1000)
        }else{
            open.wait = false;
            that.setData({
                open:open
            })
        }
        
        
    }
})