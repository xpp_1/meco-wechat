/**
 * @description 圣诞语音祝福
 */
import heloo from "../../utils/heloo";
import apiConstants from "../../server/apiConstants";
Page({
  data: {
    userInfo: {},
    navTitle: "制作祝福",
    recordRuleTime: 15 * 1000, // 录音限制时长 单位ms
    recorderManager: wx.getRecorderManager(),
    innerAudioContext: null, // 录音播放器
    voiceTime: 0, // 单位 s
    recorder: {
      status: "ready", // 'ready' | 'recording'
      text: "点击录制",
    },
    timer: null,
    status: true,
    secondes: 0, // 单位ms
    startTimestamp: 0,
    voiceFilePath: "",
    recordStatus: "ready", // 录制及播放状态 ready未开始 recording录制中 stop停止录制 playing播放中  paused停止播放
    selectedBgType: "", // 背景图type
    progressWidth: 0, // 进度条宽度
    sponsorSeq: null, // 祝福活动id
    loading: false,
  },
  onLoad() {
    this.setData({ userInfo: heloo.globalData.userInfo });
  },
  onShow: function () {
    const { christmasBg, christmasVoice } = heloo.globalData;
    if (christmasBg && christmasBg !== this.data.selectedBgType) {
      this.setData({
        selectedBgType: christmasBg,
      });
    }
    if (christmasVoice) {
      console.log(christmasVoice, "christmasVoice");
      this.setData({
        recordStatus: "stop",
        voiceFilePath: christmasVoice,
      });
    }
  },
  onReady() {
    this.data.recorderManager.onStart(() => {
      console.log("开始录制...");
    });

    //错误回调
    this.data.recorderManager.onError((res) => {
      console.log("录音失败", res);
    });
  },
  // onUnload() {
  //   this.destroy();
  // },
  // onHide() {
  //   this.destroy();
  // },
  /**
   *监听页面滚动
   */
  onPageScroll(e) {
    if (e.scrollTop > 30) {
      this.setData({
        navTitle: "",
      });
    } else {
      this.setData({
        navTitle: "制作祝福",
      });
    }
  },
  destroy() {
    // this.data.innerAudioContext.destroy()
    // this.setData({
    //   innerAudioContext: null,
    //   curAudioTime: 0,
    //   progressWidth: 0,
    //   secondes: 0,
    //   audioStatus: "start",
    //   timer: null,
    //   recorderManager:null,
    // })
  },
  // 点击开始录制
  onRecordStart() {
    const { recordStatus } = this.data;
    switch (recordStatus) {
      case "ready":
        this.getRecordLimit();
        break;
      case "recording":
        // 录制中
        this.onRecordEnd();
        break;
      case "stop":
        this.onPlayStart();
        break;
      case "playing":
        this.onPlayEnd();
        break;
      case "paused":
        this.data.innerAudioContext.play();
    }
  },
  // 结束录制
  onRecordEnd() {
    this._endRecord();
  },
  // 点击播放
  onPlayStart() {
    const innerAudioContext = wx.createInnerAudioContext();
    innerAudioContext.autoplay = true;
    innerAudioContext.src = this.data.voiceFilePath;
    this.setData({
      innerAudioContext,
    });
    innerAudioContext.onPlay(() => {
      console.log("开始播放");
      this.setData({
        recordStatus: "playing",
      });
    });
    innerAudioContext.onPause(() => {
      console.log("播放暂停");
    });
    innerAudioContext.onEnded(() => {
      console.log("播放结束");
      this.setData({
        recordStatus: "stop",
      });
    });
    innerAudioContext.onError((res) => {
      console.log(res.errMsg);
      console.log(res.errCode);
    });
  },
  // 停止播放
  onPlayEnd() {
    this.data.innerAudioContext.pause();
    this.setData({
      recordStatus: "paused",
    });
  },
  // 删除录音 重新录制
  deleteVoice() {
    let _this = this;
    wx.showModal({
      title: "",
      content: "确认删除该条录音？",
      success(res) {
        if (res.confirm) {
          heloo.globalData.christmasVoice = ''
          _this.setData({
            recordStatus: "ready",
            voiceFilePath: "",
            voiceTime: 0,
            secondes: 0,
          });
        }
      },
    });
  },
  _endRecord() {
    clearInterval(this.data.timer);
    this.data.recorderManager.stop();
    this.data.recorderManager.onStop((res) => {
      console.log("停止录音", res);
      let filePath = res.tempFilePath;
      if ((Date.now() - this.data.startTimestamp) / 1000 < 2) {
        wx.showToast({
          title: "录音时间太短,请重新录制",
          icon: "none",
          duration: 1500,
        });
        this.setData({
          startTimestamp: 0,
          secondes: 0,
          voiceTime: 0,
          progressWidth: 0,
          recordStatus: "ready",
        });
        return;
      }
      this.setData({
        recordStatus: "stop",
        voiceFilePath: filePath,
      });
      heloo.globalData.christmasVoice = filePath;
    });
  },
  // 录音授权
  getRecordLimit() {
    let that = this;
    wx.getSetting({
      success(res) {
        if (!res.authSetting["scope.record"]) {
          // 未授权
          wx.authorize({
            scope: "scope.record",
            success() {
              // 一次才成功授权
              that._startRecord();
            },
            fail(err) {
              console.log(err);
              wx.showModal({
                title: "温馨提示",
                content: "您未授权录音，该功能将无法使用",
                showCancel: true,
                confirmText: "授权",
                success: function (res) {
                  if (res.confirm) {
                    wx.openSetting({
                      success: (res) => {
                        if (!res.authSetting["scope.record"]) {
                          //未设置录音授权
                          wx.showModal({
                            title: "提示",
                            content: "您未授权录音，功能将无法使用",
                            showCancel: false,
                            success: function () {},
                          });
                        } else {
                          // 二次才成功授权
                          that._startRecord();
                        }
                      },
                      fail: function () {
                        console.log("授权设置录音失败");
                      },
                    });
                  }
                },
                fail: function (err) {
                  console.log("打开录音弹框失败 => ", err);
                },
              });
            },
          });
        } else {
          console.log("已授权");
          // 已授权
          that._startRecord();
        }
      },
    });
  },
  // 开始录音
  _startRecord() {
    this.data.startTimestamp = Date.now();
    const options = {
      duration: 15 * 1000, //指定录音的时长，单位 ms，最大为10分钟（600000），默认为1分钟（60000）
      sampleRate: 16000, //采样率
      numberOfChannels: 1, //录音通道数
      encodeBitRate: 96000, //编码码率
      format: "mp3", //音频格式，有效值 aac/mp3
      frameSize: 50, //指定帧大小，单位 KB
    };

    //点击录制
    this.data.recorderManager.start(options);

    //开始录音计时
    this.countDown();
    this.setData({
      recordStatus: "recording",
    });
  },
  countDown() {
    this.data.timer = setInterval(() => {
      this.data.secondes += 100;
      if (this.data.secondes > this.data.recordRuleTime) {
        clearInterval(this.data.timer);
        this.data.recorderManager.stop();
        this.data.recorderManager.onStop((res) => {
          wx.showToast({
            title: "录音时间已到",
            icon: "none",
            duration: 1500,
          });
          let filePath = res.tempFilePath;
          this.setData({
            recordStatus: "stop",
            voiceFilePath: filePath,
          });
        });
        return;
      }
      const progressWidth =
        (this.data.secondes / this.data.recordRuleTime) * 100;
      this.setData({
        voiceTime: Math.round(this.data.secondes / 1000),
        progressWidth,
      });
    }, 100);
  },
  uploadVoice() {
    return new Promise((resolve, reject) => {
      wx.uploadFile({
        url: apiConstants.HOST_NAME_UPLOAD_FILE,
        filePath: this.data.voiceFilePath,
        name: "file",
        formData: {
          openId: heloo.globalData.userInfo.openid,
        },
        success(res) {
          const data = JSON.parse(res.data);
          const voiceUrl =
            "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/" +
            data.data.resourcesId;
          resolve(voiceUrl);
        },
        fail(res) {
          reject(res.msg);
        },
      });
    });
  },
  onSelectBg() {
    wx.navigateTo({
      url: "/customize/pages/christmasCallBgSelect/index",
    });
  },

  onCallSubmit(resourcesId) {
    let _this = this;
    const {
      openid,
      country,
      province,
      city,
      nickname,
      sex,
      // phoneNumber,
      unionid,
      header,
      qunMemberId,
      // xpp_phone,
      // xpp_birthday,
    } = heloo.globalData.userInfo;
    const { xpp_phone, xpp_birthday } = heloo.globalData;
    return new Promise((resolve, reject) => {
      wx.request({
        url: apiConstants.HOST_NAME_SUBMIT_CHRISTMAS_CALL,
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          openid,
          country,
          province,
          city,
          nickname,
          sex,
          phone: xpp_phone,
          birthday: xpp_birthday,
          unionid,
          header,
          phoneNumber: xpp_phone,
          backgroundPicture: this.data.selectedBgType,
          qunMemberId,
          activityId: 9,
          resourcesId: resourcesId,
          soundLen: _this.data.voiceTime,
        },
        success(data) {
          if (data.data.status == 1) {
            const { number, sponsorSeq } = data.data.data;
            _this.setData({
              number,
              sponsorSeq,
            });
            resolve();
          } else {
            console.log("error___");
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            reject();
            _this.setData({
              loading: false,
            });
          }
        },
        fail(res) {
          console.log(res, "error");
          _this.setData({
            loading: false,
          });
        },
      });
    });
  },
  // 录制完成
  async onSubmit() {
    let _this = this;
    _this.setData({
      loading: true,
    });
    const resourcesId = await this.uploadVoice();
    await this.onCallSubmit(resourcesId);
    const { number, sponsorSeq } = this.data;

    _this.setData({
      loading: false,
    });
    wx.navigateTo({
      url: `/customize/pages/christmasCallMakeSuccess/index?bgType=${_this.data.selectedBgType}&sponsorSeq=${sponsorSeq}&number=${number}`,
    });
  },
});
