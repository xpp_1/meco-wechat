// 获取应用实例
import heloo from "../../utils/heloo";
import {
  Setting
} from "../../../engine/index";
import mecoAPI from "../../../pages/redEnvelope/webapi";
import {
  wxloinSaveData
} from "../../utils/mecoUserInfo.js"
// const {
//   mai
// } = getApp();
// const { mai } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}

Page({
  /**
   * 页面的初始数据
   */
  data: {
    Telephone: "", //用户手机号
    topSwiper: [],
    bottomSwiper: [], //me动时刻
    fansList: [],
    notice: "",
    userInfo: null,
    swiper: 0,
    percent: 0,
    fromId: "",
    source: "",
    level: "1",
    isLoad: false,
    // 以下为 tabbar 所需 data fields
    tabbarSetting: null,
    tabbarIndex: -1,
    pageId: "",
    flag: true,
    levelName: "", // 会员等级
    testCode: "",
    isShowMask: false,
    bannerSwiper: [{
        picurl: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/tea/home_banner3.jpg",
      },
      {
        picurl: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/tea/home_banner1.jpg",
      },
      // {
      //   picurl:
      //     "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/tea/home_banner2.jpg",
      // },
    ],
    barHeight: 0, // 状态栏高度
    isCompose: "",
    version: "",
  },
  async onLoad(options) {
    //判断是否打开弹窗
    let hideMask = wx.getStorageSync('hideMask')
    this.setData({
      isShowMask: !hideMask
    })
    const accountInfo = wx.getAccountInfoSync();
    if (["trial", "develop"].includes(accountInfo.miniProgram.envVersion)) {
      this.setData({
        version: "trial",
      });
    }
    let that = this;
    that.setData({
      isLoad: true,
    });
    // await this.initTabbarSetting(options);
    // 海报进入
    if (options.scene) {
      let message = options.scene.split("&");
      this.setData({
        fromId: message[0].split("scene=")[1],
        source: message[1].split("id=")[1],
      });
    }
    // 杯码打开
    if (options.code_ticket) {
      wx.redirectTo({
        url: "../redEnvelope/home?code_ticket=" +
          options.code_ticket +
          "&wwAppId=" +
          options.wwAppId,
      });
    }
    heloo.wxLogin().then(
      (res) => {
        console.log(res);
        wx.request({
          url: heloo.globalData.url + "/api/synchroUserinfo",
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: {
            unionid: res.data.wxuser.unionid,
          },
          success(result) {
            console.log(result);
          },
        });
        //已注册
        let notice = res.data.notice;
        if (res.data.notice) {
          notice = notice.substring(0, 50);
        }
        that.setData({
          topSwiper: res.data.topRotation,
          bottomSwiper: res.data.bottomRotation,
          fansList: res.data.mefengfulishe,
          notice: notice,
          userInfo: res.data.wxuser,
          isLoad: false,
        });
        // 渠道统计
        if (options.channel) {
          // console.log(options.channel)
          heloo.signChannel(options.channel);
        }
        that.setPercent(res.data.wxuser.sumPoints);
        that.setData({
          levelName: res.data.wxuser?.levelName,
        });
        // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
        heloo.writeTime(1);
      },
      (res) => {
        console.log(res);
        //未注册
        that.setData({
          topSwiper: res.data.topRotation,
          bottomSwiper: res.data.bottomRotation,
          fansList: res.data.mefengfulishe,
          notice: res.data.notice,
          isLoad: false,
        });
        that.setPercent(0);
        that.setData({
          levelName: "",
        });
      }
    );
    // this.userLogin();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    let that = this;
    console.log(9999999);
    wx.hideHomeButton();
    if (!that.data.isLoad) {
      heloo.wxLogin().then(
        (res) => {
          console.log(res);

          //已注册
          let notice = res.data.notice;
          if (res.data.notice) {
            notice = notice.substring(0, 50);
          }

          that.setData({
            topSwiper: res.data.topRotation,
            bottomSwiper: res.data.bottomRotation,
            fansList: res.data.mefengfulishe,
            notice: notice,
            userInfo: res.data.wxuser,
            isLoad: false,
          });
          that.setPercent(res.data.wxuser.sumPoints);
          that.setData({
            levelName: res.data.wxuser?.levelName,
          });
          that.getUserInfo();
        },
        (res) => {
          //未注册

          that.setData({
            topSwiper: res.data.topRotation,
            bottomSwiper: res.data.bottomRotation,
            fansList: res.data.mefengfulishe,
            notice: res.data.notice,
            isLoad: false,
          });
          that.setPercent(0);
          that.setData({
            levelName: "",
          });
        }
      );
    } else {
      setTimeout(() => {
        that.getUserInfo();
      }, 1000);
    }
    console.log("665556");
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: "Meco真茶局",
      path: "/customize/pages/index/index",
    };
  },

  // 签到
  async goTab1() {
    let that = this;
    const member = await mai.member.getDetail(["Card"]);
    console.log(member.isActivated, "member");
    if (!member.isActivated) {
      wx.showToast({
        title: '活动更新中~~',
        icon:'none'
      })
      wx.navigateTo({
        url: "/pages/register-member/index?completeType=back",
      });
      console.log("跳转");
    } else {
      if (!that.data.userInfo) {
        wx.showModal({
          content: "尚未授权注册，是否立即前往？",
          cancelText: "否",
          confirmText: "是",
          success(res) {
            if (res.confirm) {
              that.getInfo();
            }
          },
        });
        return;
      }
      wx.navigateTo({
        url: "../signIn/signIn?signNow=true",
      });
    }
  },
  // getMyInfo 更新个人信息 头像姓名
  getMyInfo() {
    wx.navigateTo({
      url: "/customize/pages/setUserInfo/setUserInfo?unionid=" +
        this.data.userInfo.unionid,
    });
  },
  getUserInfo() {
    const that = this;
    wx.request({
      url: heloo.globalData.url + "/api/getUserInfoByUnionid",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        unionid: this.data.userInfo.unionid,
      },
      success(data) {
        console.log(data);
        that.setData({
          ["userInfo.header"]: data.data.header,
          ["userInfo.nickname"]: data.data.nickname,
        });
      },
    });
  },
  openCupCode() {
    wx.navigateTo({
      url: '/customize/pages/addQWCode/index',
    })
  },
  scanCode(){
    wx.scanCode({
      success (res) {
        console.log(res)
      }
    })
  },
  // 幸运大抽奖
  toCode() {
    return wx.showToast({
      icon: "none",
      title: "活动已暂停～",
    });
    let that = this;
    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    }
    wx.navigateTo({
      url: "../code/code",
    });
  },

  toYuanxiao() {
    let that = this;
    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    }
    wx.navigateTo({
      url: "../exchangeWeb/index",
    });
  },
  toshare() {
    let that = this;
    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    }
    wx.navigateTo({
      url: "../share/share",
    });
  },
  // 宠粉抽大奖九宫格
  toRotate() {
    let that = this;
    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    }
    wx.navigateTo({
      url: "../rotate/rotate",
    });
  },
  // 宠粉大抽奖圆形
  async toRoundRotate() {
    let that = this;
    const member = await mai.member.getDetail(["Card"]);
    console.log(member.isActivated, "member");
    if (!member.isActivated) {
      wx.showToast({
        title: '活动更新中~~',
        icon:'none'
      })
      wx.navigateTo({
        url: "/pages/register-member/index?completeType=back",
      });
      console.log("跳转");
    } else {
      if (!that.data.userInfo) {
        wx.showModal({
          content: "尚未授权注册，是否立即前往？",
          cancelText: "否",
          confirmText: "是",
          success(res) {
            if (res.confirm) {
              that.getInfo();
            }
          },
        });
        return;
      }
      wx.navigateTo({
        url: "../roundRotate/roundRotate",
      });
    }
  },
  swiperChange(e) {
    this.setData({
      swiper: e.detail.current,
    });
  },
  setPercent(num) {
    var percent = parseInt((num / 1645) * 100);
    var level = 1;
    if (num < 445) {
      level = 1;
    } else if (num < 995) {
      level = 2;
    } else if (num < 1645) {
      level = 3;
    } else {
      level = 4;
      percent = 100;
    }
    this.setData({
      percent: percent,
      level: level,
    });
  },
  // 登录
  userLogin() {
    let that = this;
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
    wx.login({
      success(res) {
        wx.request({
          url: heloo.globalData.url + "/api/userIsNull",
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: {
            code: res.code,
          },
          success(data) {
            console.log(data);
            wx.hideLoading();
            if (data.data.status == 0) {
              let userInfo = {};
              userInfo.openid = data.data.openid;
              userInfo.unionid = data.data.unionid;
              userInfo.key = heloo.decryptDES(data.data.session_key_secret);
              console.log(userInfo.key);
              heloo.globalData.register = userInfo;
              heloo.globalData.userKey = heloo.decryptDES(
                data.data.session_key_secret
              );
            } else {
              that.setData({
                userInfo: data.data.wxuser,
              });
              heloo.globalData.userInfo = data.data.wxuser;
              heloo.globalData.userKey = heloo.decryptDES(
                data.data.session_key_secret
              );
              console.log(heloo.globalData.userKey);
              that.setPercent(data.data.wxuser.sumPoints);
              that.setData({
                levelName: data.data.wxuser?.levelName,
              });
            }
            that.setData({
              topSwiper: data.data.topRotation,
              bottomSwiper: data.data.bottomRotation,
              fansList: data.data.mefengfulishe,
              notice: data.data.notice,
            });
          },
        });
      },
    });
  },
  // 手机号授权
  // getPhone(e) {
  //   this.toEnterLink("parkEnter1", e.detail.encryptedData, e.detail.iv);
  // },
  // 果园
  async toOrchard() {
    return wx.showToast({
      title: '当前活动已结束，暂不可进入！',
      icon: 'none'
    })
    let that = this;
    const member = await mai.member.getDetail(["Card"]);
    console.log(member.isActivated, "member");
    if (!member.isActivated) {
      wx.navigateTo({
        url: "/pages/register-member/index?completeType=back",
      });
    } else {
      if (!that.data.userInfo) {
        wx.showModal({
          content: "尚未授权注册，是否立即前往？",
          cancelText: "否",
          confirmText: "是",
          success(res) {
            if (res.confirm) {
              that.getInfo();
            }
          },
        });
        return;
      } else {
        wx.showLoading({
          title: "加载中...",
          mask: true,
        });
        wx.login({
          success(res) {
            wx.request({
              url: heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
              method: "POST",
              data: {
                code: res.code,
              },
              success(result) {
                // console.log(result.data.data.wxuser);
                wx.hideLoading();
                // that.setData({
                //   Telephone: result.data.data.wxuser.phoneNumber,
                // });
                if (result.data.status == -1) {
                  wx.showToast({
                    title: "授权失败",
                    icon: "none",
                  });
                  wx.hideLoading();
                } else if (
                  result.data.data.message == "该用户openid还未注册！"
                ) {
                  wx.request({
                    url: heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.openid,
                      unionid: result.data.data.unionid,
                      phone: result.data.data.phone,
                    },
                    success(res) {
                      console.log(res);
                      wx.setStorageSync("uid", result.data.data.unionid);
                      wx.navigateTo({
                        url: `../orchard/orchard?uid=${wx.getStorageSync(
                          "uid"
                        )}`,
                      });
                      wx.hideLoading();
                    },
                    fail() {
                      wx.hideLoading();
                    },
                  });
                } else if (result.data.message == "请先成为果园用户选取种子") {
                  wx.request({
                    url: heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.openid,
                      unionid: result.data.data.unionid,
                    },
                    success(res) {
                      console.log(res);
                      wx.setStorageSync("uid", result.data.data.unionid);
                      wx.navigateTo({
                        url: `../orchard/orchard?uid=${wx.getStorageSync(
                          "uid"
                        )}`,
                      });

                      wx.hideLoading();
                    },
                    fail() {
                      wx.hideLoading();
                    },
                  });
                } else {
                  wx.request({
                    url: heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
                    method: "POST",
                    data: {
                      userInfo: JSON.stringify(heloo.globalData.userInfo),
                      openid: result.data.data.wxuser.openid,
                      unionid: result.data.data.wxuser.unionid,
                    },
                    success(res) {
                      console.log(res);
                      wx.setStorageSync("uid", result.data.data.wxuser.unionid);
                      wx.navigateTo({
                        url: `../orchard/orchard?uid=${wx.getStorageSync(
                          "uid"
                        )}`,
                      });
                      wx.hideLoading();
                    },
                    fail() {
                      wx.hideLoading();
                    },
                  });
                }
              },
              fail() {
                wx.hideLoading();
              },
            });
          },
        });
      }
    }
  },

  toBannerLink(e) {
    console.log(e);
    this.toFutureLink();
  },
  toEnterLink(e, encryptedData, iv) {
    if (e == "parkEnter1") {
      return this.toOrchard();
    }
    const {
      linktype
    } = e.target.dataset;
    switch (linktype) {
      // 会员乐园
      case "parkEnter1":
        this.toOrchard();
        break;
      case "parkEnter2":
        this.linkToXPP();
        // this.activityEnd();
        break;
      case "parkEnter3":
        // this.toRotate();
        this.toRoundRotate();
        break;
      case "parkEnter4":
        // this.toCode();
        this.toYuanxiao();
        break;
        // 新品尝鲜
      case "newEnter":
        this.toFutureLink();
        break;
        // 官方旗舰店
      case "miniShopBannerEnter":
        this.toMiniShop();
        break;
    }
  },
  toFutureLink() {
    wx.showToast({
      title: "敬请期待～",
      icon: "none",
    });
  },
  // 活动结束
  activityEnd() {
    wx.showToast({
      title: "活动已结束～",
      icon: "none",
    });
  },
  // 获取合成卡片id
  getIsCompose(id) {
    let that = this;
    console.log(that.data.userInfo);
    wx.request({
      url: "https://xpph7.happydoit.com:8090/front/card/composeInfo",
      data: {
        package_id: "1",
      },
      method: "POST",
      header: {
        Authorization: id, //Bearer后面必须接空格再加token
      },
      success: function success(res) {
        wx.hideLoading();
        console.log("isCompose", res);
        // 首页、红包页判断
        if (res.data.status == 1 && res.data.data.isCompose == 1) {
          let currentTime = new Date().getTime();
          console.log(currentTime);
          let url =
            "https://xpph7.happydoit.com/card/prize.html?unionid=" +
            heloo.globalData.userInfo.unionid +
            "&currentTime=" +
            currentTime;
          console.log(url);
          if (url) {
            url = encodeURIComponent(url);
            wx.navigateTo({
              url: "../webView/webView?url=" + url,
            });
          } else {
            wx.showToast({
              title: "敬请期待！",
              icon: "none",
            });
          }
        } else {
          that.goCardCollect();
        }
      },
      fail: function fail(err) {
        wx.hideLoading();
        console.log("fail:" + JSON.stringify(err));
      },
    });
  },
  // 去集卡页面
  toCardCollect() {
    let that = this;
    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    } else {
      that.getIsCompose(that.data.userInfo.unionid);
    }
  },
  goCardCollect() {
    let that = this;
    let currentTime = new Date().getTime();
    console.log(currentTime);
    let url =
      "https://xpph7.happydoit.com/card/index.html?unionid=" +
      heloo.globalData.userInfo.unionid +
      "&currentTime=" +
      currentTime;
    console.log(url);
    if (url) {
      url = encodeURIComponent(url);
      wx.navigateTo({
        url: "../webView/webView?url=" + url,
      });
    } else {
      wx.showToast({
        title: "敬请期待！",
        icon: "none",
      });
    }
  },
  // 头像授权
  getInfo() {
    let that = this;
    if (wx.canIUse("getUserProfile")) {
      wx.getUserProfile({
        lang: "zh_CN",
        desc: "本次授权用于个人信息显示",
        success(res) {
          wx.showLoading({
            title: "加载中...",
            mask: true,
          });
          let unionid = "";
          let openid = "";
          if (that.data.userInfo) {
            unionid = that.data.userInfo.unionid;
            openid = that.data.userInfo.openid;
          } else {
            unionid = heloo.globalData.register.unionid;
            openid = heloo.globalData.register.openid;
          }
          wx.request({
            url: heloo.globalData.url + "/api/getunionid",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              userInfo: JSON.stringify(res.userInfo),
              unionid: unionid,
              openid: openid,
              reference: that.data.fromId,
              source: that.data.source,
            },
            success(data) {
              wx.hideLoading();
              that.setData({
                userInfo: data.data.wxuser,
              });
              heloo.globalData.userInfo = data.data.wxuser;
              that.setPercent(data.data.wxuser.sumPoints);
              that.setData({
                levelName: data.data.wxuser?.levelName,
              });
            },
          });
        },
      });
    } else {
      wx.showModal({
        content: "微信客户端版本过低，请更新后重试！",
        showCancel: false,
        confirmText: "我知道了",
      });
    }
  },
  // 注册校验
  verifyRegister(callback) {
    let that = this;
    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    }
    callback();
  },
  // 微商城
  toShop() {
    console.log(heloo.globalData.wm_shop_app_id, 'wxe6b60f485e6069c1')
    //打开半屏微商城
    wx.openEmbeddedMiniProgram({
      appId: 'wx52b0f10de9c8e0bb',
      path: "/pages/index/index?cdp_chan1=24-zcj&cdp_chan2=ybdh-sy",
      // envVersion:'trial'
    })
    // wx.navigateToMiniProgram({
    //   appId: heloo.globalData.wm_shop_app_id,
    //   path: "/cms_design/index?isqdzz=1&tracepromotionid=100077527&productInstanceId=11970818643&vid=0",
    // });
    // wx.navigateToMiniProgram({
    //   appId: "wx32be38d26e316e2c",
    //   path:
    //     "pages/home/index?introduceID=990130481229&st=120&si=yibeiyima_caidananniu2",
    //   fail(res) {
    //     console.log(res.errMsg);
    //   },
    // });
  },
  // 问卷调查
  toQuestion() {
    let content;
    this.data.topSwiper.forEach((item) => {
      if (item.title == "问卷调查") {
        content = item;
      }
    });
    if (content) {
      if (content.appid == "wxd570425c5356eaa9") {
        let path = content.linkurl;
        if (!path) {
          path = content.linkPage;
        }
        wx.navigateTo({
          url: "/" + path,
        });
      } else {
        return wx.navigateTo({
          url: "../web/web?url=" + content.linkurl,
        });
      }
    } else {
      return wx.showToast({
        icon: "none",
        title: "问卷活动未开启～",
      });
    }
  },
  // Meco旗舰店
  toMiniShop() {
    // wx.navigateToMiniProgram({
    //   appId: "wx32be38d26e316e2c",
    //   path:
    //     "pages/custompage/custompage?pageItemUrl=newspage/73ce2f50-bb70-4ab8-9ac5-8fc980540589.html&workType=1&workCode=890000001521&introduceID=990130481229&st=120&si=yibeiyima_mefenfulishe_20210430",
    //   fail(res) {
    //     console.log(res.errMsg);
    //   },
    // });
    wx.navigateToMiniProgram({
      appId: 'wx52b0f10de9c8e0bb',
      path: "/one-price/one-price/one-price?cdp_chan1=24-zcj&cdp_chan2=d-banner-72-2&id=366008321470272",
    });  
  },
  toPoints() {
    wx.navigateTo({
      url: "../points/points",
    });
  },
  async toUser() {
    let that = this;
    const member = await mai.member.getDetail(["Card"]);
    console.log(member.isActivated, "member");
    if (!member.isActivated) {
      wx.showToast({
        title: '活动更新中~~',
        icon:'none'
      })
      wx.navigateTo({
        url: "/pages/register-member/index?completeType=back",
      });
      console.log("跳转");
    } else {
      if (!that.data.userInfo) {
        wx.showModal({
          content: "尚未授权注册，是否立即前往？",
          cancelText: "否",
          confirmText: "是",
          success(res) {
            if (res.confirm) {
              that.getInfo();
            }
          },
        });
        return;
      }
      wx.navigateTo({
        url: "../game/game",
      });
    }
  },
  showCode() {
    wx.navigateTo({
      url: "../package/package",
    });
  },
  //券包
  toCard() {
    wx.showLoading()
    wxloinSaveData().then(res => {
      wx.navigateTo({
        url: "../mecoCardShop/index",
        success(){
          wx.hideLoading()
        }
      });
    })

  },
  jump(e) {
    let that = this;
    let type = e.currentTarget.dataset.type;
    let index = e.currentTarget.dataset.index;
    let list = [];
    /* 调试用 
    wx.navigateTo({
      url: '/pages/redEnvelope/home?code_ticket=253d91b94ad5291a122446fc05a90413&wwAppId=wxb8a223205e2083f0',
    })
    return
    */

    if (!that.data.userInfo) {
      wx.showModal({
        content: "尚未授权注册，是否立即前往？",
        cancelText: "否",
        confirmText: "是",
        success(res) {
          if (res.confirm) {
            that.getInfo();
          }
        },
      });
      return;
    }
    // TODO
    // console.log(index, "index");

    // if (index === 0) {
    //   // 微商城
    //   return that.toMiniShop();
    // } else if (index === 1) {
    //   // 积分商城
    //   return wx.navigateTo({
    //     url: "/member/pages/goods-list/index",
    //   });

    //   // return that.toFutureLink();
    // }
    if (type == 1) {
      list = that.data.topSwiper;
    } else if (type == 2) {
      list = that.data.fansList;
    } else if (type == 3) {
      list = that.data.bottomSwiper;
    }

    if (list[index].appid) {
      if (list[index].appid == "wxb8a223205e2083f0") {
        let path = list[index].linkurl;
        if (!path) {
          path = list[index].linkPage;
        }
        wx.navigateTo({
          url: "/customize" + path,
        });
      } else if (list[index].appid == "wxd570425c5356eaa9") {
        let path = list[index].linkurl;
        if (!path) {
          path = list[index].linkPage;
        }
        wx.navigateTo({
          url: "/" + path,
        });
      } else {
        let path = list[index].linkurl;
        if (!path) {
          path = list[index].linkPage;
        }
        wx.navigateToMiniProgram({
          appId: list[index].appid,
          path: path,
          fail(res) {
            console.log(res.errMsg);
          },
        });
      }
    } else if (list[index].linkurl) {
      wx.navigateTo({
        url: "../web/web?url=" + list[index].linkurl,
      });
    } else if (list[index].linkPage) {
      wx.navigateTo({
        url: "../web/web?url=" + list[index].linkPage,
      });
    } else if (list[index].remarks === "种果树") {
      that.toOrchard();
    } else {
      // 去集卡
      that.toCardCollect();
    }
  },

  async initTabbarSetting(options) {
    const { usR } = getApp();
const mai = {
  member:usR
}
    // const { mai } = getApp();
    if (!mai.member.getOpenId()) {
      await mai.member.signin();
    }
    // const tabbarSetting = await Setting.getTabbarSetting();
    // const pages = getCurrentPages();
    // const pagePath = `/${pages[pages.length - 1].route}`;
    // const tabbarIndex = Setting.getTabbarIndex(
    //   tabbarSetting,
    //   "home",
    //   "",
    //   pagePath
    // );
    // this.setData({
    //   tabbarSetting,
    //   tabbarIndex,
    //   pageId: options.pageId || "",
    // });
  },

  getNavBarHeight(e) {
    this.setData({
      barHeight: e.detail.barHeight
    });
  },
  linkToTest() {
    wx.navigateTo({
      url: "../signInOld/signIn",
    });
  },
  linkToOfflineActivity() {
    wx.navigateTo({
      url: "../offlineActivity/index",
    });
  },
  linkToXPPTest() {
    wx.scanCode({
      success(res) {
        console.log(res.path, "path");
        wx.navigateTo({
          url: "/" + res.path + "&enterType=scan",
        });
      },
    });
  },
  linkToXPP() {
    wx.navigateTo({
      // url: "/customize/pages/offlineActivity/oneShare/index?scene=ME2023032443045&enterType=primary" //小程序直接进入测试
      url: "/customize/pages/offlineActivity/oneShare/index?scene=ME2023032415107&enterType=primary", //小程序直接进入正式
    });
  },
  linkToXPPTest2() {
    wx.navigateTo({
      url: "/customize/pages/offlineActivity/oneShare/index?scene=ME2023032443045&enterType=primary", //小程序直接进入测试
    });
  },
  // linkToXPPTest3() {
  //   wx.navigateTo({
  //     url: "/customize/pages/offlineActivity/flashActivity/index?scene=ME2023061327941&enterType=scan" //小程序直接进入正式
  //   });
  // },
  setTestCode: function (e) {
    this.setData({
      testCode: e.detail.value,
    });
  },
  linkToXPPTest3: function () {
    if (!this.data.testCode)
      return wx.showToast({
        title: "请输入活动编码",
        icon: "none"
      });
    wx.navigateTo({
      url: "/customize/pages/offlineActivity/flashActivity/index?scene=" +
        this.data.testCode +
        "&enterType=scan",
    });
  },
  linkToCoupon: async function () {
    // wx.navigateToMiniProgram({
    //   appId: 'wx1a8460be609a207c',
    //   path: '/cms_design/index?isqdzz=1&tracepromotionid=100077525&productInstanceId=11970818643&vid=0',
    //   fail(res) {
    //     console.log(res.errMsg)
    //   }
    // })

    // return 
    const {
      mai
    } = getApp();
    if (!mai.member.getOpenId()) {
      await mai.member.signin();
    }
    // const openId = mai.member.getOpenId();
    const unionId = mai.member.getUnionId();
    // console.log(openId, "openId");
    let wid = wx.getStorageSync("wid");
    if (!wid) {
      // 导入客户
      let res = await mecoAPI.wmRequest({
        url: "/apigw/weimob_crm/v2.0/customer/import",
        method: "POST",
        data: {
          importType: 1,
          userList: [{
            // appId: "wxb8a223205e2083f0",
            appId: "wx1a8460be609a207c",
            // openId,
            unionId,
            appChannel: 1,
            // userName: "test",
          }, ],
        },
      });
      console.log(res, "res343434");
      const resWid = res?.data?.successList[0]?.wid;
      if (resWid) {
        wid = resWid;
        wx.setStorageSync("wid", wid);
      }
      if (res?.data?.errorList[0]?.errorMessage == "该客户已存在") {
        wid = res?.data?.errorList[0]?.wid;
        if (wid) {
          wx.setStorageSync("wid", wid);
        }
      }
    }
    console.log(wid, "wid");
    //获取uuid
    const requestId = mecoAPI.getUUID();
    console.log(requestId, "requestId");
    const couponIds = [
      66249101,
      66233087,
      66241177,
      66254054,
      66254055,
      66245999,
      66230968,
      66254057,
      66240089,
      66246000,
    ];
    let couponNums = couponIds.map((item) => {
      return {
        couponTemplateId: item,
        num: 1,
        requestId,
      };
    });

    console.log(couponNums, "couponNums");

    //分发wm优惠券
    let couponRes = await mecoAPI.wmRequest({
      url: "/apigw/weimob_crm/v2.0/coupon/receive",
      method: "POST",
      data: {
        couponNums,
        // couponNums: [
        //测试券
        // {
        //   couponTemplateId: "66212848",//优惠券 ID
        //   requestId,
        //   num: 1,
        // },
        // {
        //   couponTemplateId: "66209697",
        //   requestId,
        //   num: 1,
        // },
        // {
        //   couponTemplateId: "66206735",
        //   requestId,
        //   num: 1,
        // },
        // ],
        wid,
        scene: 945000, //领券场景，945000-api发券；
        vidType: 2, //组织架构节点类型。类型包括：1-集团；2-品牌；3-区域；4部门；5-商场；6-楼层；10-门店；11-网点；100-自提点
        vid: 6016299774643, //组织架构节点 ID
      },
    });

    console.log(couponRes, "couponRes");
    const couponList = couponRes?.data?.couponResultList;
    let flag = false;
    if (couponList && couponList.length > 0) {
      couponList.forEach((item) => {
        if (item.isSuccess) {
          flag = true;
        }
      });
    }
    if (flag) {
      wx.showToast({
        icon: "none",
        title: "券已发"
      });
    }
  },
  openHalfMini() {
    wx.openEmbeddedMiniProgram({
      appId: 'wxe6b60f485e6069c1',
      envVersion: 'trial'
    })
  },
  //关闭遮罩
  closeMask() {
    this.setData({
      isShowMask: false
    })
    wx.setStorageSync('hideMask', true)
  }
});