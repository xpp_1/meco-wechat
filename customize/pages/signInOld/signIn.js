// pages/signIn/signIn.js
// const app = getApp();
import heloo from '../../utils/heloo';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        isSign: false,
        common:"",
        seven:"",
        banner: [],
        addPoints:"",
        specialPoints:"",
        holiday:"",
        holidaytext:"",
        userInfo:null,
        continuity: [{
                day: "1天",
                percent: "6%",
                point: "5"
            },
            {
                day: "2天",
                percent: "20%",
                point: "5"
            },
            {
                day: "3天",
                percent: "36%",
                point: "5"
            },
            {
                day: "4天",
                percent: "52%",
                point: "5"
            },
            {
                day: "5天",
                percent: "66%",
                point: "5"
            },
            {
                day: "6天",
                percent: "82%",
                point: "5"
            },
            {
                day: "7天",
                percent: "100%",
                point: "20"
            }
        ], //连续签到数据设置
        days: [{
            num: "",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "1",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "2",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "3",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "4",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "5",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "6",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "7",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "8",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "9",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "10",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "11",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "12",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "13",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "14",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "15",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "16",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "17",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "18",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "19",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "20",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "21",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "22",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "23",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "24",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "25",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "26",
            sign: true,
            text: "",
            date: ""
        }, {
            num: "27",
            sign: true,
            text: "",
            date: ""
        }, {
            num: "28",
            sign: false,
            text: "",
            date: ""
        }, {
            num: "29",
            sign: false,
            text: "见面会",
            date: ""
        }, {
            num: "30",
            sign: false,
            text: "见面会",
            date: ""
        }, {
            num: "31",
            sign: false,
            text: "",
            date: ""
        }], //当月日历
        today:"",
        year:"",
        month:""
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            userInfo:heloo.globalData.userInfo
        })
        this.initMonth();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return {
            title: 'Meco真茶局',
            path: '/customize/pages/index/index',
            // imageUrl: "/images/shareicon.jpg"
          }
    },
    close(){
        this.setData({
            isSign:false
        })
    },
    initMonth() { //当月数据
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() + 1;
        let day = date.getDate();
        this.getDetail(year + "-" + month);
        if (month < 10) {
            month = "0" + month
        }
        
        this.setData({
            today: year + "-" + month + "-" + day
        })
        this.setDays(year, parseInt(month));
    },
    preMonth() {
        let that = this;
        let year = that.data.year;
        let month = parseInt(that.data.month) - 1;
        if (month < 1) {
            month = 12;
            year = year - 1;
        }
        that.setDays(year, month);
        that.getDetail(year + "-" + month);
    },
    nextMonth() {
        let that = this;
        let year = that.data.year;
        let month = parseInt(that.data.month) + 1;
        if (month > 12) {
            month = 1;
            year = year + 1;
        }
        that.setDays(year, month);
        that.getDetail(year + "-" + month);
    },
    setDays(year, month) { //通用设置当月天数
        let that = this;
        let space = new Date(year + "/" + month + "/1").getDay(); //第一天是周几
        let lastDay = new Date(year, month, 0).getDate(); //本月天数
        let days = [];
        if (month < 10) {
            month = "0" + month
        }
        // 占位
        if (space == 0) {
            space = 7;
        }
        for (let i = 1; i < space; i++) {
            let row = {};
            row.num = "",
                row.sign = false,
                row.text = "",
                row.date = "";
            days.push(row)
        }
        // 实际日期
        for (let i = 1; i < lastDay + 1; i++) {
            let row = {};
            row.num = i,
            row.sign = false,
            row.text = "",
            row.date = new Date(year + "/" + month + "/" + i).getDay();
            if(i == that.data.today.substring(8,10)&&year == that.data.today.substring(0,4)&&month == that.data.today.substring(5,7)){
                row.today = true;
            }
            days.push(row)
        }
        that.setData({
            year: year,
            month: month,
            days: days
        })
    },
    getDetail(month) {
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/mecoSign/list',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: that.data.userInfo.openid,
                month: month
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    let days = that.data.days;
                    let space = 0;
                    for(let i = 0;i < days.length;i++){
                        if(!days[i].num){
                            space++;
                        }
                    }
                    console.log(space)
                    days.map((item, index) => {
                        if(index < space){

                        }else{
                            if (item.num == data.data.mecoSignin[index-space].day) {
                                item.sign = data.data.mecoSignin[index-space].sign;
                                item.text = data.data.mecoSignin[index-space].text;
                            }
                        }
                        
                    })
                    that.setData({
                        days: days,
                        banner: data.data.banner,
                        common:data.data.signPoints,
                        seven:data.data.sevenDayPoints
                    })
                }
            }
        })

    },
    signIn() {
        let that = this;
        wx.showLoading({
            title: '加载中...',
            mask: true
        })
        wx.request({
            url: heloo.globalData.url + '/api/mecoSign/sign',
            method: "POST",
            header: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: {
                openid: that.data.userInfo.openid
            },
            success(data) {
                wx.hideLoading();
                if (data.data.status == 1) {
                    let userInfo = that.data.userInfo;
                    userInfo.signdays = userInfo.signdays+1>7?"7":userInfo.signdays+1;
                    if(that.data.year == that.data.today.substring(0,4)&&that.data.month == that.data.today.substring(5,7)){
                        let days = that.data.days;
                        days.map((item)=>{
                            if(item.num == that.data.today.substring(8,10)){
                                item.sign = 1;
                            }
                        })
                        that.setData({
                            days:days
                        })
                    }
                    that.setData({
                        isSign:true,
                        addPoints:data.data.addPoints,
                        holiday:data.data.text,
                        specialPoints:data.data.specialPoints,
                        holidaytext:"",
                        userInfo:userInfo
                    })
                    // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
                    heloo.writeTime(3,"pages/signIn/signIn")
                }else{
                   wx.showToast({
                     title: data.data.message,
                     icon:"none",
                     duration:2000
                   })
                }
            }
        })

    },
    daySign(e){
        let that = this;
        let index = e.currentTarget.dataset.index;
        let day = that.data.days[index].num;
        if(that.data.year == that.data.today.substring(0,4)&&that.data.month == that.data.today.substring(5,7)&&day==that.data.today.substring(8,10)){
            that.signIn();
        }
    },
    jump(e) {
        let that = this;
        let index = e.currentTarget.dataset.index;
        let list = that.data.banner;
        if (list[index].appid) {
            if(list[index].appid  == "wxb8a223205e2083f0"){
                let path = list[index].linkurl;
                if(!path){
                path = list[index].linkPage;
                }
                wx.navigateTo({
                  url: "/customize"+path,
                })
              }else if (list[index].appid == "wxd570425c5356eaa9") {
                let path = list[index].linkurl;
                if (!path) {
                    path = list[index].linkPage;
                }
                wx.navigateTo({
                    url: "/"+path,
                })
            }else{
                let path = list[index].linkurl;
                if(!path){
                  path = list[index].linkPage;
                }
                wx.navigateToMiniProgram({
                  appId: list[index].appid,
                  path: path,
                  fail(res) {
                    console.log(res.errMsg)
                  }
                })
              }
        } else if (list[index].linkurl) {
            wx.navigateTo({
                url: '../web/web?url=' + list[index].linkurl
            })
        }else if (list[index].linkPage) {
            wx.navigateTo({
                url: '../web/web?url=' + list[index].linkPage
            })
        }
    },
    // 手机号授权
    getPhone(e){
        var that = this;
        wx.checkSession({
          success: function() {
            if (e.detail.iv) {
              if (!that.data.userInfo) {
                wx.showModal({
                  title: '授权失败',
                  content: '尚未授权注册',
                })
                return;
              }
              wx.showLoading({
                title: '加载中...',
                mask: true
              })
              wx.request({
                url: heloo.globalData.url + '/pointProduct/getPhoneNum',
                method: "POST",
                header: {
                  "Content-Type": "application/x-www-form-urlencoded"
                },
                data: {
                  openid: that.data.userInfo.openid,
                  sessionKey: heloo.globalData.userKey,
                  phonepagetype:"3",
                  encryptedData: e.detail.encryptedData,
                  iv: e.detail.iv
                },
                success: function(res) {
                  wx.hideLoading();
                  if (res.data.status == "1") {
                    console.log(res)
                    heloo.globalData.userInfo = res.data.userinfo;
                    that.setData({
                        userInfo:res.data.userinfo
                    })
                    that.signIn();
                  } else {
                    wx.showModal({
                      title: '手机号解密失败',
                      content: res.data.msg,
                    })
                  }
                }
              })
            }
          },
          fail: function() {
            wx.showModal({
              title: '授权失败',
              content: '登录状态过期，请重新授权！',
              success: function() {
                that.userLogin();
              }
            })
          }
        })
      
    },
})