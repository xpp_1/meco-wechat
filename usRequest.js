const version = wx.getAccountInfoSync().miniProgram.envVersion
const env = {
  // develop: 'http://10.3.56.188:8090',
  develop: 'https://scrm.chinaxpp.com',
  trial: 'https://scrm.chinaxpp.com',
  release: 'https://scrm.chinaxpp.com'
}
let isRequest = false
let requestList = []
export default class mecoRequest {
  constructor() {
    this.unionId = ''
    this.oppenId = '';
    this.accessToken = ''
    this.userInfo = {}
    this.baseUrl = env[version]
    this.signin()
  }
  request(options) {
    requestList.push(options)
    const {
      url,
      data = {},
      params = {},
      method = "get",
      header = {}
    } = options;
    const XppUserInfo = this.getAcessToken()
    if (XppUserInfo) {
      header['XPP-Access-Token'] = XppUserInfo
    }
    if (isRequest) return
    return new Promise((resolve, reject) => {
      wx.request({
        url: this.baseUrl + url,
        method,
        params,
        data,
        header: {
          "content-type": "application/json",
          ...header
        },
        success: async (config) => {
          if (config.data && config.data.code == '200') {
            requestList.pop()
            resolve(config.data)
          } else if (config.data.code == '401') {
            console.log('requestList', requestList);
            //重新授权
            const result = await this.signin()
            if (result.code == 401) {return reject(config)}
            const res = await this.request(options)
            requestList.pop()
            resolve(res.data)
          } else {
            reject(config)
          }
        },
        fail: reject,
      });
    });
  };
  //获取历史积分信息
  async scoreHistories(data) {
    return new Promise((resolve, reject) => {
      this.request({
        url: '/member/memberScoreHistoryAPI/members/scoreHistories',
        method: 'post',
        data
      }).then((res) => {
        if (res.code == 200) {
          resolve(res.data)
        } else {
          reject(res)
        }
      }).catch(reject)
    })

  }
  //获取客户信息
  async getDetail() {
    return new Promise((resolve, reject) => {
      this.request({
        url: '/member/memberInfoAPI/v2/member'
      }).then((res) => {
        if (res.code == 200) {
          resolve(res.data)
        } else {
          reject(res)
        }
      }).catch(reject)
    })
  }
  // 鉴权
  signin() {
    return new Promise((resolve, reject) => {
      wx.login({
        success: (res) => {
          // return console.log(res.code);
          this.request({
            url: '/member/memberInfoAPI/v2/weapp/oauth',
            method: 'post',
            data: {
              appId: 'wxb8a223205e2083f0',
              code: res.code
            }
          }).then((result) => {
            console.log(result,'鉴权信息！');
            this.oppenId = result.data.openId
            this.accessToken = result.data.accessToken
            this.unionId = result.data.unionId
            this.userInfo = Object.assign(result.data.originFromVO, result.data.memberVO)
            resolve(true)
            wx.setStorageSync('XppUserInfo', result.data)
          }).catch((err) => {
            console.log(err,'err鉴权的错误信息！');
            reject(false)
          });
        }
      })
    })

  }
  //事件上报
  memberEventLogs(data) {
    return new Promise((resolve, reject) => {
      this.request({
        url: '/member/memberEventLogAPI/v2/memberEventLogs',
        method: 'post',
        data
      }).then((res) => {
        if (res.code == 200) {
          resolve(res.data)
        } else {
          reject(res)
        }
      }).catch(reject)
    })
  }
  XppUserInfo(){
    return wx.getStorageSync('XppUserInfo') || {}
  }
  getOpenId() {
      let id = this.XppUserInfo().oppenId
    return this.oppenId || id || ''
  }
  getAcessToken() {
    let accessToken = this.XppUserInfo().accessToken
    return this.accessToken || accessToken || ''
  }
  getUnionId() {
    let unionId = this.XppUserInfo().unionId
    return this.unionId || unionId || ''
  }
  getUserInfo() {
    const info = this.XppUserInfo().originFromVO 
    let result = {}
    if(info){
      Object.assign(result,info.originFromVO, info.memberVO)
    }
    return this.userInfo || result
  }
  getMemberId() {
    const info = this.XppUserInfo().originFromVO 
    let result = {}
    if(info){
      Object.assign(result,info.originFromVO, info.memberVO)
    }
    return this.userInfo.id || result.id
  }
  isSocialAuthorized() {
    return false
  }
  isActivated() {
    return false
  }
}