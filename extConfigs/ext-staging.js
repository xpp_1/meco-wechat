export default {
  ext: {
    hasSkeleton: true,
    isGroup: false,
    'mai-button': {
      shouldOauth: true,
      shouldReportFormId: false,
    },
    maiAccountId: '5cb43c7c33b38b3b895c02c4',
    maiCampaign: 'meco-member-center',
    weappName: 'meco-member-center',
    h5ChannelId: '5e785ff5c5e761c2133eaa8a',
    maiDomainCDN: 'https://staging-statics.maiscrm.com',
    maiDomainH5: 'https://staging-portal.maiscrm.com',
    maiDomainH5V2: 'https://staging-h5.maiscrm.com',
    maiENV: 'staging',
    maiEventDefaultSource: 'weapp-direct',
    overridePages: [],
    weappId: 'wxf40bf62f2e5e43d5',
    weappInitPage: '/customize/pages/example/index',
    splashPage: 'pages/splash/index',
    weappStorageVersion: 1,
  },
};
