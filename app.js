import Mai from "./engine/maijs/latest.min";
import { ExtConfig } from "./engine/index";
import usRequest from './usRequest'
const mai = new Mai(ExtConfig.getMaiEnv(), ExtConfig.getMaiAccountId());
const usR = new usRequest()
App({
  mai,
  usR,
  onLaunch(options) {
    // 判断是否支持自动更新
    if (wx.canIUse("getUpdateManager")) {
      const updateManager = wx.getUpdateManager();
      updateManager.onCheckForUpdate(function (res) {
        // 检测是否有新版本
        if (res.hasUpdate) {
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: "更新提示",
              content: "新版本已经准备好，是否重启应用？",
              success: function (result) {
                if (result.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate();
                }
              },
            });
          });
          updateManager.onUpdateFailed(function () {
            // 自动更新失败，需要用户手动删除后进入 (一般不会进入此步骤)
            wx.showModal({
              title: "已经有新版本了哟~",
              content: "新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~",
            });
          });
        }
      });
    } else {
      // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
      wx.showModal({
        title: "提示",
        content:
          "当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。",
      });
    }
    // eslint-disable-next-line
    console.debug("App.onLaunch", options);
    mai.request.interceptors.response.use(
      (res) => {
        // eslint-disable-next-line
        console.debug("request result", {
          request: res.request,
          status: res.status,
          response: res.data,
        });
        return res.data;
      },
      (error) => {
        // eslint-disable-next-line
        console.error("request error", {
          error,
          request: error.request,
          response: error.response,
        });
        return Promise.reject(error);
      }
    );
  },
});
