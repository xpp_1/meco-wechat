export default {
  ext: {
    hasSkeleton: true,
    isGroup: false,
    'mai-button': {
      shouldOauth: true,
      shouldReportFormId: false,
    },
    maiAccountId: '5ee3221af0da4b69960bf2f8',
    maiCampaign: 'meco-member-center',
    weappName: 'meco-member-center',
    h5ChannelId: '5ee832f73fd163da3681cf43',
    maiDomainCDN: 'https://statics.maiscrm.com',
    maiDomainH5: 'https://www.quncrm.com',
    maiDomainH5V2: 'https://h5.maiscrm.com',
    maiENV: 'production',
    maiEventDefaultSource: 'weapp-direct',
    overridePages: [],
    weappId: 'wxb8a223205e2083f0',
    weappInitPage: '/customize/pages/index/index',
    splashPage: 'pages/splash/index',
    weappStorageVersion: 1,
  },
};
