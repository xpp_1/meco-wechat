/***********
 *  缓存用户信息
 **********/
import {
  baseConfig
} from "./mecoBaseConfig"
export const wxloinSaveData = () => {
  return new Promise((resolve, reject) => {
    wx.login({
      success(re) {
        if (re.code) {
          wx.request({
            url: baseConfig.UCODE_URL + "/auth/mp/login",
            data: {
              code: re.code,
              appId: baseConfig.appId
            },
            success(res) {
              if (res.statusCode == 200 && res.data.token) {
                wx.setStorageSync("SESSION_INFO", res.data)
                wx.setStorageSync('UMTOKEN', res.data.token)
                resolve(res.data)
              } else {
                reject({
                  error: "error"
                })
              }


            },
            fail(err) {
              console.error(err);
              reject(err)
            }
          })

        }
      }
    })
  })
}

// 缓存scrm用户信息
export const scrmLoinSaveData = () => {
  return new Promise((resolve, reject) => {
    wx.showLoading({
      title: '加载中',
    })
    wx.login({
      success(re) {
        if (re.code) {
          wx.request({
            url: baseConfig.BASE_URL + "/meco/getAccessToken",
            data: {
              code: re.code,
              appId: baseConfig.appId
            },
            success(res) {
              if (res.data.data.code == "200" && res.data.data) {
                console.log(res.data.data,"????????????????");
                wx.setStorageSync("SCRM_TOKEN", res.data.data.data.accessToken)
                wx.setStorageSync("SCRM_VO", res.data.data.data)
                // if (res.data.data.memberVO?.phone) {
                //   wx.setStorageSync("MOBILE", res.data.data.memberVO.phone)
                // }
                wx.hideLoading()
                resolve(res.data)
              } else {
                wx.hideLoading()
                reject({
                  error: "error"
                })
              }
            },
            fail(err) {
              console.error(err);
              wx.hideLoading()
              reject(err)
            }
          })

        }
      }
    })
  })
}