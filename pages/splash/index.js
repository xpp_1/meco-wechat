import {
  ExtConfig,
  Setting
} from '../../engine/index';
import {
  scrmLoinSaveData
} from "../splash/mecoUserInfo.js"
// const {
//   mai
// } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
Page({
  async onLoad() {
    await mai.member.signin();
    await Setting.getTabbarSetting();
    scrmLoinSaveData().then(res => {

    })
    this.gotoNextPage();
  },
  gotoNextPage() {
    let redirectUrl = ExtConfig.getWeappInitPage();
    if (this.options.redirectUrl) {
      redirectUrl = decodeURIComponent(this.options.redirectUrl);
    }
    wx.reLaunch({
      url: redirectUrl
    });
  },
});
