// pages/rotate/rotate.js
import CryptoJS from "./crypto-js.min";
var codeTimer = null;
let timer = 0;
let times = 0;
let speed = 200;
const heloo = {
  decryptDES: function (ciphertext) {
    const keyHex = CryptoJS.enc.Utf8.parse('meco_seession_key')
    if (ciphertext) {
      const decrypted = CryptoJS.DES.decrypt(ciphertext, keyHex, {
        mode: CryptoJS.mode.ECB,
        padding: CryptoJS.pad.Pkcs7,
      })
      return decrypted.toString(CryptoJS.enc.Utf8)
    }
  },
  wxLogin: function () {
    let that = this;
    return new Promise((resolve, reject) => {
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      wx.login({
        success(res) {
          wx.request({
            url: that.globalData.url + "/api/userIsNull",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              code: res.code,
            },
            success(data) {
              wx.hideLoading();
              if (data.data.status == 0) {
                let userInfo = {};
                userInfo.openid = data.data.openid;
                userInfo.unionid = data.data.unionid;
                userInfo.key = that.decryptDES(data.data.session_key_secret);
                that.globalData.register = userInfo;
                that.globalData.userKey = that.decryptDES(data.data.session_key_secret);
                wx.clearStorageSync();
                reject(data);
              } else {
                that.globalData.userInfo = data.data.wxuser;
                that.globalData.userKey = that.decryptDES(data.data.session_key_secret);
                resolve(data);
              }
            },
          });
        },
      });
    });
  },
  writeTime: function (type, path, url) {
    let that = this;
    let options = wx.getLaunchOptionsSync();
    let openid = that.globalData.openid;
    if (path) {
      var page = path;
    } else {
      var page = options.path;
    }
    if (that.globalData.userInfo) {
      openid = that.globalData.userInfo.openid;
    }
    let data = {
      openid: openid,
      type: type,
      page: page,
      memo: options.scene,
    };
    if (url) {
      data.CouponURL = url;
    }
    let location = wx.getStorageSync("longitude");
    if (type == "2" && location && path) {
      data.location = location;
      data.location2 = wx.getStorageSync("latitude");
    }
    wx.request({
      url: that.globalData.url + "/api/meco/pvuv",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: data,
      success(data) {
        console.log(data);
      },
    });
  },
  writeCoupon: function (type, path, url) {
    let that = this;
    let options = wx.getLaunchOptionsSync();
    let openid = that.globalData.openid;
    if (path) {
      var page = path;
    } else {
      var page = options.path;
    }
    if (that.globalData.userInfo) {
      openid = that.globalData.userInfo.openid;
    }
    wx.request({
      url: that.globalData.url + "/api/meco/pvuv",
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      data: {
        openid: openid,
        type: type,
        page: page,
        CouponURL: url,
        location: wx.getStorageSync("longitude"),
        location2: wx.getStorageSync("latitude"),
        memo: options.scene,
      },
      success(data) {
        console.log(data);
      },
    });
  },
  globalData: {
    userInfo: null,
    register: null,
    userKey: "",
    url: "https://ssl.meco.chinaxpp.com",
    // url:"http://192.168.1.101:8888"
  },
};
// const { mai } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
const wm_shop_app_id = 'wx1a8460be609a207c'
Component({
  data: {
    flag: false, // 点击开始旋转(节流)
    isTrue0: null, // 获得到的奖品
    deg: 0, // 旋转之后的度数
    userInfo: null, // 用户信息
    drawList: [], // 奖品列表
    integral: 234, //积分数量
    drawNum: 0, // 抽奖次数
    free: 0, // 免费抽奖次数
    prizeType: null, // 中奖类型
    fromId: null, // 发送邀请人id
    isPrize: true, // 隐藏中奖弹窗
    prize: {}, // 中奖物品
    adv: [], // 轮播
    region: ["", "", ""], // 省市区
    address: "", // 详情地址
    userName: "", // 收货人
    userPhone: "", // 手机号
    shareFriend: [], // 中奖人公告
    tel: "", //验证码的手机号
    code: "", // 验证码
    getCodeText: "获取验证码",
    time: 60,
    // 一元换购券
    isShowOne: false, // 中奖弹出层
    latitude: "", // 维度
    longitude: "", // 经度
    source: "", // 来源
    buildingList: [], // 门店列表
    isWrite: false, // 核销
    qrCodeImg: "", // 二维码
    testUrl: "https://ssl.meco.chinaxpp.com", // heloo.globalData.url
    prizeList: [],
    activeNum: 0, //转动位置
    stopNum: 0, //停止位置
    chance: 0,
    isActivated: false, // 是否会员
    roundRotatephoneshow: false,
    phoneNumber: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  attached: function () {
    // ...
    this.userLogin();
    // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
    heloo.writeTime(4, "/customize/pages/index/index");
    this.initRegister();
  },
  detached: function () {
    // 在组件实例被从页面节点树移除时执行
    clearInterval(codeTimer);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === "button") {
      // 来自页面内转发按钮
      return {
        title: "Meco真茶局",
        path:
          "/customize/pages/roundRotate/roundRotate?fromId=" +
          this.data.userInfo.id,
      };
    }
    return {
      title: "Meco真茶局",
      path: "/customize/pages/index/index",
    };
  },
  methods: {
    // 群脉注册校验
    async initRegister() {
      let that = this;
      const maiOpenId = await mai.member.getOpenId();
      if (!maiOpenId) {
        await mai.member.signin();
      }
      const member = await mai.member.getDetail(["Card"]);
      console.log(member.isActivated, "round");
      that.setData({
        isActivated: member.isActivated,
      });
    },
    // 获取门店列表
    getAddressList() {
      let that = this;
      console.log(that.data.userInfo);
      wx.request({
        url: this.data.testUrl + "/api/xpppresent/range",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          longitude: that.data.longitude,
          latitude: that.data.latitude,
        },
        success(res) {
          if (res.data.status == 1) {
            // 处理距离信息
            res.data.data.forEach((el) => {
              let num = parseInt(Number(el.distance));
              el.distance =
                num > 1000 ? parseInt(num / 1000) + "km" : num + "m";
            });
            // console.log(res.data);
            that.setData({
              buildingList: res.data.data,
            });
          }
        },
      });
    },
    // 获取二维码
    getQrCode(prizeId) {
      let that = this;
      wx.request({
        url: this.data.testUrl + "/api/xpppresent/verificationList",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          longitude: this.data.longitude,
          latitude: this.data.latitude,
          presentAddrId: prizeId,
        },
        success(res) {
          console.log("qrcode", res.data.data);
          that.setData({
            qrCodeImg: res.data.data,
          });
        },
      });
    },
    // 去兑换
    goExchange() {
      this.setData({
        isShowOne: false,
        isWrite: true,
      });
    },
    //返回主页
    back() {
      wx.redirectTo({
        url: "/customize/pages/index/index",
      });
    },
    //点击开始旋转
    rotate() {
      let that = this;
      let num = that.data.activeNum;
      times++;
      if (
        times > 50 &&
        that.data.prize &&
        that.data.activeNum == that.data.stopNum
      ) {
        clearTimeout(timer); // 清除转动定时器，停止转动
        times = 0;
        timer = 0;
        speed = 200;
        that.setData({
          isStart: false,
          stopNum: "",
          isPrize: false,
        });
      } else {
        if (times < 50) {
          speed -= 20; // 加快转动速度
        } else {
          speed += 20;
        }
        if (speed < 40) {
          speed = 40;
        }
        num++;
        if (num > 7) {
          num = 0;
        }
        that.setData({
          activeNum: num,
        });
        timer = setTimeout(() => {
          that.rotate();
        }, speed);
      }
    },
    // 抽奖
    async start() {
      let that = this;
      // const member = await mai.member.getDetail(["Card"]);
      // that.setData({
      //   isActivated: member.isActivated,
      // });
      // console.log(that.data.isActivated, "roundStart______", member.isActivated);
      // if (!that.data.isActivated) {
      //   return wx.navigateTo({
      //     url: "/pages/register-member/index?completeType=back",
      //   });
      // }
      // const maiOpenId = await mai.member.getOpenId()
      // if (!maiOpenId) {
      //   await mai.member.signin();
      // }
      // const member = await mai.member.getDetail(["Card"]);
      // console.log(member.isActivated, "member");
      // if (!member.isActivated) {
      //   //是否注册会员
      //   return wx.navigateTo({
      //     url: "/pages/register-member/index?completeType=back",
      //   });
      // }
      if (that.data.userInfo) {
        if (that.data.chance > 0 || that.data.free > 0) {
          if (!that.data.isStart) {
            that.setData({
              isStart: true,
            });
            that.rotate();
            that.getPrize();
          }
        } else {
          wx.showToast({
            title: "次数不足",
            icon: "none",
          });
        }
      } else {
        that.getInfo();
      }
    },
    // 活动规则
    goRule(e) {
      let type = e.currentTarget.dataset.type;
      wx.navigateTo({
        url:
          "/customize/pages/prizeRule/prizeRule?type=" +
          type +
          "&wxId=" +
          this.data.userInfo.id,
      });
    },
    // 登录
    userLogin() {
      console.log('抽奖页面1111');
      console.log(this.data.roundRotatephoneshow);
      let that = this;
      wx.login({
        success(res) {
          wx.request({
            url: heloo.globalData.url + "/api/userIsNull",
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            data: {
              code: res.code,
            },
            success(data) {
              if (data.data.status == 0) {
                let userInfo = {};
                userInfo.openid = data.data.openid;
                userInfo.unionid = data.data.unionid;
                userInfo.key = heloo.decryptDES(data.data.session_key_secret);
                heloo.globalData.register = userInfo;
                heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
                that.setData({
                  isLogin: false,
                });
                // 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
                heloo.writeTime(1, "pages/roundRotate/roundRotate");
                heloo.writeTime(4, "pages/roundRotate/roundRotate");
                that.getList();
              } else {
                that.setData({
                  userInfo: data.data.wxuser,
                });
                console.log('状态码不是0');
                console.log(that.data.userInfo);
                heloo.globalData.userKey = heloo.decryptDES(data.data.session_key_secret);
                heloo.globalData.userInfo = data.data.wxuser;
                that.getList();
                that.getAdv();
              }
            },
            fail(res) {
              console.log(res);
            },
          });
        },
      });
    },
    //奖品列表
    getList() {
      let that = this;
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      let openid = "";
      if (that.data.userInfo) {
        openid = that.data.userInfo.openid;
        console.log(openid);
      }
      wx.request({
        url: this.data.testUrl + "/api/xpppresent/list",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          openid: openid,
        },
        success(data) {
          wx.hideLoading();
          console.log(heloo.globalData.userInfo);
          if (data.data.status == 1) {
            let row = {
              name: "start",
              isBtn: true,
            };
            let arr = [0, 1, 2, 7, 3, 6, 5, 4];
            let list = data.data.data.drawPresentList;
            list.map((item, index) => {
              item.active = arr[index];
            });
            list.splice(4, 0, row);
            const { freeDrawChance, drawChance } = data.data.data;
            that.setData({
              prizeList: list,
              free: freeDrawChance,
              chance: drawChance,
              rotateBtnUrl:
                freeDrawChance + drawChance == 0
                  ? "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/luckyDraw/rotateBtnActive.png"
                  : "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/luckyDraw/rotateBtn.png",
            });
          } else {
            wx.showToast({
              title: "活动过于火爆，请稍后重试", // 转盘偶发性空白
              icon: "none",
            });
          }
        },
        fail(res) {
          console.log(res);
        },
      });
    },
      //打开商城
  goLottery(){
    wx.openEmbeddedMiniProgram({
      path:'/hd_lego/index?isqdzz=1&tracepromotionid=100142861&id=30000036951&actId=30000036951&tmpKey=bigwheel&vid=0&productInstanceId=11970806643',
      appId:'wx1a8460be609a207c',
      // envVersion:'trial'
    })
  },
    //抽奖
    getPrize() {
      let that = this;
      wx.request({
        url: this.data.testUrl + "/api/xpppresent/draw",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          source: this.data.source,
          longitude: this.data.longitude,
          latitude: this.data.latitude,
        },
        success(data) {
          if (data.data.status == 1) {
            let list = that.data.prizeList;
            let prize = data.data.data;
            list.map((item) => {
              if (item.id == prize.drawPresentId) {
                prize.number = item.number;
                that.setData({
                  stopNum: item.active,
                  prize: prize,
                });
              }
            });
            if (that.data.free > 0) {
              let free = that.data.free;
              free--;
              that.setData({
                free: free,
              });
            } else if (that.data.chance > 0) {
              let chance = that.data.chance;
              let userInfo = that.data.userInfo;
              // userInfo.points = userInfo.points - 50;
              chance--;
              that.setData({
                chance: chance,
                userInfo: userInfo,
              });
              heloo.globalData.userInfo = userInfo;
            }
          } else {
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
            clearTimeout(timer); // 清除转动定时器，停止转动
            times = 0;
            timer = 0;
            speed = 200;
            that.setData({
              activeNum: 0,
              stopNum: "",
              isStart: false,
            });
          }
        },
      });
    },
    //校验手机号输入
    confirmphone() {
      let that = this
      var phoneReg = /^1[3456789]\d{9}$/;
      if (phoneReg.test(that.data.phoneNumber.trim())) {
        that.data.userInfo.phoneNumber = that.data.phoneNumber
        console.log("手机号格式正确");
        wx.request({
          url: heloo.globalData.url + "/pointProduct/getPhoneNum",
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          data: {
            openid: that.data.userInfo.openid,
            sessionKey: heloo.globalData.userKey,
            phone: that.data.phoneNumber,
          },
          success: function (res) {
            wx.hideLoading();
            if (res.data.status == "1") {
              console.log(res);
              heloo.globalData.userInfo = res.data.userinfo;
              that.setData({
                userInfo: res.data.userinfo,
                roundRotatephoneshow: false,
              })
              that.getPrize();
            } else {
              wx.showModal({
                title: "注册失败",
                content: res.data.msg,
              });
            }
          },
        });

      } else {
        wx.showToast({
          title: '手机号格式不正确',
          icon: 'none'
        })
      }
    },
    //用户填写手机号
    getPhone() {
      var that = this;
      console.log(that.data.userInfo);
      if (that.data.userInfo.phoneNumber) {
        that.getPrize()
      } else {
        that.setData({
          roundRotatephoneshow: true,
        })
      }
    },
    //获取手机号方法
    // getPhone(e) {
    //   var that = this;
    //   wx.checkSession({
    //     success: function () {
    //       if (e.detail.iv) {
    //         if (!that.data.userInfo) {
    //           wx.showModal({
    //             title: "授权失败",
    //             content: "尚未授权注册",
    //           });
    //           return;
    //         }
    //         wx.showLoading({
    //           title: "加载中...",
    //           mask: true,
    //         });
    //         wx.request({
    //           url: heloo.globalData.url + "/pointProduct/getPhoneNum",
    //           method: "POST",
    //           header: {
    //             "Content-Type": "application/x-www-form-urlencoded",
    //           },
    //           data: {
    //             openid: that.data.userInfo.openid,
    //             sessionKey: heloo.globalData.userKey,
    //             phonepagetype: "2",
    //             encryptedData: e.detail.encryptedData,
    //             iv: e.detail.iv,
    //           },
    //           success: function (res) {
    //             wx.hideLoading();
    //             if (res.data.status == "1") {
    //               console.log(res);
    //               heloo.globalData.userInfo = res.data.userinfo;
    //               that.setData({
    //                 userInfo: res.data.userinfo,
    //               });
    //               that.getPrize();
    //             } else {
    //               wx.showModal({
    //                 title: "手机号解密失败",
    //                 content: res.data.msg,
    //               });
    //             }
    //           },
    //         });
    //       }
    //     },
    //     fail: function () {
    //       wx.showModal({
    //         title: "授权失败",
    //         content: "登录状态过期，请重新授权！",
    //         success: function () {
    //           that.userLogin();
    //         },
    //       });
    //     },
    //   });
    // },
    // 授权
    getInfo() {
      let that = this;
      if (wx.canIUse("getUserProfile")) {
        wx.getUserProfile({
          lang: "zh_CN",
          desc: "本次授权用于个人信息显示",
          success(res) {
            console.log(res);
            wx.showLoading({
              title: "加载中...",
              mask: true,
            });
            let unionid = "";
            let openid = "";
            if (that.data.userInfo) {
              unionid = that.data.userInfo.unionid;
              openid = that.data.userInfo.openid;
            } else {
              unionid = heloo.globalData.register.unionid;
              openid = heloo.globalData.register.openid;
            }
            wx.request({
              url: heloo.globalData.url + "/api/getunionid",
              method: "POST",
              header: {
                "Content-Type": "application/x-www-form-urlencoded",
              },
              data: {
                userInfo: JSON.stringify(res.userInfo),
                unionid: unionid,
                openid: openid,
                source: "nine",
              },
              success(data) {
                console.log(data);
                wx.hideLoading();
                that.setData({
                  userInfo: data.data.wxuser,
                });
                heloo.globalData.userInfo = data.data.wxuser;
                that.userLogin();
              },
            });
          },
        });
      } else {
        wx.showModal({
          content: "微信客户端版本过低，请更新后重试！",
          showCancel: false,
          confirmText: "我知道了",
        });
      }
    },
    // 轮播
    getAdv() {
      let that = this;
      wx.request({
        url:
          heloo.globalData.url +
          `/api/xpppresent/getWinningRecord?wxid=${this.data.userInfo.id}`,
        method: "GET",
        success(res) {
          console.log(res.data);
          that.setData({
            shareFriend: res.data.heard,
            adv: res.data.data,
          });
        },
      });
    },
    // 关闭中奖弹窗
    closePrize() {
      this.setData({
        isPrize: true,
      });
    },
    // 地址信息
    bindRegionChange(e) {
      this.setData({
        region: e.detail.value,
      });
    },
    // 名字
    setName(e) {
      this.setData({
        userName: e.detail.value,
      });
    },
    // 手机号
    setPhone(e) {
      this.setData({
        userPhone:e.detail.value && e.detail.value.trim(),
      });
    },
    // 详情地址
    setAdd(e) {
      this.setData({
        address: e.detail.value,
      });
    },
    //领取积分和红包
    getPoints() {
      let that = this;
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      wx.request({
        url: heloo.globalData.url + "/api/xpppresent/getPointsRedPack",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          presentAddrId: that.data.prize.id,
        },
        success(data) {
          wx.hideLoading();
          console.log(data.data);
          if (that.data.prize.presentName == "100积分") {
            let userInfo = that.data.userInfo;
            let integral = userInfo.points + 100;
            that.setData({
              "userInfo.points": integral,
            });
          } else if (that.data.prize.presentName == "5积分") {
            let userInfo = that.data.userInfo;
            let integral = userInfo.points + 5;
            that.setData({
              "userInfo.points": integral,
            });
          }
          if (data.data.status == 1) {
            that.setData({
              isPrize: true,
              prize: null,
            });

            wx.showToast({
              title: "领取成功！",
              icon: "success",
            });
          } else {
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
          }
        },
      });
    },
    // 领取实物
    submitInfo() {
      let that = this;
      if (that.data.userName == "") {
        wx.showToast({
          title: "请填写姓名！",
          icon: "none",
        });
        return;
      }
      if (that.data.userPhone.length != 11) {
        wx.showToast({
          title: "请填写正确手机号！",
          icon: "none",
        });
        return;
      }
      if (that.data.region[2] == "") {
        wx.showToast({
          title: "请选择省市区！",
          icon: "none",
        });
        return;
      }
      if (that.data.address == "") {
        wx.showToast({
          title: "请填写详细地址！",
          icon: "none",
        });
        return;
      }
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      wx.request({
        url: heloo.globalData.url + "/api/present/update",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          presentAddrId: that.data.prize.id,
          name: that.data.userName,
          phone: that.data.userPhone,
          province:
            that.data.region[0] + that.data.region[1] + that.data.region[2],
          shipaddr: that.data.address,
        },
        success(data) {
          wx.hideLoading();
          if (data.data.status == 1) {
            that.setData({
              userName: "",
              userPhone: "",
              address: "",
              region: ["", "", ""],
              isPrize: true,
            });
            wx.showToast({
              title: "信息已保存，静待发货",
              icon: "none",
            });
          } else {
            wx.showToast({
              title: data.data.message,
              icon: "none",
            });
          }
        },
      });
    },
    // 领取微商城券
    getcard() {
      wx.navigateToMiniProgram({
        appId: wm_shop_app_id,
        path: "/cms_design/index?isqdzz=1&tracepromotionid=100077529&productInstanceId=11970818643&vid=0",
      });
    },
    // 邀请好友
    friendHelp() {
      console.log("助力接口请求开始", heloo.globalData.userInfo.id);
      wx.request({
        url:
          heloo.globalData.url +
          `/api/xpppresent/inviteFriends?wxid=${heloo.globalData.userInfo.id}&launchId=${this.options.fromId}`,
        method: "GET",
        success(res) {
          console.log("助力接口请求结束");
          console.log(res.data.message);
        },
      });
    },
    // 验证码手机号
    setTel(e) {
      this.setData({
        tel: e.detail.value,
      });
    },
    // 点击获取验证码
    getCodeBtn() {
      let that = this;
      if (that.data.getCodeText == "获取验证码") {
        if (that.data.tel.length != 11) {
          wx.showToast({
            title: "请填写正确手机号！",
            icon: "none",
          });
          return;
        }
        that.setData({
          getCodeText: 60 + "s",
        });
        that.getCode();
        codeTimer = setInterval(() => {
          if (that.data.time < 1) {
            clearInterval(codeTimer);
            that.setData({
              getCodeText: "获取验证码",
              time: 60,
            });
          } else {
            let times = that.data.time;
            times--;
            that.setData({
              getCodeText: times + "s",
              time: times,
            });
          }
        }, 1000);
      }
    },
    // 发请求获取code
    getCode() {
      let that = this;
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      wx.request({
        url: heloo.globalData.url + "/api/xpppresent/getVilifyCode",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          phone: that.data.tel,
        },
        success: function (res) {
          wx.hideLoading();
          console.log(res);
          if (res.data.status == "1") {
            wx.showToast({
              title: "短信已发送",
            });
          } else {
            wx.showModal({
              title: "验证码失败",
              content: res.data.message,
            });
          }
        },
      });
    },
    // 获取到的验证码
    sendCode(e) {
      this.setData({
        code: e.detail.value,
      });
    },
    // 验证码领取奖品
    codeSubmit() {
      let that = this;
      if (that.data.tel.length != 11) {
        wx.showToast({
          title: "请填写正确手机号！",
          icon: "none",
        });
        return;
      }
      if (that.data.code == "") {
        wx.showToast({
          title: "请填写验证码！",
          icon: "none",
        });
        return;
      }
      wx.showLoading({
        title: "加载中...",
        mask: true,
      });
      wx.request({
        url: heloo.globalData.url + "/api/xpppresent/getMonthCardCode",
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        data: {
          wxid: that.data.userInfo.id,
          phone: that.data.tel,
          presentAddrId: that.data.prize.id,
          code: that.data.code,
        },
        success: function (res) {
          wx.hideLoading();
          if (res.data.status == "1") {
            wx.showToast({
              title: "领取成功！",
              icon: "success",
            });
            that.setData({
              isPrize: true,
              code: "",
              codePhone: "",
            });
          } else {
            wx.showModal({
              title: "领取失败",
              content: res.data.message,
            });
          }
        },
      });
    },
    // 一元换购
    closeOne() {
      this.setData({
        isShowOne: false,
        isWrite: false,
      });
    },
    // 打电话
    callTel(e) {
      wx.makePhoneCall({
        phoneNumber: e.currentTarget.dataset.tel + "",
        success: function () {
          console.log("拨打电话成功！");
        },
      });
    },
  },
});
