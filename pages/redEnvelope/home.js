// pages/scan/scan.js
import CryptoJS from "./crypto-js.min";
const mecoAPI = require("webapi.js");
const umaUrl = "https://ucode-openapi.aax6.cn";
import FMAgent from "../redEnvelope/fm-1.6.5-es.min";
const heloo = {
	decryptDES: function (ciphertext) {
		const keyHex = CryptoJS.enc.Utf8.parse('meco_seession_key')
		if (ciphertext) {
			const decrypted = CryptoJS.DES.decrypt(ciphertext, keyHex, {
				mode: CryptoJS.mode.ECB,
				padding: CryptoJS.pad.Pkcs7,
			})
			return decrypted.toString(CryptoJS.enc.Utf8)
		}
	},
	wxLogin: function () {
		let that = this;
		return new Promise((resolve, reject) => {
			wx.showLoading({
				title: "加载中...",
				mask: true,
			});
			wx.login({
				success(res) {
					wx.request({
						url: that.globalData.url + "/api/userIsNull",
						method: "POST",
						header: {
							"Content-Type": "application/x-www-form-urlencoded",
						},
						data: {
							code: res.code,
						},
						success(data) {
							wx.hideLoading();

							if (data.data.status == 0) {
								let userInfo = {};
								userInfo.openid = data.data.openid;
								userInfo.unionid = data.data.unionid;
								userInfo.key = that.decryptDES(data.data.session_key_secret);
								that.globalData.register = userInfo;
								that.globalData.userKey = that.decryptDES(data.data.session_key_secret);
								wx.clearStorageSync();
								reject(data);
							} else {
								that.globalData.userInfo = data.data.wxuser;
								that.globalData.userKey = that.decryptDES(data.data.session_key_secret);
								resolve(data);
							}
						},
					});
				},
			});
		});
	},
	writeTime: function (type, path, url) {
		let that = this;
		let options = wx.getLaunchOptionsSync();
		let openid = that.globalData.openid;
		if (path) {
			var page = path;
		} else {
			var page = options.path;
		}
		if (that.globalData.userInfo) {
			openid = that.globalData.userInfo.openid;
		}
		let data = {
			openid: openid,
			type: type,
			page: page,
			memo: options.scene,
		};
		if (url) {
			data.CouponURL = url;
		}
		let location = wx.getStorageSync("longitude");
		if (type == "2" && location && path) {
			data.location = location;
			data.location2 = wx.getStorageSync("latitude");
		}
		wx.request({
			url: that.globalData.url + "/api/meco/pvuv",
			method: "POST",
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			data: data,
			success(data) {
				console.log(data);
			},
		});
	},
	writeCoupon: function (type, path, url) {
		let that = this;
		let options = wx.getLaunchOptionsSync();
		let openid = that.globalData.openid;
		if (path) {
			var page = path;
		} else {
			var page = options.path;
		}
		if (that.globalData.userInfo) {
			openid = that.globalData.userInfo.openid;
		}
		wx.request({
			url: that.globalData.url + "/api/meco/pvuv",
			method: "POST",
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			data: {
				openid: openid,
				type: type,
				page: page,
				CouponURL: url,
				location: wx.getStorageSync("longitude"),
				location2: wx.getStorageSync("latitude"),
				memo: options.scene,
			},
			success(data) {
				console.log(data);
			},
		});
	},
	globalData: {
		userInfo: null,
		register: null,
		userKey: "",
		url: "https://ssl.meco.chinaxpp.com",
		// url:"http://192.168.1.44:8080",
		baseUrlOrchard: "https://xpph7.happydoit.com:8090",
		_fmOpt: {
			partnerCode: "chinaxpp", // 请填入您的partner code
			appName: "meco_xcx", // 请填入您的app name
			// 谨慎配置，确认是生产环境的情况下才需要设置下面的值，测试沙盒环境下面配置需要删掉
			env: "PRODUCTION"
		},
	},
};
// const {
// 	mai
// } = getApp();
const { usR } = getApp();
const mai = {
  member:usR
}
const wm_shop_app_id = 'wx1a8460be609a207c'
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		swiper: 0,
		userInfo: {
			avatarUrl: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/meco/defaultuser.png",
			city: "",
			country: "",
			gender: 0,
			language: "zh_CN",
			nickName: "微信昵称",
			province: "",
		},
		openSet: false,
		pic: [],
		prize: null,
		isShow: false,
		isShow2023: false,
		isGet: false,
		redpackagerule: false,
		nowTime: '', //当前时间
		// 中奖信息填写
		userName: "",
		userPhone: "",
		userCard: "",
		address: "",
		umToken: "",
		umSerialId: "",
		wwAppId: "", //解码前信息
		ucode: "", //解码前信息
		longitude: "",
		latitude: "",
		umCode: "", //解码后信息
		umUri: "", //解码后信息
		codeStatusTip: "", //首次扫码进入后提示，用于反复点击“开奖”时现实
		uuid: "",
		prizeOrderNum: "",
		getted: false,
		pageNo: 0,
		record: [],
		isRecord: true,
		hideRule3: true,
		hideRule4: true,
		hideRule5: true,
		gifUrl: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/tea/open4.png",
		gifUrlLoop: "https://meco-1259435766.cos.ap-shanghai.myqcloud.com/jz/tea/open4.png",
		cardNumber: "", // 卡片id
		isOpened: false, // 四期红包是否已开启过
		fk: false, //判断用户风险等级 true:部分风险，false:高风险拦截中现金红包
		blacklist: false,
		ruleVersion: null,
		newyear: false, //判断是否是2023年的码
		phoneNumber:"",
		phonedialog:true,
		methodstype:1,
	},

	gifImgLoad: function (e) {
		//     let gifurl = this.data.gifUrl;
		//     let nowTime = +new Date();
		//     setTimeout(() => {
		//       this.setData({
		//         gifUrlLoop: gifurl + '?' + nowTime
		//     })
		//   }, 800)
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		console.log('判断是否更新了————————————3');
		let that = this;
		// that.codeTip("Meco真茶局更新服务中");
		// return
		let scene = wx.getLaunchOptionsSync().scene;
		var fmagent = new FMAgent(heloo.globalData._fmOpt);
		console.log(options);
		var date = new Date();
		var year = date.getFullYear() //年
		var month = date.getMonth() + 1 //月
		var day = date.getDate() //日
		this.setData({
			nowTime: year + '年' + month + '月' + day + '日'
		})
		/**
		 * 测试页面参数 code_ticket: "b4a651bc5be1883ea3f989f615ea8063" wwAppId: "wx15bad65549cdd897"
		 */
		// 场景值判断
		if (
			scene != 1013 &&
			scene != 1011 &&
			scene != 1012 &&
			scene != 1124 &&
			scene != 1125 &&
			scene != 1035
		) {
			if (!options.code_ticket) {
				wx.reLaunch({
					url: "/customize/pages/index/index",
				});
				return;
			}
		}
		// 扫码进入小程序自带参数
		that.setData({
			ucode: options.code_ticket,
			wwAppId: options.wwAppId,
		});
		// 登录获取用户信息
		heloo.wxLogin().then(
			(res) => {
				console.log('已注册');
				console.log(res);
				//已注册
				that.setData({
					userInfo: res.data.wxuser,
				});
				// if(!heloo.globalData.userInfo.phoneNumber){
				// 	that.getPhoneByDefault()
				// 	return
				// }
				heloo.writeTime(1);
				// heloo.writeTime(2)
				that.umaLogin();
				fmagent.getInfo({
					page: that, // 请传入FMAgent所在的Page或Component对象
					openid: heloo.globalData.userInfo.openid, // 请传入加密的用户openid
					// 如果您开通了unionid功能，请传入加密的用户unionid，
					// 否则留空即可
					unionid: heloo.globalData.userInfo.unionid ?
						heloo.globalData.userInfo.unionid : "",
					noClipboard: true,
					success(res) {
						console.log('已注册执行风控');
						console.log(heloo.globalData);
						let blackbox = res
						wx.setStorageSync('blackbox', blackbox)
						wx.request({
							url: "https://playbill.chinaxpp.com:8090/grouponApi0921/fraudCheck", //仅为示例，并非真实的接口地址
							method: "POST",
							data: {
								black_box: blackbox,
								account_login: heloo.globalData.userInfo.openid,
								account_mobile: heloo.globalData.userInfo.phoneNumber,
								openid: heloo.globalData.userInfo.openid,
								nickname: heloo.globalData.userInfo.nickname,
								register_occur_time: heloo.globalData.userInfo.createTime,
								scene: "logo",
								upload_type: "url",
								image_url: 'null',
								event_id: 1, //扫码
								package_id: '2' //扫码传2，果园传1
							},
							header: {
								"content-type": "application/json", // 默认值
							},
							success(res) {
								console.log(res.data);
								var info = res.data.data;
								console.log('执行风控1成功')
								
								// Review需要执行拍照风控操作
								if (info.final_decision == "Reject") {
									that.codeTip("当前活动太火爆，请稍后再试");
									that.setData({
										fk: false,
									})
									return
									that.setData({
										fk: false,
									})
									// 1：小程序访问 2：一杯一码扫码 3：签到 4：宠粉大抽奖 5：爱合成 6：水果大作战
									heloo.writeTime(1);
									that.umaLogin();
								}
								 else if(info.final_decision == "Review") {
									console.log(blackbox)
									that.setData({
										fk: true,
									})
									that.ctx = wx.createCameraContext()
								}
							},
							fail(res) {
								console.log(res);
								console.log('风控调用失败了');
								heloo.writeTime(1);
								that.umaLogin();
								// 失败回调，res为各种exception对象
							},
						});
						// 成功回调，res为blackbox字符串
					},
					fail: function (res) {
						console.log(res);
						heloo.writeTime(1);
						that.umaLogin();
						// 失败回调，res为各种exception对象
					},
				})
			},
			(res) => {
				console.log('未注册');
				//未注册
				that.setInfo()
			}
		);

		var _this = this;
		mecoAPI.post({
			url: "/openseller/GetSellerCouponPlugsinsParams",
			success: function (r) {
				console.log("GetSellerCouponPlugsinsParams", r);
				if (r.Code != 1) return;
				_this.setData(r.Data);
			},
		});
	},
	//用户不允许摄像头
	error(e) {
		wx.showToast({
			title: '不授权将无法参与该次营销活动',
			icon: 'none'
		})
		setTimeout(() => {
			this.setData({
				fk: false
			})
			wx.redirectTo({
				url: '/customize/pages/index/index',
			})
		}, 1500);

	},
	//拍照
	takePhoto() {
		let that = this
		that.ctx.takePhoto({
			quality: 'high',
			success: (res) => {
				console.log(res)
				const path = res.tempImagePath;
				wx.showLoading({
					title: "上传中...",
					mask: true,
				});
				wx.uploadFile({
					url: 'https://ssl.meco.chinaxpp.com/imageUpWXTX', //仅为示例，非真实的接口地址
					filePath: path,
					name: "upfile",
					formData: {
						fileName: path.split("wxfile://")[1], //线上
						// "fileName": path.split("http://tmp/")[1] //线下
					},
					success(res) {
						console.log(wx.getStorageSync('blackbox'))
						let img = JSON.parse(res.data);
						console.log('图片执行成功');
						wx.hideLoading();
						wx.request({
							url: "https://playbill.chinaxpp.com:8090/grouponApi0921/fraudCheck", //仅为示例，并非真实的接口地址
							method: "POST",
							data: {
								black_box: wx.getStorageSync('blackbox'),
								account_login: heloo.globalData.userInfo.openid,
								account_mobile: heloo.globalData.userInfo.phoneNumber,
								openid: heloo.globalData.userInfo.openid,
								nickname: heloo.globalData.userInfo.nickname,
								upload_type: "url",
								image_url: img.url,
								scene: "logo",
								event_id: 2, //上传图片
								register_occur_time: heloo.globalData.userInfo.createTime,
								package_id: '2' //扫码传2，果园传1
								// gps_address:`${that.longitude,that.latitude}`
							},
							header: {
								"content-type": "application/json", // 默认值
							},
							success(res) {
								console.log(res.data);
								that.setData({
									fk: false
								})
								heloo.writeTime(1);
								that.umaLogin();
							},
							fail(res) {
								heloo.writeTime(1);
								that.umaLogin();
								that.setData({
									fk: false
								})
							},
						});
						console.log(res);
					},
					fail(res) {
						console.log(res);
						that.codeTip("上传失败，请稍后再试");
					},
				});
				this.setData({
					src: res.tempImagePath
				})
			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		let that = this;
		if (that.data.openSet) {
			wx.getLocation({
				type: "wgs84",
				success(res) {
					wx.setStorageSync("longitude", res.longitude);
					wx.setStorageSync("latitude", res.latitude);
					that.setData({
						longitude: res.longitude,
						latitude: res.latitude,
					});
				},
				fail() {
					// wx.showModal({
					//     title: '位置信息未授权',
					//     content: '扫码中断，请退出小程序后重试',
					// });
				},
				complete() {
					that.getPic();
					var fmagent = new FMAgent(heloo.globalData._fmOpt);
					fmagent.getInfo({
						page: that, // 请传入FMAgent所在的Page或Component对象
						openid: heloo.globalData.userInfo.openid, // 请传入加密的用户openid
						// 如果您开通了unionid功能，请传入加密的用户unionid，
						// 否则留空即可
						unionid: heloo.globalData.userInfo.unionid ?
							heloo.globalData.userInfo.unionid : "",
						noClipboard: true,
						success(res) {
							console.log(res)
							console.log('执行onshow风控成功')
							let blackbox = res
							wx.setStorageSync('blackbox', blackbox)
							wx.request({
								url: "https://playbill.chinaxpp.com:8090/grouponApi0921/fraudCheck", //仅为示例，并非真实的接口地址
								method: "POST",
								data: {
									black_box: blackbox,
									account_login: heloo.globalData.userInfo.openid,
									account_mobile: heloo.globalData.userInfo.phoneNumber,
									openid: heloo.globalData.userInfo.openid,
									nickname: heloo.globalData.userInfo.nickname,
									scene: "logo",
									upload_type: "url",
									image_url: 'null',
									event_id: 1, //扫码
								},
								header: {
									"content-type": "application/json", // 默认值
								},
								success(res) {
									console.log(res.data);
									var info = res.data.data;
									// Review 为部分有风险需要执行拍杯身码动作
									if (info.final_decision == "Reject") {
										that.codeTip("当前活动太火爆，请稍后再试");
										that.setData({
											fk: false,
										})
										return
										that.setData({
											fk: false,
										})
										that.getPic();
									} 
									else if(info.final_decision == "Review") {
										that.setData({
											fk: true,
										})
										that.ctx = wx.createCameraContext()
									
									}
								},
								fail(res) {
									console.log(res);
									that.getPic();
									// 失败回调，res为各种exception对象
								},
							});
							// 成功回调，res为blackbox字符串
						},
						fail: function (res) {
							console.log(res);
							that.getPic();
							// 失败回调，res为各种exception对象
						},
					});
				},
			});
		}
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {
		return {
			title: "Meco真茶局",
			path: "/customize/pages/index/index",
			// imageUrl: "/images/shareicon.jpg"
		};
	},
	// 分发微商城优惠券
  sendWmCoupon: async function () {
    if (!mai.member.getOpenId()) {
      await mai.member.signin();
    }
    // const openId = mai.member.getOpenId();
		// console.log(openId, "openId");
		const unionId = mai.member.getUnionId();
    let wid = wx.getStorageSync("wid");
    if (!wid) {
      // 导入客户
      let res = await mecoAPI.wmRequest({
        url: "/apigw/weimob_crm/v2.0/customer/import",
        method: "POST",
        data: {
          importType: 1,
          userList: [
            {
              appId: wm_shop_app_id,
              unionId,
              appChannel: 1,
              // userName: "test",
            },
          ],
        },
      });
			console.log(res, "res343434");
			const resWid = res?.data?.successList[0]?.wid
			if (resWid) {
				wid = resWid
				wx.setStorageSync("wid", wid);
			}
      if (res?.data?.errorList[0]?.errorMessage == "该客户已存在") {
        wid = res?.data?.errorList[0]?.wid;
        if (wid) {
          wx.setStorageSync("wid", wid);
        }
      }
    }
    console.log(wid, "wid");
    //获取uuid
    const requestId = mecoAPI.getUUID()
    console.log(requestId,'requestId');
		const couponIds = [
      66249101,
      66233087,
      66241177,
      66254054,
      66254055,
      66245999,
      66230968,
      66254057,
      66240089,
			66246000,
			//20230901起30元优惠券
			66354541,
			66350542,
			66353577
    ];
    let couponNums = couponIds.map((item) => {
      return {
        couponTemplateId: item,
        num: 1,
        requestId,
      };
		});
		console.log(couponNums,'couponNums');
    //分发wm优惠券
    let couponRes = await mecoAPI.wmRequest({
      url: "/apigw/weimob_crm/v2.0/coupon/receive",
      method: "POST",
			data: {
				couponNums,
        // couponNums: [
        //   {
        //     couponTemplateId: "66212848",//优惠券 ID
        //     requestId,
        //     num: 1,
        //   },
        //   {
        //     couponTemplateId: "66209697",
        //     requestId,
        //     num: 1,
        //   },
        //   {
        //     couponTemplateId: "66206735",
        //     requestId,
        //     num: 1,
        //   },
        // ],
        wid,
        scene: 945000,//领券场景，945000-api发券；
        vidType: 2,//组织架构节点类型。类型包括：1-集团；2-品牌；3-区域；4部门；5-商场；6-楼层；10-门店；11-网点；100-自提点
        vid: 6016299774643,//组织架构节点 ID
      },
    });

    console.log(couponRes, 'couponRes');
    const couponList = couponRes?.data?.couponResultList
    let flag = false
    if (couponList && couponList.length>0) {
      couponList.forEach(item => {
        if (item.isSuccess) {
          flag = true
        }
      })
    }
    if (flag) {
      wx.showToast({icon:'none',title:'券已发'})
    }
  },
	// 点击开奖按钮
	start() {
		// 开奖前分发微商城优惠券
		this.sendWmCoupon()
		let that = this;
		console.log('有手机号')
		if (that.data.latitude && that.data.longitude) {
			console.log('有经纬度')
			if (that.data.userInfo.id) {
				wx.request({
					url: heloo.globalData.url + "/api/blacklist/getBlack",
					method: "POST",
					header: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
					data: {
						wxid: that.data.userInfo.id,
					},
					success(data) {
						if (data.data.status == 1 && data.data.type == 1) {
							console.log('如果是黑名单用户让他开奖中15元优惠券')
							that.setData({
								blacklist: true,
								
							});
						}
						that.lottery();
					},
					fail(res) {
						that.lottery();
					}
				})
			} else {
				that.getInfo();
			}
		} else {
			console.log('没有经纬度')
			wx.showModal({
				title: "位置信息未授权",
				content: "是否立即前往，进行红包开奖？",
				cancelText: "否",
				confirmText: "是",
				success(res) {
					if (res.confirm) {
						that.setData({
							openSet: true,
						});
						wx.openSetting({
							withSubscriptions: true,
						});
					}
				},
			});
		}
	},
	swiperChange(e) {
		this.setData({
			swiper: e.detail.current,
		});
	},
	showRule() {
		this.setData({
			hideRule3: this.data.ruleVersion == 3 ? false : true,
			hideRule4: this.data.ruleVersion == 4 ? false : true,
			hideRule5: this.data.ruleVersion == 5 ? false : true,
		});
	},
	showRule2023() {
		this.setData({
			isShow2023: true,
			redpackagerule: true
		})
	},
	hideRule() {
		this.setData({
			hideRule3: true,
			hideRule4: true,
			hideRule5: true,
		});
	},
	closePrize() {
		let that = this;
		this.setData({
			isShow: false,
			isShow2023: false,
		});
		wx.redirectTo({
			url: `/customize/pages/roundRotate/roundRotate?longitude=${this.data.longitude}&latitude=${this.data.latitude}&source=3`,
		});
	},
	close() {
		this.setData({
			isShow: false,
			isShow2023: false,
		});
	},
	ruleclose() {
		this.setData({
			redpackagerule: false,
		});
		if (!this.data.prize || !this.data.prize.prizeType) {
			this.setData({
				isShow2023: false
			})
		}
	},
	openredpackagerule() {
		this.setData({
			redpackagerule: true,
		});
	},
	toUse() {
		this.setData({
			isShow: false,
			isShow2023: false,
		});
		wx.redirectTo({
			url: `/customize/pages/roundRotate/roundRotate?longitude=${this.data.longitude}&latitude=${this.data.latitude}&source=3`,
		});
	},
	toIndex() {
		this.setData({
			isShow: false,
			isShow2023: false,
		});
		console.log("toIndex");
		wx.redirectTo({
			url: "/customize/pages/index/index",
		});
	},
	closeGet() {
		let that = this;
		this.setData({
			isGet: false,
		});
		wx.reLaunch({
			url: "/customize/pages/index/index",
		});
	},
	setName(e) {
		this.setData({
			userName: e.detail.value,
		});
	},
	setCardId(e) {
		this.setData({
			userCard: e.detail.value,
		});
	},
	setPhone(e) {
		this.setData({
			userPhone: e.detail.value,
		});
	},
	setAdd(e) {
		this.setData({
			address: e.detail.value,
		});
	},
	getcoupon(params) {
		var _this = this;
		if (_this.data.latitude && _this.data.longitude) {
			console.log(params);
			if (this.data.getted) {
				return;
			}
			if (params.detail.errcode != "OK") {
				console.log(params.detail);
				wx.showModal({
					title: "领取失败",
					content: params.detail.msg,
					confirmText: "我知道了",
					showCancel: false,
				});
				return;
			}

			if (!params.detail.send_coupon_result) {
				console.log(params.detail);
				if (_this.data.clickindex == "0") {
					wx.showModal({
						title: "领取失败",
						content: "没有可领取的商家券",
						confirmText: "我知道了",
						showCancel: false,
					});
				}
				return;
			}

			var reqList = [];
			for (var i = 0; i < params.detail.send_coupon_result.length; i++) {
				var send_coupon = params.detail.send_coupon_result[i];
				if (send_coupon.code == "SUCCESS") {
					var req = {};
					req.coupon_code = send_coupon.coupon_code;
					req.out_request_no = send_coupon.out_request_no;
					req.stock_id = send_coupon.stock_id;
					reqList.push(req);
				}
			}

			if (reqList.length == 0) return;

			mecoAPI.post({
				showErrorMessage: false,
				url: "/openseller/SaveReceiveSellerCouponRecord",
				data: {
					openID: "小程序openid",
					list: reqList,
				},
				success: function (r) {
					if (r.Code == 1 && r.Data) {
						setTimeout(() => {
							wx.showToast({
								title: "已加入卡包！",
								icon: "success",
								duration: 2000,
							});

							_this.setData({
								getted: true,
							});
						}, 200);
					} else {
						if (_this.data.clickindex == "0") {
							wx.showModal({
								title: "温馨提示",
								content: r.Message,
								confirmText: "我知道了",
								showCancel: false,
							});
						}
					}
				},
			});
		}

	},
	getUserlocation() {
		// 本地经纬度
		console.log('执行getUserlocation');
		let that = this;
		let longitude = wx.getStorageSync("longitude");
		let latitude = wx.getStorageSync("latitude");
		if (latitude) {
			that.setData({
				longitude: longitude,
				latitude: latitude,
			});
			that.getPic();
		} else {
			console.log('调用定位');
			wx.getLocation({
				type: "wgs84",
				success(res) {
					wx.setStorageSync("longitude", res.longitude);
					wx.setStorageSync("latitude", res.latitude);
					that.setData({
						longitude: res.longitude,
						latitude: res.latitude,
					});
				},
				fail() {
					wx.showModal({
						title: "位置信息未授权",
						content: "扫码中断，请退出小程序后重试",
						success(res) {
							if (res.confirm) {
								wx.openSetting({
									success(res) {
										console.log(res);
										if (res.authSetting["scope.userLocation"]) {
											that.getUserlocation();
										}
									},
								});
							}
						},
					});
				},
				complete() {
					that.getPic();
				},
			});
		}
	},

	// 头像授权
	getInfo() {
		let that = this;
		if (wx.canIUse("getUserProfile")) {
			wx.getUserProfile({
				lang: "zh_CN",
				desc: "本次授权用于个人信息显示",
				success(res) {
					wx.showLoading({
						title: "加载中...",
						mask: true,
					});
					wx.request({
						url: heloo.globalData.url + "/api/getunionid",
						method: "POST",
						header: {
							"Content-Type": "application/x-www-form-urlencoded",
						},
						data: {
							userInfo: JSON.stringify(res.userInfo),
							unionid: heloo.globalData.register.unionid,
							openid: heloo.globalData.register.openid,
						},
						success(data) {
							wx.hideLoading();
							that.setData({
								userInfo: data.data.wxuser,
							});
							heloo.globalData.userInfo = data.data.wxuser;
							//判断该用户是否为黑名单用户
							wx.request({
								url: heloo.globalData.url + "/api/blacklist/getBlack",
								method: "POST",
								header: {
									"Content-Type": "application/x-www-form-urlencoded",
								},
								data: {
									wxid: that.data.userInfo.id,
								},
								success(data) {
									console.log('黑名单用户中15元优惠券')
									if (data.data.status == 1 && data.data.type == 1) {
										that.setData({
											blacklist: true,
										});
									}
									that.lottery();
								}
							})

						},
					});
				},
			});
		} else {
			wx.showModal({
				content: "微信客户端版本过低，请更新后重试！",
				showCancel: false,
				confirmText: "我知道了",
			});
		}
	},
	// 自定义授权，微信收回授权接口，给用户默认信息进行注册
	setInfo() {
		let that = this;
		wx.request({
			url: heloo.globalData.url + "/api/getunionid",
			method: "POST",
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			data: {
				userInfo: JSON.stringify(that.data.userInfo),
				unionid: heloo.globalData.register.unionid,
				openid: heloo.globalData.register.openid,
			},
			success(data) {
				that.setData({
					userInfo: data.data.wxuser,
				});
				that.umaLogin();
				heloo.globalData.userInfo = data.data.wxuser;
				//未注册
				let info = heloo.globalData.register ? heloo.globalData.register : heloo.globalData.userInfo
				fmagent.getInfo({
					page: that, // 请传入FMAgent所在的Page或Component对象
					openid: info.openid, // 请传入加密的用户openid
					// 如果您开通了unionid功能，请传入加密的用户unionid，
					// 否则留空即可
					unionid: info.unionid ?
						info.unionid : "",
					noClipboard: true,
					success: function (res) {
						console.log('执行风控2成功')
						console.log(res);
						let blackbox = res;
						wx.setStorageSync('blackbox', blackbox)
						wx.request({
							url: "https://playbill.chinaxpp.com:8090/grouponApi0921/fraudCheck", //仅为示例，并非真实的接口地址
							method: "POST",
							data: {
								black_box: blackbox,
								account_login: info.openid,
								openid: info.openid,
								event_id: 1, //扫码
								package_id: '2' //扫码传2，果园传1
							},
							header: {
								"content-type": "application/json", // 默认值
							},
							success(res) {
								console.log(res.data);
								let fkinfo = res.data.data;
								// Review 为有风险
								if (fkinfo.final_decision == "Reject") {
									that.codeTip("当前活动太火爆，请稍后再试");
									that.setData({
										fk: false,
									})
									return
								
									that.umaLogin();
								} 
								else if(fkinfo.final_decision == "Review") {
									that.setData({
										fk: true,
									})
									that.ctx = wx.createCameraContext()
								}
							},
							fail(res) {
								console.log(res);
								that.umaLogin();
								// 失败回调，res为各种exception对象
							},
						});
					},
					fail: function (res) {
						console.log(res);
						that.umaLogin();
						// 失败回调，res为各种exception对象
					},
				});
			},
		});
	},
	//获取输入的手机号
	getphoneNumber(e){
		console.log(e);
this.setData({
	phoneNumber:e.detail.value && e.detail.value.trim()
})
	},
	//校验手机号输入
	confirmphone(){
		let that = this
		var phoneReg = /^1[3456789]\d{9}$/;
		if (phoneReg.test(that.data.phoneNumber)) {
				heloo.globalData.userInfo.phoneNumber = that.data.phoneNumber
				console.log(heloo.globalData);	
			console.log("手机号格式正确");
				wx.request({
					url: heloo.globalData.url + "/pointProduct/getPhoneNum",
					method: "POST",
					header: {
						"Content-Type": "application/x-www-form-urlencoded",
					},
					data: {
						openid: that.data.userInfo.openid,
						sessionKey: heloo.globalData.userKey,
						phone:that.data.phoneNumber,
					},
					success: function (res) {
						wx.hideLoading();
						if (res.data.status == "1") {
							console.log(res);
							heloo.globalData.userInfo = res.data.userinfo;
							that.setData({
								userInfo: res.data.userinfo,
								phonedialog:true,
							})
							if(that.data.methodstype == 1){
							that.start();
						}else if(that.data.methodstype == 2){
							that.getPrize();
						}
						} else {
							wx.showModal({
								title: "注册失败",
								content: res.data.msg,
							});
						}
					},
				});
			
		  } else {
			wx.showToast({
				title:'手机号格式不正确',
				icon: "none",
			});
		  }
	},
	// 自定义授权后点击开红包进行手机号注册
	getPhoneByDefault(e) {
		var that = this;
		if(heloo.globalData.userInfo.phoneNumber){
			that.start();
			that.setData({
				phonedialog:true,
			})
			console.log('有手机号')
		}else {
			that.setData({
				phonedialog:false,
				methodstype:1,
			})
			console.log('没有手机号')
		}
		
		// wx.checkSession({
		// 	success: function () {
		// 		if (e.detail.iv) {

		// 			if (!that.data.userInfo) {
		// 				wx.showModal({
		// 					title: "授权失败",
		// 					content: "尚未授权注册",
		// 				});
		// 				return;
		// 			}
		// 			wx.showLoading({
		// 				title: "加载中...",
		// 				mask: true,
		// 			});
		// 			wx.request({
		// 				url: heloo.globalData.url + "/pointProduct/getPhoneNum",
		// 				method: "POST",
		// 				header: {
		// 					"Content-Type": "application/x-www-form-urlencoded",
		// 				},
		// 				data: {
		// 					openid: that.data.userInfo.openid,
		// 					sessionKey: heloo.globalData.userKey,
		// 					phonepagetype: "2",
		// 					encryptedData: e.detail.encryptedData,
		// 					iv: e.detail.iv,
		// 				},
		// 				success: function (res) {
		// 					wx.hideLoading();
		// 					if (res.data.status == "1") {
		// 						console.log(res);
		// 						heloo.globalData.userInfo = res.data.userinfo;
		// 						that.setData({
		// 							userInfo: res.data.userinfo,
		// 						});
		// 						that.start();
		// 					} else {
		// 						wx.showModal({
		// 							title: "手机号解密失败",
		// 							content: res.data.msg,
		// 						});
		// 					}
		// 				},
		// 			});
		// 		}
		// 	},
		// 	fail: function () {
		// 		wx.showModal({
		// 			title: "授权失败",
		// 			content: "登录状态过期，请重新授权！",
		// 			success: function () {
		// 				that.userLogin();
		// 			},
		// 		});
		// 	},
		// });
	},                                                           
	getPhone(e) {
		var that = this;
		if(heloo.globalData.userInfo.phoneNumber){
			that.getPrize();
			that.setData({
				phonedialog:true,
			})
		}else {
			that.setData({
				phonedialog:false,
				methodstype:2, //1是开奖2是领取奖品
			})
		}
		// wx.checkSession({
		// 	success: function () {
		// 		if (e.detail.iv) {
		// 			if (!that.data.userInfo) {
		// 				wx.showModal({
		// 					title: "授权失败",
		// 					content: "尚未授权注册",
		// 				});
		// 				return;
		// 			}
		// 			wx.showLoading({
		// 				title: "加载中...",
		// 				mask: true,
		// 			});
		// 			wx.request({
		// 				url: heloo.globalData.url + "/pointProduct/getPhoneNum",
		// 				method: "POST",
		// 				header: {
		// 					"Content-Type": "application/x-www-form-urlencoded",
		// 				},
		// 				data: {
		// 					openid: that.data.userInfo.openid,
		// 					sessionKey: heloo.globalData.userKey,
		// 					phonepagetype: "2",
		// 					encryptedData: e.detail.encryptedData,
		// 					iv: e.detail.iv,
		// 				},
		// 				success: function (res) {
		// 					wx.hideLoading();
		// 					if (res.data.status == "1") {
		// 						console.log(res);
		// 						heloo.globalData.userInfo = res.data.userinfo;
		// 						that.setData({
		// 							userInfo: res.data.userinfo,
		// 						});
		// 						that.getPrize();
		// 					} else {
		// 						wx.showModal({
		// 							title: "手机号解密失败",
		// 							content: res.data.msg,
		// 						});
		// 					}
		// 				},
		// 			});
		// 		}
		// 	},
		// 	fail: function () {
		// 		wx.showModal({
		// 			title: "授权失败",
		// 			content: "登录状态过期，请重新授权！",
		// 			success: function () {
		// 				that.userLogin();
		// 			},
		// 		});
		// 	},
		// });
	},
	// 时间戳转换-函数封装
	formatDate(inputTime) {
		var date = new Date(inputTime);
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		m = m < 10 ? "0" + m : m;
		var d = date.getDate();
		d = d < 10 ? "0" + d : d;
		var h = date.getHours();
		h = h < 10 ? "0" + h : h;
		var minute = date.getMinutes();
		var second = date.getSeconds();
		minute = minute < 10 ? "0" + minute : minute;
		second = second < 10 ? "0" + second : second;
		return y + "-" + m + "-" + d + " " + h + ":" + minute + ":" + second;
	},
	formatDate2(inputTime) {
		var date = new Date(inputTime);
		var y = date.getFullYear();
		var m = date.getMonth() + 1;
		m = m < 10 ? "0" + m : m;
		var d = date.getDate();
		d = d < 10 ? "0" + d : d;
		var h = date.getHours();
		h = h < 10 ? "0" + h : h;
		var minute = date.getMinutes();
		var second = date.getSeconds();
		minute = minute < 10 ? "0" + minute : minute;
		second = second < 10 ? "0" + second : second;
		return y + "." + m + "." + d;
	},
	getPic() {
		let that = this;
		if (that.data.pic && that.data.pic.length > 0) {
			return;
		}
		wx.showLoading({
			title: "加载中...",
			mask: true,
		});
		wx.request({
			url: heloo.globalData.url + "/api/lunbopicture/list",
			method: "POST",
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			data: {
				type: 3,
			},
			success(data) {
				console.log(data);
				that.setData({
					pic: data.data.data,
				});
				that.umaCode();
			},
		});
	},
	// heloo奖品记录
	signPrize(prize, uuid) {
		let that = this;
		wx.request({
			url: heloo.globalData.url + "/api/meco/awardPresent",
			method: "POST",
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			data: {
				// openid，province，award（奖品名），receiver，（收货人），phone，address
				openid: that.data.userInfo.openid,
				address: that.data.address,
				award: prize.name,
				awardId: prize.prizeId,
				prizeType: prize.prizeType,
				phone: that.data.userInfo.phoneNumber,
				receiver: that.data.userInfo.nickname,
				prizeUid: uuid,
				productNumber: that.data.productNumber,
				productName: that.data.productName,
			},
			success(data) {
				console.log("保存");
				console.log(data);
				console.log(that.data.productNumber);
				wx.hideLoading();
				if (data.data.status == 1) {
					that.setData({
						isShow: that.data.ruleVersion == 2023 ? false : true,
						isShow2023: that.data.ruleVersion == 2023 ? true : false,
						isOpened: true,
					});
				} else {
					console.log(data);
					wx.showToast({
						title: data.data.message,
						icon: "none",
					});
				}
			},
		});
	},
	// 领取微商城券
	getcard() {
		// wx.navigateToMiniProgram({
		// 	appId: "wx32be38d26e316e2c",
		// 	path: "pages/home/index?introduceID=990134291769&st=120&si=yibeiyima3_20210325",
		// });
		wx.navigateToMiniProgram({
			appId: wm_shop_app_id,
			path: "/cms_design/index?isqdzz=1&tracepromotionid=100077615&productInstanceId=11970818643&vid=0",
		});
	},
	// 点击跳转
	jump(e) {
		let that = this;
		let type = e.currentTarget.dataset.type;
		let index = e.currentTarget.dataset.index;
		let list = that.data.pic;
		if (list[index].appid) {
			if (list[index].appid == "wxb8a223205e2083f0") {
				let path = list[index].linkurl;
				if (!path) {
					path = list[index].linkPage;
				}
				wx.navigateTo({
					url: "/customize" + path,
				});
			} else if (list[index].appid == "wxd570425c5356eaa9") {
				let path = list[index].linkurl;
				if (!path) {
					path = list[index].linkPage;
				}
				wx.navigateTo({
					url: "/" + path,
				});
			} else {
				let path = list[index].linkurl;
				if (!path) {
					path = list[index].linkPage;
				}
				wx.navigateToMiniProgram({
					appId: list[index].appid,
					path: path,
					fail(res) {
						console.log(res.errMsg);
					},
				});
			}
		} else if (list[index].linkurl) {
			wx.navigateTo({
				url: "/customize/pages/web/web?url=" + list[index].linkurl,
			});
		} else if (list[index].linkPage) {
			wx.navigateTo({
				url: "/customize/pages/web/web?url=" + list[index].linkPage,
			});
		}
	},
	submit() {
		let that = this;
		if (that.data.userName == "") {
			wx.showToast({
				title: "请填写姓名！",
				icon: "none",
			});
			return;
		}
		if (that.data.userPhone.length != 11) {
			wx.showToast({
				title: "请填写正确手机号！",
				icon: "none",
			});
			return;
		}
		if (that.data.prize.prizeType == "PHYSICAL" && that.data.address == "") {
			wx.showToast({
				title: "请填写详细地址！",
				icon: "none",
			});
			return;
		}
		if (
			that.data.prize.prizeType == "CHANGE_OF_MONEY" ||
			that.data.prize.prizeType == "RED_PACKET"
		) {
			if (
				that.data.prize.prizeRegister &&
				that.data.prize.prizeRegister.registerInfo &&
				that.data.prize.prizeRegister.registerInfo.length > 0
			) {
				let _IDRe18 = /^([1-6][1-9]|50)\d{4}(18|19|20)\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
				let _IDre15 = /^([1-6][1-9]|50)\d{4}\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}$/;
				// 校验身份证：
				if (
					_IDRe18.test(that.data.userCard) ||
					_IDre15.test(that.data.userCard)
				) {} else {
					wx.showToast({
						title: "请填写正确身份证号！",
						icon: "none",
					});
					return;
				}
			}
			if (
				that.data.prize.poolPrizeVo &&
				that.data.prize.poolPrizeVo.prizeRegister.registerInfo &&
				that.data.prize.poolPrizeVo.prizeRegister.registerInfo.length > 0
			) {
				let _IDRe18 = /^([1-6][1-9]|50)\d{4}(18|19|20)\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
				let _IDre15 = /^([1-6][1-9]|50)\d{4}\d{2}((0[1-9])|10|11|12)(([0-2][1-9])|10|20|30|31)\d{3}$/;
				// 校验身份证：
				if (
					_IDRe18.test(that.data.userCard) ||
					_IDre15.test(that.data.userCard)
				) {} else {
					wx.showToast({
						title: "请填写正确身份证号！",
						icon: "none",
					});
					return;
				}
			}
		}
		wx.showLoading({
			title: "加载中...",
			mask: true,
		});
		wx.request({
			url: heloo.globalData.url + "/api/meco/awardPresent",
			method: "POST",
			header: {
				"Content-Type": "application/x-www-form-urlencoded",
			},
			data: {
				openid: that.data.userInfo.openid,
				prizeUid: that.data.uuid,
				receiver: that.data.userName,
				phone: that.data.userPhone,
				address: that.data.address,
				userCard: that.data.userCard,
				award: that.data.prize.name,
				awardId: that.data.prize.prizeId,
				prizeType: that.data.prize.prizeType,
			},
			success(data) {
				console.log(data);
				if (data.data.status == 1) {
					that.getPrize();
				} else {
					wx.showToast({
						title: data.data.message,
						icon: "none",
					});
				}
			},
		});
	},
	// u码登录
	umaLogin() {
		var that = this;
		wx.login({
			success(res) {
				wx.request({
					url: umaUrl + "/auth/mp/login", //仅为示例，并非真实接口地址。
					data: {
						appId: "wxb8a223205e2083f0",
						code: res.code,
					},
					method: "GET",
					header: {
						"Content-Type": "application/json", //自定义请求头信息
					},
					success: function success(um) {
						console.log("umaLogin", res);
						that.setData({
							umToken: um.data.token,
							umSerialId: um.data.serialId,
						});
						that.getUserlocation();
					},
					fail: function fail(err) {
						wx.hideLoading();
						console.log("fail:" + JSON.stringify(err));
					},
				});
			},
		});
	},
	// u码解码
	umaCode() {
		let that = this;
		wx.request({
			url: umaUrl + "/tag/v2/wx/decode",
			data: {
				codeTicket: that.data.ucode,
				wwAppId: that.data.wwAppId,
			},
			method: "GET",
			header: {
				Authorization: "Bearer " + that.data.umToken, //Bearer后面必须接空格再加token
				serialId: that.data.umSerialId,
				"Content-Type": "application/json", //自定义请求头信息
			},
			success: function success(um) {
				console.log("解码");
				console.log(um);
				that.setData({
					umCode: um.data.code,
					umUri: um.data.uri,
				});
				// 2021.8.19直接开始查验u码信息
				if (um.statusCode == 200) {
					that.umaInfo();
				} else {
					// wx.hideLoading();
					that.codeTip("此码未登记，重新扫码或联系客服！")
				}
			},
			fail: function fail(err) {
				wx.hideLoading();
				console.log("fail:" + JSON.stringify(err));
			},
		});
	},
	// u码信息查询
	umaInfo() {
		let that = this;
		wx.request({
			url: umaUrl + "/tag/v2/promotion", //仅为示例，并非真实接口地址。
			data: {
				code: that.data.umCode,
				uri: that.data.umUri,
			},
			method: "GET",
			header: {
				Authorization: "Bearer " + that.data.umToken,
				serialId: that.data.umSerialId,
				"Content-Type": "application/json", //自定义请求头信息
			},
			success: function success(um) {
				console.log("码信息");
				// console.log("第五期代码已更新");
				console.log(um);
				wx.hideLoading();
				if (um.data.publishStatus == "PROCESSING") {
					if (um.data.tagInfo) {
						let codeStatus = um.data.tagInfo.status;
						if (codeStatus == "ACT" || codeStatus == "SCANED") {
							that.setProduct(um.data.tagInfo.productId);
							heloo.writeTime("2", "pages/redEnvelope/home");
							console.log("码批次");
							console.log(um.data.tagInfo.tagBatchConfigId);
							// 第三期
							if (um.data.tagInfo.tagBatchConfigId == "1000236") {
								that.setData({
									ruleVersion: 3,
									newyear: false,
								});
							}
							// 第四期
							if (
								um.data.tagInfo.tagBatchConfigId == "1000317" ||
								um.data.tagInfo.tagBatchConfigId == "1000392"
							) {
								console.log("第四期新码更新");
								that.setData({
									ruleVersion: 4,
									newyear: false,
								});
							}
							// 第四期
							if (um.data.tagInfo.tagBatchConfigId == "1000344") {
								that.setData({
									ruleVersion: 4,
									newyear: false,
								});
							}
							//2023年
							if (um.data.tagInfo.tagBatchConfigId == "1000437" ||
								um.data.tagInfo.tagBatchConfigId == "1000526"
							) {
								that.setData({
									newyear: true,
									ruleVersion: 2023,
								});
							}
						} else if (codeStatus == "SCRAP") {
							that.codeTip("此码已报废，点击微商城获取更多福利哦！");
						} else if (codeStatus == "NO_ACT") {
							that.codeTip("此码未激活，点击微商城获取更多福利哦！");
						} else if (codeStatus == "REG") {
							that.codeTip("此码未激活已登记，点击微商城获取更多福利哦！");
						} else if (codeStatus == "LOTTERY") {
							that.codeTip("此码已抽过奖了，点击微商城获取更多福利哦！");
						} else if (codeStatus == "DEAD") {
							wx.hideLoading();
							wx.showModal({
								title: "提示",
								content: "此码已失效，点击微商城获取更多福利哦！",
								showCancel: true,
								confirmText: "微商城",
								success: function success(res) {
									if (res.confirm) {
										heloo.writeTime(
											"2",
											"pages/redEnvelope/home",
											"pages/home/index?introduceID=990128581405&st=120&si=yibeiyima_tanchaung"
										);
										wx.navigateToMiniProgram({
											appId: wm_shop_app_id,
											path: "/cms_design/index?isqdzz=1&tracepromotionid=100077520&productInstanceId=11970818643&vid=0",
										});
										// wx.navigateToMiniProgram({
										// 	appId: "wx32be38d26e316e2c",
										// 	path: "pages/home/index?introduceID=990128581405&st=120&si=yibeiyima_tanchaung",
										// 	extraData: {},
										// 	success: function success(res) {
										// 		console.log("res", res);
										// 	},
										// });
									} else {
										wx.reLaunch({
											url: "/customize/pages/index/index",
										});
									}
								},
							});
						}
					} else {
						that.codeTip("活动已结束，点击微商城获取更多福利哦！"); //活动信息缺失
					}
				} else {
					let publish = um.data.publishStatus;
					if (publish == "ENDED") {
						that.codeTip("活动已结束，点击微商城获取更多福利哦！");
					} else if (publish == "NO_STARTED") {
						that.codeTip("活动未开始，点击微商城获取更多福利哦！");
					} else if (publish == "PAUSE") {
						that.codeTip("活动已暂停，点击微商城获取更多福利哦！");
					}
				}
			},
			fail: function fail(err) {
				wx.hideLoading();
				console.log("fail:" + JSON.stringify(err));
			},
		});
	},
	// 统一弹框
	codeTip(text) {
		this.data.codeStatusTip = text;
		wx.hideLoading();
		wx.showModal({
			title: "提示",
			content: text,
			showCancel: true,
			confirmText: "微商城",
			success: function success(res) {
				if (res.confirm) {
					// wx.navigateToMiniProgram({
					// 	appId: "wx32be38d26e316e2c",
					// 	path: "pages/home/index?introduceID=990128581405",
					// 	extraData: {},
					// 	success: function success(res) {
					// 		console.log("res", res);
					// 	},
					// });
					wx.navigateToMiniProgram({
						appId: wm_shop_app_id,
						path: "/cms_design/index?isqdzz=1&tracepromotionid=100077520&productInstanceId=11970818643&vid=0",
					});
				} else {
					wx.reLaunch({
						url: "/customize/pages/index/index",
					});
				}
			},
		});
	},
	// u码抽奖
	lottery() {
		let that = this;
		console.log('u码抽奖')
		console.log(that.data.umCode)
		// 防止空码
		if (!that.data.umCode) {
			that.codeTip("此码未登记，重新扫码或联系客服！");
			return;
		}
		wx.showLoading({
			title: "加载中...",
			mask: true,
		});
		wx.request({
			url: umaUrl +
				"/lottery/v2/tag?longitude=" +
				that.data.longitude +
				"&latitude=" +
				that.data.latitude,
			data: {
				code: that.data.umCode,
				uri: that.data.umUri,
			},
			method: "POST",
			header: {
				Authorization: "Bearer " + that.data.umToken, //Bearer后面必须接空格再加token
				serialId: that.data.umSerialId,
				"Content-Type": "application/json", //自定义请求头信息
			},
			success: function success(um) {
				wx.hideLoading();
				console.log("开奖", um);
				// console.log("token:"+that.data.umToken)
				// console.log("serialId:"+that.data.umSerialId)
				if (um.data.ecode) {
					console.log(um.data);
					console.log(um.data.emsg);
					that.codeTip(that.data.codeStatusTip);
					// wx.showToast({
					//     title: um.data.emsg,
					//     icon: "none"
					// })
				} else {
					if (um.data.isHit) {
						//如果是黑名单用户直接中优惠券
						if (that.data.blacklist) {
							let prize = {};
							prize.prizeType = "COUPON";
							prize.name = "微商城15元优惠券";
							prize.prizeId = "1000674";
							that.setData({
								isShow: that.data.ruleVersion == 2023 ? false : true,
								isShow2023: that.data.ruleVersion == 2023 ? true : false,
								isOpened: true,
								prize: prize,
							});
							that.signPrize(prize, "未中奖");
						} else {
							// //如果是高风险用户直接中积分不中现金
							// if (!that.data.fk) {
							// 	let prize = {};
							// 	prize.prizeType = "POINT";
							// 	prize.name = "50会员积分";
							// 	prize.prizeId = "1000674";
							// 	that.setData({
							// 		isShow: that.data.ruleVersion == 2023 ? false : true,
							// 		isShow2023: that.data.ruleVersion == 2023 ? true : false,
							// 		isOpened: true,
							// 		prize: prize,
							// 	});
							// 	that.signPrize(prize, "未中奖");
							// } else {
							//中奖
							that.setData({
								uuid: um.data.uuid,
								prizeOrderNum: um.data.prizes[0].prizeOrderNum,
								prize: um.data.prizes[0],
								isShow: that.data.ruleVersion == 2023 ? false : true,
								isShow2023: that.data.ruleVersion == 2023 ? true : false,
								isOpened: true,
							});
							that.signPrize(um.data.prizes[0], um.data.uuid);
							// }
						}
					} else {
						let prizeRandom = Math.random();
						let prize = {};
						// 如果是黑名单用户强制中优惠券
						if (that.data.blacklist) {
							prize.prizeType = "COUPON";
							prize.name = "微商城15元优惠券";
							prize.prizeId = "1000674";
							that.setData({
								isShow: that.data.ruleVersion == 2023 ? false : true,
								isShow2023: that.data.ruleVersion == 2023 ? true : false,
								isOpened: true,
								prize: prize,
							});
							that.signPrize(prize, "未中奖");
						} else {
							if (that.data.ruleVersion == 2023) {
								if (prizeRandom > 0.5) {
									prize.prizeType = "POINT";
									prize.name = "50会员积分";
									prize.prizeId = "1000674";
								} else {
									prize.prizeType = "COUPON";
									prize.name = "微商城15元优惠券";
									prize.prizeId = "1000677";
								}
							} else {
								prize.prizeType = "POINT";
								prize.name = "50会员积分";
								prize.prizeId = "1000674";
							}
							that.setData({
								isShow: that.data.ruleVersion == 2023 ? false : true,
								isShow2023: that.data.ruleVersion == 2023 ? true : false,
								isOpened: true,
								prize: prize,
							});
							if (prize.prizeType == "POINT") {
								that.signPrize(prize, "未中奖");
							}
						}
					}
				}
			},
			fail: function fail(err) {
				wx.hideLoading();
				console.log("fail:" + JSON.stringify(err));
			},
		});
	},
	// u码兑奖
	getPrize() {
		let that = this;
		wx.showLoading({
			title: "加载中...",
			mask: true,
		});
		if (that.data.userCard) {
			var data = {
				uuid: that.data.uuid,
				prizeOrderNum: that.data.prizeOrderNum,
				register: {
					consignee: that.data.userName,
					phoneNumber: that.data.userPhone,
					idCardNo: that.data.userCard,
				},
			};
		} else if (that.data.address) {
			var data = {
				uuid: that.data.uuid,
				prizeOrderNum: that.data.prizeOrderNum,
				register: {
					consignee: that.data.userName,
					address: that.data.address,
					phoneNumber: that.data.userPhone,
				},
			};
		} else {
			var data = {
				uuid: that.data.uuid,
				prizeOrderNum: that.data.prizeOrderNum,
			};
		}
		console.log(data);
		wx.request({
			url: umaUrl + "/claim",
			data: data,
			method: "POST",
			header: {
				Authorization: "Bearer " + that.data.umToken, //Bearer后面必须接空格再加token
				serialId: that.data.umSerialId,
				"Content-Type": "application/json", //自定义请求头信息
			},
			success: function success(um) {
				wx.hideLoading();
				console.log("兑奖");
				console.log(um);
				if (um.data.prizeStatus == "CLAIM_SUCCESS") {
					if (that.data.prize.prizeType == "PHYSICAL") {
						//实物
						that.setData({
							userName: "",
							userPhone: "",
							address: "",
							isGet: true,
							isShow: false,
							isShow2023: false,
						});
					} else {
						wx.showToast({
							title: "领取成功",
							icon: "none",
							success() {
								if (
									that.data.prize.prizeType == "CHANGE_OF_MONEY" ||
									that.data.prize.prizeType == "RED_PACKET"
								) {
									console.log("关闭领取弹窗");
									setTimeout(() => {
										wx.redirectTo({
											url: "/customize/pages/roundRotate/roundRotate",
											// url: "/customize/pages/index/index",
										});
									}, 1500);
								}
							},
						});
						that.setData({
							isShow: false,
							isShow2023: false,
						});
					}
				} else {
					wx.showToast({
						title: um.data.ecode + ":" + um.data.emsg,
						icon: "none",
					});
				}
			},
			fail: function fail(err) {
				wx.hideLoading();
				console.log("fail:" + JSON.stringify(err));
			},
		});
	},
	hideRecord() {
		this.setData({
			isRecord: true,
		});
	},
	// 拉取获奖记录
	getRecord() {
		wx.navigateTo({
			url: "/customize/pages/package/package",
		});
		return;
		let that = this;
		wx.showLoading({
			title: "加载中...",
			mask: true,
		});
		wx.request({
			url: umaUrl + "/user/lotteryRecords?page=" + that.data.pageNo + "&size=20",
			data: {
				page: that.data.pageNo,
				size: 20,
			},
			method: "POST",
			header: {
				Authorization: "Bearer " + that.data.umToken, //Bearer后面必须接空格再加token
				serialId: that.data.umSerialId,
				"Content-Type": "application/json", //自定义请求头信息
			},
			success: function success(um) {
				wx.hideLoading();
				console.log("记录");
				console.log(um);
				let pageNo = that.data.pageNo;
				let record = that.data.record;
				let list = um.data.content;
				list.map((item) => {
					if (item.prizeType) {
						if (item.prizeType == "COUPON" || item.prizeType == "POINT") {
							item.prizeName = "50会员积分";
							item.prizeStatus = "CLAIM_SUCCESS";
						}
					} else {
						item.prizeName = "50会员积分";
						item.prizeStatus = "CLAIM_SUCCESS";
					}
					item.time = that.formatDate2(item.lotteryTime);
				});
				if (list.length > 0) {
					pageNo++;
				}
				that.setData({
					record: record.concat(list),
					pageNo: pageNo,
					isRecord: false,
				});
			},
			fail: function fail(err) {
				wx.hideLoading();
				console.log("fail:" + JSON.stringify(err));
			},
		});
	},
	setPrize(e) {
		let index = e.currentTarget.dataset.index;
		let that = this;
		let prize = that.data.record[index];
		prize.name = prize.prizeName;
		that.setData({
			uuid: prize.uuid,
			prizeOrderNum: prize.prizeOrderNum,
			prize: prize,
			isShow: that.data.ruleVersion == 2023 ? false : true,
			isShow2023: that.data.ruleVersion == 2023 ? true : false,
			isOpened: true,
			isRecord: true,
			record: [],
			pageNo: 0,
		});
	},
	setProduct(id) {
		let that = this;
		let productNumber = id;
		let namelist = [
			"桃桃红柚",
			"泰式青柠",
			"金桔柠檬",
			"樱桃莓莓",
			"红石榴白葡萄",
			"Meco蜜谷泰式青柠瓶装果汁茶",
			"Meco蜜谷桃桃红柚瓶装果汁茶",
			"Meco蜜谷红石榴白葡萄瓶装果汁茶",
			"Meco蜜谷·果汁茶桃桃红柚（22D02版）",
			"Meco蜜谷·果汁茶泰式青柠（22D02版）",
			"Meco蜜谷·果汁茶樱桃莓莓（22D02版）",
			"Meco蜜谷·果汁茶红石榴白葡萄（22D02版）",
			"Meco蜜谷·果汁茶荔枝百香（22D02版）",
			"Meco蜜谷·果汁茶芒果芭乐（23D01版）",

			// "Meco蜜谷樱桃莓莓瓶装果汁茶",
			// "Meco蜜谷荔枝百香瓶装果汁茶",
			// "Meco蜜谷芒果芭乐瓶装果汁茶",

		];
		let idlist = [
			"1000038",
			"1000039",
			"1000040",
			"1004334",
			"1025887",
			"6938888882170",
			"6938888882187",
			"6938888882194",
			"1034352",
			"1034353",
			"1034354",
			"1034355",
			"1034356",
			"1034368",
		];
		// 1000038	桃桃红柚
		// 1000039	泰式青柠
		// 1000040	金桔柠檬
		// 1004334	樱桃莓莓
		// 1025887	红石榴白葡萄
		// productName 1000038
		if (idlist.indexOf(id.toString()) < 0) {} else {
			// 匹配到商品
			that.setData({
				productNumber: productNumber,
				productName: namelist[idlist.indexOf(id.toString())],
			});
		}
	},
	getOrchardPhone(e) {
		this.toOrchard(e.detail.encryptedData, e.detail.iv);
	},
	// 0元领果汁
	async toOrchard() {
    return wx.showToast({
      title: '当前活动已结束，暂不可进入',
      icon:'none'
    })
		let that = this;
		const maiOpenId = await mai.member.getOpenId()
		if (!maiOpenId) {
			await mai.member.signin();
		}
		const member = await mai.member.getDetail(["Card"]);
		console.log(member.isActivated, "member");
		if (!member.isActivated) {
			//是否注册会员
			return wx.navigateTo({
				url: "/pages/register-member/index?completeType=back",
			});
		}
		if (!that.data.userInfo) {
			wx.showModal({
				content: "尚未授权注册，是否立即前往？",
				cancelText: "否",
				confirmText: "是",
				success(res) {
					if (res.confirm) {
						that.getInfo();
					}
				},
			});
			return;
		}
		wx.showLoading({
			title: "加载中...",
			mask: true,
		});
		wx.login({
			success(res) {
				console.log(res.code);
				wx.request({
					url: heloo.globalData.baseUrlOrchard + "/farmApi/userIsNull",
					method: "POST",
					data: {
						code: res.code,
					},
					success(result) {
						if (result.data.status == -1) {
							wx.hideLoading();
							wx.showToast({
								title: "授权失败",
								icon: "none",
							});
						} else if (result.data.data.message == "该用户openid还未注册！") {
							wx.request({
								url: heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
								method: "POST",
								data: {
									userInfo: JSON.stringify(heloo.globalData.userInfo),
									openid: result.data.data.openid,
									unionid: result.data.data.unionid,
									phone: result.data.data.phone,
								},
								success(res) {
									console.log(res);
									wx.setStorageSync("uid", result.data.data.unionid);
									wx.navigateTo({
										url: `/customize/pages/orchard/orchard?uid=${wx.getStorageSync(
											"uid"
										)}`,
									});
									wx.hideLoading();
								},
								fail() {
									wx.hideLoading();
								},
							});
						} else if (result.data.message == "请先成为果园用户选取种子") {
							wx.request({
								url: heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
								method: "POST",
								data: {
									userInfo: JSON.stringify(heloo.globalData.userInfo),
									openid: result.data.data.openid,
									unionid: result.data.data.unionid,
								},
								success(res) {
									console.log(res);
									wx.setStorageSync("uid", result.data.data.unionid);
									wx.navigateTo({
										url: `/customize/pages/orchard/orchard?uid=${wx.getStorageSync(
											"uid"
										)}`,
									});
									wx.hideLoading();
								},
								fail() {
									wx.hideLoading();
								},
							});
						} else {
							wx.request({
								url: heloo.globalData.baseUrlOrchard + "/farmApi/getunionid",
								method: "POST",
								data: {
									userInfo: JSON.stringify(heloo.globalData.userInfo),
									openid: result.data.data.wxuser.openid,
									unionid: result.data.data.wxuser.unionid,
								},
								success(res) {
									console.log(res);
									wx.setStorageSync("uid", result.data.data.wxuser.unionid);
									wx.navigateTo({
										url: `/customize/pages/orchard/orchard?uid=${wx.getStorageSync(
											"uid"
										)}`,
									});
									wx.hideLoading();
								},
								fail() {
									wx.hideLoading();
								},
							});
						}
					},
					fail() {
						wx.hideLoading();
					},
				});
			},
		});
	},
});
