import { Setting, _ } from "../../engine/index";

Component({
  properties: {
    tabbarSetting: {
      type: Object,
      value: {},
      observer(newVal) {
        if (!_.isEmpty(newVal)) {
          const { pages, iconColors, textColors, backgroundColor } = newVal;
          pages.forEach((item, index) => {
            item.isSelected = index === this.data.tabbarIndex;
            item.isImage = item.images[0].startsWith("http");
          });
          this.setData({
            tabbarSetting: newVal,
            styleVars: `
            
              --bg-color: ${backgroundColor};
              --icon-color: ${iconColors[0]};
              --icon-active-color: ${iconColors[1]};
              --text-color: ${textColors[0]};
              --text-active-color: ${textColors[1]};
            `,
          });
        }
      },
    },
    tabbarIndex: {
      type: Number,
      value: 0,
    },
    pageId: {
      type: String,
      value: "",
    },
  },
  methods: {
    async onChangeTab(e) {
      const currentIndex = e.currentTarget.dataset.index;
      if (currentIndex === this.data.tabbarIndex) {
        return;
      }
      const { tabbarSetting } = this.data;
      const page = tabbarSetting.pages[currentIndex];
      console.log(page, "page66666666");
      // "micromall"
      switch (page.type) {
        case Setting.pageType.HOME:
          Setting.redirectTo("/customize/pages/example/index");
          return;
        case Setting.pageType.WEAPP:
          if (page.pagePath === "/member/pages/goods-list/index") {
            wx.navigateTo({
              url: "../exchangeWeb/index",
            });
          } else {
            if (page.pagePath == "/customize/pages/exchangeWeb/index") {
              wx.navigateTo({
                url: page.pagePath,
              })
            }else{
              Setting.redirectTo(page.pagePath);
            }
          }
          return;
          case Setting.pageType.MICRO_MALL:
            wx.openEmbeddedMiniProgram({
              appId:'wx1a8460be609a207c',
              path:"/cms_design/index?isqdzz=1&tracepromotionid=100077527&productInstanceId=11970818643&vid=0",
              // envVersion:'trial'
            })
            return
            break;
        case Setting.pageType.CUSTOM:
          // 其他页面自定义配置
          // if (page.title === '微商城') {
          //   Setting.redirectTo('/customize/pages/heloo_shop/heloo_shop');
          //   return;
          // }
          break;
        default:
          break;
      }
      console.log(1)
      const result = Setting.getRedirectUrlAndParams(page);
      if (!result) {
        return;
      }
      wx.redirectTo({ ...result });
    },
  },
});
