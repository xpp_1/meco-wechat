import pageSetting from './page'
export default class Setting {
  static tabBarConfig = null;

  static pageType = {
    PRODUCT: 'product',
    CART: 'cart',
    CLASSIFICATION: 'classification',
    ME: 'me',
    SHARE: 'share',
    HOME: 'home',
    PRODUCT_DETAIL: 'productDetail',
    CUSTOM: 'custom',
    WEAPP: 'weapp',
    MICRO_MALL: 'micromall',
    SIGNIN: 'signin',
    LUCKY_DRAW: 'luckydraw',
    LUCKY_DRAW_V2: 'luckydrawV2',
    WORD_COLLECTION_CAMPAIGN: 'wordcollectioncampaign',
    VENUE: 'venue',
    SHARE_CAMPAIGN: 'sharecampaign',
    SURVEY: 'survey',
    WECHATWORK: 'wechatwork',
    MEMBER_CENTER: 'member',
    MEETING: 'meeting',
  };

  static _isLoading = false;

  static getTabbarSetting = async (status = 'published') => {
    return new Promise((resolve)=>resolve(pageSetting))
    // const { mai } = getApp();
    // if (Setting.tabBarConfig) {
    //   return Setting.tabBarConfig;
    // }

    // const url = '/v2/ec/weappTabBars';
    // const result = await mai.request.get(url, { params: { status } });
    // Setting.tabBarConfig = result;
    // return result;
  }

  static getRedirectUrlAndParams = (pageSetting) => {
    const { type } = pageSetting;
    const args = pageSetting.args || {};
    const value = pageSetting.id || pageSetting.value;
    let url = '';
    let query = { pageId: value };
    switch (type) {
      case Setting.pageType.PRODUCT:
        url = '/pages/product-list/index';
        break;
      case Setting.pageType.CART:
        url = '/pages/shopping-cart/index';
        break;
      case Setting.pageType.CLASSIFICATION:
        url = '/pages/product-categories/index';
        query = { pageId: value, classificationId: args.id };
        break;
      case Setting.pageType.ME:
        url = '/pages/personal-center/index';
        break;
      case Setting.pageType.SHARE:
        url = '/pages/share/index';
        break;
      case Setting.pageType.HOME:
        url = '/pages/home/index';
        break;
      case Setting.pageType.CUSTOM:
        url = '/pages/custom-page/index';
        break;
      case Setting.pageType.PRODUCT_DETAIL:
        url = '/pages/product-detail/index';
        query = { productId: value };
        break;
      case Setting.pageType.MICRO_MALL:
        url = '/member/pages/score-goods-list/index';
        query = { pageId: value, shelfId: args.id };
        break;
      case Setting.pageType.SIGNIN:
        url = '/member/pages/sign-in/index';
        break;
      case Setting.pageType.WECHATWORK:
        url = '/mall/pages/card/index';
        break;
      case Setting.pageType.MEMBER_CENTER:
        url = '/mall/pages/member-center-v3/index';
        break;
      default:
        break;
    }
    if (!url) {
      return null;
    }
    return { url, query };
  }

  static getTabbarIndex = (tabbarSetting, type, pageId, pagePath = '') => {
    const tabbarIndex = tabbarSetting.pages.findIndex((item) => {
      if (item.type === Setting.pageType.WEAPP) {
        return item.pagePath === pagePath;
      }

      return item.type === type && (!pageId || item.id === pageId);
    });
    return tabbarIndex;
  }

  static redirectTo = (url) => {
    if (Setting._isLoading) {
      return;
    }

    Setting._isLoading = true;
    wx.redirectTo({
      url,
      complete: () => { Setting._isLoading = false; },
      // eslint-disable-next-line
      fail: (error) => { console.warn('error', error); },
    });
  }
}
