const extConfig = require('../ext')
export default class ExtConfig {
  
  static _config = extConfig.default.ext;

  static getWeappId = () => {
    return ExtConfig._config.weappId;
  }

  static getMaiEnv = () => {
    return ExtConfig._config.maiENV;
  }

  static getMaiAccountId = () => {
    return ExtConfig._config.maiAccountId;
  }

  static getWeappInitPage = () => {
    return ExtConfig._config.weappInitPage;
  }
}
