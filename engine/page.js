export default {
  "id": "5f4cdd923ac61f00f2a2b7c1",
  "accountId": "5ee3221af0da4b69960bf2f8",
  "style": "",
  "customColors": ["#44B1CC", "#FFFFFF"],
  "status": "open",
  "pages": [{
    "images": ["https://statics.maiscrm.com/5ee3221af0da4b69960bf2f8/modules/content/o_1fn0va5u3j2u8nm14tpnip9qh2/%E9%A6%96%E9%A1%B5%3A%E6%9C%AA%E9%80%89%E4%B8%AD.png", "https://statics.maiscrm.com/5ee3221af0da4b69960bf2f8/modules/content/o_1fn0va6uu1t9q1q0dmf10l917eb5/%E9%A6%96%E9%A1%B5%3A%E9%80%89%E4%B8%AD.png"],
    "title": "首页",
    "id": "5f4cdd923ac61f00f2a2b7b4",
    "pageName": "默认页面-首页",
    "type": "home",
    "status": "",
    "appId": "",
    "pagePath": "/customize/pages/index/index",
    "args": {}
  }, {
    "images": ["https://statics.maiscrm.com/5ee3221af0da4b69960bf2f8/modules/content/o_1fn0va6po5dtlbs1jpp6321io44/%E7%A7%AF%E5%88%86%E5%95%86%E5%9F%8E%3A%E6%9C%AA%E9%80%89%E4%B8%AD.png", "https://statics.maiscrm.com/5ee3221af0da4b69960bf2f8/modules/content/o_1fn0va6971h971kkenp6s73tbe3/%E7%A7%AF%E5%88%86%E5%95%86%E5%9F%8E%3A%E9%80%89%E4%B8%AD.png"],
    "title": "微商城",
    "id": "",
    "pageName": "",
    "type": "weapp",
    "status": "",
    "appId": "",
    "pagePath": "",
    "args": {}
  }, {
    "images": ["https://statics.maiscrm.com/5ee3221af0da4b69960bf2f8/modules/content/o_1fn0v9ukv7tn1pnihv6121fpkk0/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83%3A%E6%9C%AA%E9%80%89%E4%B8%AD.png", "https://statics.maiscrm.com/5ee3221af0da4b69960bf2f8/modules/content/o_1fn0va5l41k20eme1aq71vgo121u1/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83%3A%E9%80%89%E4%B8%AD.png"],
    "title": "个人中心",
    "id": "5f4cdd923ac61f00f2a2b7b5",
    "pageName": "默认页面-个人中心",
    "type": "me",
    "status": "",
    "appId": "",
    "pagePath": "/customize/pages/userinfo/index",
    "args": {}
  }],
  "backgroundColor": "#FFFFFF",
  "textColors": ["#999999", "#44B1CC"],
  "iconColors": ["#999999", "#44B1CC"],
  "createdAt": "2020-08-31T19:22:58+08:00",
  "updatedAt": "2024-04-28T11:35:39+08:00",
  "marketingStyle": {
    "enable": true,
    "priceColor": "#44B1CC",
    "originPriceColor": "rgba(68, 177, 204, 0.5)",
    "tagColor": "#44B1CC"
  },
  "loadingImage": ""
}