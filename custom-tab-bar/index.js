import { Setting } from "../engine/index";

Component({
  data: {
    selected: null,
    isEnabled: true,
  },
  async attached() {
    // const { mai } = getApp();
    const { usR } = getApp();
const mai = {
  member:usR
}
    if (!mai.member.getOpenId()) {
      return;
    }

    // 每个页面都会生成 tabBar 实例
    const tabBarSetting = await Setting.getTabbarSetting();
    this.setData({ isEnabled: tabBarSetting.status === "open" });
    if (!this.data.isEnabled) {
      return;
    }

    this.formatTabBarSetting(tabBarSetting);
  },
  methods: {
    formatTabBarSetting(tabBarSetting) {
      const { pages, iconColors, textColors, backgroundColor } = tabBarSetting;
      pages.forEach((item) => {
        item.isImage = item.images[0].startsWith("http");
      });
      this.setData({
        tabBarSetting,
        styleVars: `
            --bg-color: ${backgroundColor};
            --icon-color: ${iconColors[0]};
            --icon-active-color: ${iconColors[1]};
            --text-color: ${textColors[0]};
            --text-active-color: ${textColors[1]};
          `,
      });
    },
    async onChangeTab({ currentTarget }) {
      const selected = currentTarget.dataset.index;
      const page = this.data.tabBarSetting.pages[selected];
      switch (page.type) {
        case Setting.pageType.HOME:
          Setting.redirectTo("/customize/pages/example/index");
          return;
        case Setting.pageType.WEAPP:
          if (page.pagePath === "/member/pages/goods-list/index") {
            wx.navigateTo({
              url: "../exchangeWeb/index",
            });
          } else {
            Setting.redirectTo(page.pagePath);
          }
          return;
        case Setting.pageType.CUSTOM:
          // 其他页面自定义配置
          // if (page.title === '微商城') {
          //   Setting.redirectTo('/customize/pages/heloo_shop/heloo_shop');
          //   return;
          // }
          break;
          case Setting.pageType.MICRO_MALL:
            wx.openEmbeddedMiniProgram({
              appId:'wx1a8460be609a207c',
              path:"/cms_design/index?isqdzz=1&tracepromotionid=100077527&productInstanceId=11970818643&vid=0",
              // envVersion:'trial'
            })
            return
            break;
        default:
          break;
      }

      const result = Setting.getRedirectUrlAndParams(page);
      if (!result) {
        return;
      }
      wx.redirectTo({ ...result });
    },
  },
});
